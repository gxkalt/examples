/*****************************************************************************
 *
 * Copyright 2013 Altera Corporation. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/

#include <stdio.h>
#include <inttypes.h>
#include "alt_clock_manager.h"
#include "alt_generalpurpose_io.h"
#include "alt_interrupt.h"
#include "alt_timers.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_rstmgr.h"

/* enable semihosting with gcc by defining an __auto_semihosting symbol */
int __auto_semihosting;

//
// Initializes and enables the interrupt controller.
//
ALT_STATUS_CODE socfpga_int_start(ALT_INT_INTERRUPT_t int_id,
                                  alt_int_callback_t callback,
                                  void * context)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    //
    // Initialize the global and CPU interrupt items
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_init();
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_init();
    }

    //
    // Setup the interrupt specific items
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_isr_register(int_id, callback, context);
    }

    if (   (status == ALT_E_SUCCESS)
        && (int_id >= 32)) // Ignore target_set() for non-SPI interrupts.
    {
        int target = 0x3;
        status = alt_int_dist_target_set(int_id, target);
    }

    //
    // Enable the distributor, CPU, and global interrupt
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_dist_enable(int_id);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_enable();
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_enable();
    }

    return status;
}

void socfpga_int_stop(ALT_INT_INTERRUPT_t int_id)
{
    //
    // Disable the global, CPU, and distributor interrupt
    //

    alt_int_global_disable();

    alt_int_cpu_disable();

    alt_int_dist_disable(int_id);

    //
    // Unregister the ISR.
    //

    alt_int_isr_unregister(int_id);

    //
    // Uninitialize the CPU and global data structures.
    //

    alt_int_cpu_uninit();

    alt_int_global_uninit();
}

ALT_STATUS_CODE socfpga_timer_start(ALT_GPT_TIMER_t timer, uint32_t period_in_ms)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t freq;

    //
    // Determine the frequency of the timer
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &freq);
        printf("INFO: Frequency = %" PRIu32 ".\n", freq);
    }

    //
    // Set the counter of the timer, which determines how frequently it fires.
    //

    if (status == ALT_E_SUCCESS)
    {
        uint32_t counter = (freq / 1000) * period_in_ms;

        status = alt_gpt_counter_set(timer, counter);
        printf("INFO: Period    = %" PRIu32 " millisecond(s).\n", period_in_ms);
        printf("INFO: Counter   = %" PRIu32 ".\n", counter);
    }

    //
    // Set to periodic, meaning it fires repeatedly.
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpt_mode_set(timer, ALT_GPT_RESTART_MODE_PERIODIC);
    }
    
    //
    // Set the prescaler of the timer to 1.
    //
    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpt_prescaler_set(timer, 1);
    }

    //
    // Clear pending interrupts
    //

    if (status == ALT_E_SUCCESS)
    {
        if (alt_gpt_int_if_pending_clear(timer) == ALT_E_BAD_ARG)
        {
            status = ALT_E_BAD_ARG;
        }
    }

    //
    // Enable timer interrupts
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpt_int_enable(timer);
    }

    //
    // Start the timer.
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpt_tmr_start(timer);
    }

    return status;
}

void socfpga_timer_stop(ALT_GPT_TIMER_t timer)
{
    alt_gpt_tmr_stop(timer);

    alt_gpt_int_disable(timer);
}

ALT_STATUS_CODE socfpga_gpio_start(ALT_GPIO_PORT_t bank)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    //
    // Init the GPIO system
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpio_init();
    }

    //
    // Put all pins into OUTPUT mode.
    //

    if (status == ALT_E_SUCCESS)
    {
        status = alt_gpio_port_datadir_set(bank, ALT_GPIO_BITMASK, ALT_GPIO_BITMASK);
    }

    return ALT_E_SUCCESS;
}

void socfpga_gpio_stop(ALT_GPIO_PORT_t bank)
{
    //
    // Uninit the GPIO system
    //

    alt_gpio_uninit();
}

typedef struct ALT_TEST_TIMER_CONTEXT_s
{
    // Number of times interrupt has been called.
    volatile int count;

    // Current part of the grey code to be displayed.
    volatile int code;
}
ALT_TEST_TIMER_CONTEXT_t;

void test_timer_int_callback(uint32_t icciar, void * context)
{
#define ALTR_5XS1_GPIO1_LED0                                  (0x00008000)        //  GPIO 44 (44 - 29 == 15)
#define ALTR_5XS1_GPIO1_LED1                                  (0x00004000)        //  GPIO 43 (43 - 29 == 14)
#define ALTR_5XS1_GPIO1_LED2                                  (0x00002000)        //  GPIO 42 (42 - 29 == 13)
#define ALTR_5XS1_GPIO1_LED3                                  (0x00001000)        //  GPIO 41 (41 - 29 == 12)
#define ALTR_5XS1_GPIO1_LED_SHIFT                             (12)

    ALT_TEST_TIMER_CONTEXT_t * data = (ALT_TEST_TIMER_CONTEXT_t *) context;

    //
    // Clear the timer interrupt
    //

    alt_gpt_int_if_pending_clear(ALT_GPT_CPU_PRIVATE_TMR);

    //
    // Increment the ISR call count.
    //

    data->count++;

    printf("ISR: Count = %d.\n", data->count);

    //
    // Calculate the grey code, 4 bits
    //
    uint32_t gray = (data->code >> 1) ^ data->code;

    printf("ISR: Gray code(i=0x%x) => 0x%" PRIx32 ".\n", data->code, gray);

    data->code++;

    // GPIO1, software port 1

    alt_gpio_port_data_write(ALT_GPIO_PORTB,
                             (ALTR_5XS1_GPIO1_LED0 | ALTR_5XS1_GPIO1_LED1 |
                              ALTR_5XS1_GPIO1_LED2 | ALTR_5XS1_GPIO1_LED3),
                             gray << ALTR_5XS1_GPIO1_LED_SHIFT);
}

int main(int argc, char** argv)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    ALT_TEST_TIMER_CONTEXT_t data;
    data.count = 0;
    data.code  = 0;

    //
    // Start the interrupt system
    //

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_int_start(ALT_INT_INTERRUPT_PPI_TIMER_PRIVATE,
                                   test_timer_int_callback, &data);
    }

    //
    // Setup the GPIO
    //

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_gpio_start(ALT_GPIO_PORTB);
    }

    //
    // Start the timer system
    //

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_timer_start(ALT_GPT_CPU_PRIVATE_TMR, 1500);
    }

    //
    // Wait for the timer to be called X times.
    //

    if (status == ALT_E_SUCCESS)
    {
        while (data.count < 10)
        {
            ;
        }
    }

    //
    // Stop the timer system
    //

    socfpga_timer_stop(ALT_GPT_CPU_PRIVATE_TMR);
    
    //
    // Stop the GPIO system
    //

    socfpga_gpio_stop(ALT_GPIO_PORTB);

    //
    // Stop the interrupt system
    //

    socfpga_int_stop(ALT_INT_INTERRUPT_PPI_TIMER_PRIVATE);

    if (status == ALT_E_SUCCESS)
    {
        printf("RESULT: All tests successful.\n");
        return 0;
    }
    else
    {
        printf("RESULT: Some failures detected.\n");
        return 1;
    }
}

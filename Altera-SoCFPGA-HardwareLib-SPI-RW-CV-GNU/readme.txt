Introduction

This HardwareLibs project is meant as an example to show how to read/write  
EEPROM deivce with SPI API, build code with GNU toolchain. 

Furthermore, <Makefile> can be changed to enable/disable semihosting requirement,

If SEMIHOSTING ?= 0, then code will be build without semihosting support. In this 
   case, one can't watch output data from App Console.
   However, one can download code to DS-5, and then just run it, with a serial port
   set to 115200, 8, n, 1 to capture output data.

If SEMIHOSTING ?= 1, then code will be built with semihosting support.

Please note that at this time, disabling semihosting will only allow
ALT_PRINTF() to function to print through the UART. Any other function call
that can be semihosted (such as printf()), will cause the code to not
compile. In a future release, all functions that can be semihosted will also
be supported in non-semihosted mode.

This test uses ST M95xxx 512Kbit serial SPI bus EEPROM as the slave. The datasheet can be found at 
http://www.st.com/web/en/resource/technical/document/datasheet/CD00048102.pdf

Included, please find a variety of support documentations:

1. <eeprom-wiring-schematic.pdf>

   It shows how to wire the eeprom device with J32 header on DevKit board.

2. <board-setup.jpg>

   It shows that an EEPROM board connected on DevKit J32 header. Please MAKE 
   SURE that J31 to the left of J32 has a jumper block installed. Without 
   jumper block, J32 is connected with I2C pins.  With jumper block, J32 is
   connected with SPI pins. DevKit shipped without the jumper block installed.

3. <capture-coarse.PNG>

   It shows that the SPI signal captured on a Tektronix DPO 2024B scope
   with two byte transfer.

4. <capture-fine.PNG>

   It shows that the SPI signal captured on a Tektronix DPO 2024B scope
   with a single byte transfer.

5. <test-result-app-console.rtf> 

   When debugging or running the SPI example design on a DS-5, the test 
   result seen on DS-5 App Console on lower right corner.

=====

Source Files

The following are descriptions of the source and header files contained in this
project:

hwlib.c

  This file is the main glue for all the components. It initializes the
  SPI master and slave. Then it writes a block of 32-byte of data to a 
  randomly generated EEPROM internal address, reads them back, then compares the data 
  read with the data written to see if they are the same. The above procedure 
  repeats multiple times to show the consistency.

=====

Building Example

Before running the example, the target executable first needs to be built.

1. In DS-5, build the application:
  1a. Switch to the C/C++ Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> C/C++.
  1b. In the "Project Explorer" panel, right-mouse-click 
      "Altera-SoCFPGA-HardwareLib-SPI-RW-CV-GNU" and select "Build Project".

The Console panel (bottom of the UI) should detail the progress of the build
and report any warnings or errors.

=====

System Setup

1. Connect the USB-BlasterII to the host computer.
2. Install the USB-BlasterII driver on the host computer if that driver is not
   already present. Consult documentation for QuartusII for instructions on
   installing the USB-BlasterII driver.
3. In DS-5, configure the launch configuration.
  3a. Select the menu: Run >> Debug Configurations...
  3b. In the options on the left, expand "DS-5 Debugger" and select
      "Altera-SoCFPGA-HardwareLib-SPI-RW-CV-GNU-Debug".
  3c. In the Target Connection drop down list, "choose USB-Blaster"    
  3d. In the "Connections" section near the bottom, click Browse.
  3e. Select the appropriate USB-BlasterII to use. Multiple items will be
      presented if there is more than one USB-BlasterII connection attached to
      the host computer.
  3f. Click "Apply" then "OK" to apply the USB-BlasterII selection.
  3g. Click "Close" to close the Debug Configuration. Otherwise click "Debug"
      run the example in the debugger.

=====

Running the Example

After building the example and setting up the host computer system, the example
can be run by following these steps.

1. In DS-5, launch the debug configuration.
  1a. Switch to the Debug Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> DS-5 Debug.
  1b. In the "Debug Control" panel, right-mouse-click
      "Altera-SoCFPGA-HardwareLib-SPI-RW-CV-GNU-Debug" and select
      "Connect to Target".

Connecting to the target takes a moment to load the preloader, run the
preloader, load the executable, and run executable. After the debug connection
is established, the debugger will pause the execution at the main() function.
Users can then set additional break points, step into, step out of, or step one
line using the DS-5 debugger. Consult documentation for DS-5 for more
information on debugging operations.

How to flash the code to QSPI memory:

   1. Open <Makefile> file, set SEMIHOSTING ?= 0.
      build code, the result is a file <hwlib.axf>.
   2. Open Embedded Command Shell in the Altera SocEDS installation directory, 
      e.g., C:\altera\14.x\embedded\Embedded_Command_Shell.bat
   3. run command at the workspace directory where hwlib.axf is located: 
       fromelf --bin --output=hwlib.bin hwlib.axf
       ./add_header.pl --load_addr 0x2000000 --entry 0 --name SPI-READ-WRITE --file hwlib.bin   
          a file hwlib_whdr.bin will be created.
   4. Make sure the BSEL0-2 setting is: Left, Left, Left.
   5. run command to program code to QSPI:
          quartus_hps.exe -c 1 -o P -a 0x60000 hwlib_whdr.bin
   6. After programming is done, open up a serial data capture program, like window PUTTY,
	  set baudrate = 115200, 8bit data, 1bit stop, none parity, no flow control.
   7. Push reset button, you will see your program is running.

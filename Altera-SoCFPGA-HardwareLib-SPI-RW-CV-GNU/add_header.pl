#!/usr/bin/perl
use strict;
use Getopt::Long;
                                                                                                                  # setup my defaults
my $load_addr = 0x0;
my $entry_ofset = 0x0;
my $name = "mpl-img";
my $input_file = "";
my $help = 0;

sub usage {
  print "Usage must be like:  add_header.pl --load_addr 0x100000 --entry 0 --name test --file input.bin\n";
  print "You must specify load_addr, entry, and input file\n";
  die "\n"; 
}

GetOptions(
    'load_addr=o' => \$load_addr,
    'entry=o'     => \$entry_ofset,
    'name=s'    => \$name,
    'file=s'    => \$input_file,
    'help!'     => \$help,
) or die "Incorrect usage!\n";
                                                                                    
if( $help ) {                                                                                        
    usage();
} 

if ($input_file eq ""){ "Must specify input file\n"; usage();}
if ($load_addr == 0x0){print "\tusing load_addr 0x0\n";} 
if ($entry_ofset == 0x0){print "\tusing entry offset 0x0\n";}
my $out_file = $input_file;
if($out_file =~ m/\.bin$/){
  $out_file =~ s/\.bin/_whdr\.bin/; #call _whdr.bin instead of just .bin using input name
}
else{
  $out_file = "$out_file\_whdr\.bin";
}

print "adding header to $input_file and creating output file: $out_file\n";  


my @out_file;
my $wcount = 0; #word count
my $bdata; #binary data
my $data; #un-packed hex data
open(IN, "<",$input_file) or die "cant open input file $input_file\n";
open(OUT, ">",$out_file) or die "cant open output file $out_file\n";

#read infile into bdata 4 bytes at a time
while(read(IN,$bdata,4)){
        $data = unpack("h*", $bdata);                                              
        $data = uc unpack 'H*', reverse pack 'h*', $data;
        $out_file[$wcount++] = hex $data;
#        printf "0x%x\n", hex $data;
}
close IN;

## CRC calculation from  http://billauer.co.il/blog/2011/05/perl-crc32-crc-xs-module/
my $init_value =  0;
my $polynomial = 0xedb88320;  
my @lookup_table;
# first create the table
for (my $i=0; $i<256; $i++) {
  my $x = $i;
  for (my $j=0; $j<8; $j++) {
    if ($x & 1) {
      $x = ($x >> 1) ^ $polynomial;
    } else {
      $x = $x >> 1;
    }
  }
  push @lookup_table, $x;
}
#now calculate the CRC from the input file in the array byte by byte
my $crc = $init_value^0xffffffff; # init_value 0
my ($byte4, $byte3, $byte2, $byte1);
foreach my $input (@out_file){
  $byte4 = ($input & 0xff000000)>>24;
  $byte3 = ($input & 0x00ff0000) >> 16;
  $byte2 = ($input & 0x0000ff00) >> 8; 
  $byte1 = ($input & 0x000000ff);
  $crc = (($crc >> 8) & 0xffffff) ^ $lookup_table[ ($crc ^ $byte1) & 0xff ];
  $crc = (($crc >> 8) & 0xffffff) ^ $lookup_table[ ($crc ^ $byte2) & 0xff ];
  $crc = (($crc >> 8) & 0xffffff) ^ $lookup_table[ ($crc ^ $byte3) & 0xff ];
  $crc = (($crc >> 8) & 0xffffff) ^ $lookup_table[ ($crc ^ $byte4) & 0xff ];  
}
$crc = $crc ^ 0xffffffff;
printf("CRC = 0x%x\n",$crc);

# print the output file with the header appeneded at the beggining
# print header
binmode(OUT);
print OUT pack('N',0x27051956);   # mkimg magic number in BigEndian
print OUT pack('N',0);            # img header crc checksum in BigEndian
print OUT pack('N',0);            # timestamp in BigEndian
print OUT pack('N',($wcount*4));  # image data size in bytes in BigEndian
print OUT pack('N',$load_addr);   # data load address in BigEndian
print OUT pack('N',$entry_ofset); # entry in BigEndian
print OUT pack('N',$crc);         # img data crc checksum in BigEndian
print OUT pack('N',0x11020500);   # operating system, cpu architecture, image type, compression type
print length $name;
if (length $name < 32){
  print OUT $name;
  for (my $i= length $name; $i<32; $i++){
    print OUT pack('C',0);
  }
}
else {
  my @chars = split("", $name);
  my $i = 0;
  foreach my $char (@chars){ 
    if($i++ <32){
      print OUT $char;
    }
  }
}

#print the image
foreach my $img_data (@out_file){
  print OUT pack('V', $img_data);
}
close OUT; 

sub byteswap32 {
  my $input = @_[0];
  my ($b4, $b3, $b2, $b1);
  my $rev = 0;
  $b4 = ($input & 0xff000000)>>24;
  $b3 = ($input & 0x00ff0000) >> 16;
  $b2 = ($input & 0x0000ff00) >> 8; 
  $b1 = ($input & 0x000000ff);
  $rev = $b4 | ($b3<<8) | ($b2<<16) | ($b1<<24);
  $rev;
}
                                                                                                

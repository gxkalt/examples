/******************************************************************************
 *
 * Copyright 2014 Altera Corporation. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 ******************************************************************************/

// Test run for SPI EEPROM M95256

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_tmr.h"
#include "alt_globaltmr.h"
#include "alt_timers.h"
#include "alt_spi.h"
#include "alt_printf.h"

#if defined(__ARMCC_VERSION) && defined(PRINTF_UART)
#pragma import(__use_no_semihosting)

void _sys_exit(int return_code)
{
    while(1);
}
#endif

//
// !!!
// When accessing EEPROM device, note:
//
// If user wishes to keep the slave select signal remains active for the duration of the serial transfer, 
// set both serial clock phase(SCPH) and the serial clock polarity(SCPOL) configuration parameters 
// to logic 1;
//
// In our SPI initialization code for EEPROM:
//
// ALT_STATUS_CODE test_spi_cfg_prepare(ALT_SPI_DEV_t *device, uint32_t speed)
// 
// The above guideline is implemented as such.
// !!!
//

/////
//#define LOGGER

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
#endif

#ifndef MIN
#define MIN(a, b) ((a) > (b) ? (b) : (a))
#endif

/////

#define SPI_EEPROM_TIMEOUT_ACK              200000
#define SPI_EEPROM_BLOCK_DATA               32
#define SPI_EEPROM_SINGLE_BLOCK             64
#define SPI_EEPROM_CTRL_DATA_SIZE           3
#define SPI_EEPROM_ADDRESS_MAX              0xFFF

#define SPI_TEST_EEPROM_MAIN_COUNTER        25
#define SPI_TEST_EEPROM_READ_COUNTER        15

#define SPI_TEST_EEPROM_MIN_SPEED           1000000
#define SPI_TEST_EEPROM_MAX_SPEED           2000000

#define CRYSTAL_CLOCKS_PER_SEC              25000000

uint16_t test_data_array_s[SPI_EEPROM_BLOCK_DATA];
uint16_t test_data_array_r[SPI_EEPROM_BLOCK_DATA];

/////
void get_error_type(ALT_STATUS_CODE cause, char * output, size_t size)
{
    char * name;
    char buffer[32];

    switch (cause)
    {
    case ALT_E_ERROR:
        name = "ERROR";
        break;
    case ALT_E_SUCCESS:
        name = "SUCCESS";
        break;
    case ALT_E_BAD_ARG:
        name = "BAD_ARG";
        break;
    case ALT_E_ARG_RANGE:
        name = "ARG_RANGE";
        break;
    case ALT_E_TMO:
        name = "TMO";
        break;
    default:
        snprintf(buffer, sizeof(buffer), "UNKNOWN[0x%x]", (int)cause);
        name = buffer;
        break;
    }

    snprintf(output, size, "ALT_E_%s", name);
}

//
// Function set write enable to eeprom
//
ALT_STATUS_CODE test_spi_eeprom_wr_enable(ALT_SPI_DEV_t *device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint16_t send_buffer[] = {0x06};

    status = alt_spi_int_enable(device, 0x1F);
    status = alt_spi_master_tx_transfer(device, 0x1, 1, send_buffer);

    return status;
}

//
// Function readwrite data from eeprom
//
ALT_STATUS_CODE test_spi_eeprom_read_status(ALT_SPI_DEV_t *device, 
                                            uint8_t *eeprom_status)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint16_t send_buffer[] = { 0x05, 0x0 };
    uint16_t recv_buffer[] = { 0x00, 0x0 };

    status = alt_spi_master_tx_rx_transfer(device, 0x1, 2, send_buffer, recv_buffer);
    if (status == ALT_E_SUCCESS)
    {
        *eeprom_status = recv_buffer[1];
    }

    return status;
}

//
// Function wait while eeprom will ready to answer
//
ALT_STATUS_CODE test_spi_eeprom_ready(ALT_SPI_DEV_t *device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint8_t eeprom_status;
    uint32_t timeout = SPI_EEPROM_TIMEOUT_ACK;
    do 
    {
        status = test_spi_eeprom_read_status(device, &eeprom_status);
        if (status != ALT_E_SUCCESS)
        {
            break;
        }
    
        if (--timeout == 0)
        {
            status = ALT_E_TMO;
            break;
        }
    }
    while (eeprom_status & 0x1);

    return status;
}

//
// Function wait while spi bus is busy
//
ALT_STATUS_CODE test_spi_eeprom_busy_waiter(ALT_SPI_DEV_t *device)
{
    uint32_t timeout = SPI_EEPROM_TIMEOUT_ACK;
    timeout = SPI_EEPROM_TIMEOUT_ACK;
    do 
    {
        if (--timeout == 0)
        {
            return ALT_E_TMO;
        }
    }
    while (alt_spi_is_busy(device));

    return ALT_E_SUCCESS;
}

//
// Function readwrite data from eeprom
//
ALT_STATUS_CODE test_spi_eeprom_rw_data(ALT_SPI_DEV_t *device, 
                                        uint16_t * test_data_array_w,
                                        uint16_t * test_data_array_r,
                                        uint16_t address,
                                        size_t data_size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint16_t test_data_temp_w[SPI_EEPROM_SINGLE_BLOCK + SPI_EEPROM_CTRL_DATA_SIZE];
    uint16_t test_data_temp_r[SPI_EEPROM_SINGLE_BLOCK];

    uint32_t mem_offset = 0;
    uint32_t left_size = data_size;

    while (left_size)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_ready(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_wr_enable(device);
        }
        test_data_temp_w[0]= 0x2;
        test_data_temp_w[1]= ((address + mem_offset) & 0xFF00) >> 8;
        test_data_temp_w[2]= (address + mem_offset) & 0xFF;
        uint32_t send_size = MIN(SPI_EEPROM_SINGLE_BLOCK, left_size);
        for (size_t idx = 0; idx < send_size; idx ++)
        {
            test_data_temp_w[idx + SPI_EEPROM_CTRL_DATA_SIZE] = test_data_array_w[mem_offset + idx];
        }

        if (status == ALT_E_SUCCESS)
        {
            status = alt_spi_master_tx_transfer(device, 0x1, send_size + SPI_EEPROM_CTRL_DATA_SIZE, test_data_temp_w);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_ready(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = alt_spi_master_eeprom_transfer(device, 0x1, 0x3, address + mem_offset, send_size, test_data_temp_r);
        }
        
        for (size_t idx = 0; idx < send_size; idx ++)
        {
            test_data_array_r[idx + mem_offset] = test_data_temp_r[idx];
        }
        
        if (status == ALT_E_SUCCESS)
        {
            mem_offset += send_size;
            left_size -= send_size;
        }
    }

    return status;
}

//
// Function read data from eeprom
//
ALT_STATUS_CODE test_spi_eeprom_read_data(ALT_SPI_DEV_t *device, 
                                          uint16_t * test_data_array_r,
                                          uint16_t address,
                                          size_t data_size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint16_t test_data_temp_r[SPI_EEPROM_SINGLE_BLOCK];

    uint32_t mem_offset = 0;
    uint32_t left_size = data_size;

    while (left_size)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_ready(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        
        uint32_t send_size = MIN(SPI_EEPROM_SINGLE_BLOCK, left_size);
        if (status == ALT_E_SUCCESS)
        {
            status = alt_spi_master_eeprom_transfer(device, 0x1, 0x3, address + mem_offset, send_size, test_data_temp_r);
        }
        
        for (size_t idx = 0; idx < send_size; idx ++)
        {
            test_data_array_r[idx + mem_offset] = test_data_temp_r[idx];
        }

        memcpy(test_data_array_r + mem_offset, test_data_temp_r, send_size * sizeof(uint16_t));
        
        if (status == ALT_E_SUCCESS)
        {
            mem_offset += send_size;
            left_size -= send_size;
        }
    }

    return status;

}


//
// Function write data to eeprom
//
ALT_STATUS_CODE test_spi_eeprom_write_data(ALT_SPI_DEV_t *device, 
                                           uint16_t * test_data_array_w,
                                           uint16_t address,
                                           size_t data_size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint16_t test_data_temp_w[SPI_EEPROM_SINGLE_BLOCK + SPI_EEPROM_CTRL_DATA_SIZE];

    uint32_t mem_offset = 0;
    uint32_t left_size = data_size;

    while (left_size)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_ready(device);
        }
        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_wr_enable(device);
        }

        test_data_temp_w[0]= 0x2;
        test_data_temp_w[1]= ((address + mem_offset) & 0xFF00) >> 8;
        test_data_temp_w[2]= (address + mem_offset) & 0xFF;
        uint32_t send_size = MIN(SPI_EEPROM_SINGLE_BLOCK, left_size);
        memcpy(test_data_temp_w + SPI_EEPROM_CTRL_DATA_SIZE, test_data_array_w + mem_offset, send_size * sizeof(uint16_t));
        
        if (status == ALT_E_SUCCESS)
        {
            status = alt_spi_master_tx_transfer(device, 0x1, send_size + SPI_EEPROM_CTRL_DATA_SIZE, test_data_temp_w);
        }

        if (status == ALT_E_SUCCESS)
        {
            mem_offset += send_size;
            left_size -= send_size;
        }
    }

    return status;
}

//
// Function compare data in send and recieve arrays. Return ALT_E_TRUE if they are idential 
//
ALT_STATUS_CODE test_spi_eeprom_data_compare(const uint16_t *test_data_array_s, 
                                             const uint16_t *test_data_array_r)
{
#define LOGGER
#ifdef LOGGER
        ALT_PRINTF("TEST Write data: ");
        for (int index = 0; index < SPI_EEPROM_BLOCK_DATA; index++)
        {
            ALT_PRINTF(" %x ", (int)test_data_array_s[index]);
        }
        ALT_PRINTF("\n");
    
        ALT_PRINTF("TEST Read data: ");
        for (int index = 0; index < SPI_EEPROM_BLOCK_DATA; index++)
        {
            ALT_PRINTF(" %x ", (int)test_data_array_r[index]);
        }
        ALT_PRINTF("\n");
#endif
#undef LOGGER
    for (uint32_t index = 0; index < SPI_EEPROM_BLOCK_DATA; index++)
    {
        if (test_data_array_s[index] != test_data_array_r[index])
        {
            ALT_PRINTF("INFO: Mismatch found at index 0x%x.\n", (int)index);
            return ALT_E_FALSE;
        }
    }

    return ALT_E_TRUE;
}

//
// Configuration of the spi controler
//
ALT_STATUS_CODE test_spi_cfg_prepare(ALT_SPI_DEV_t *device, uint32_t speed)
{
    ALT_PRINTF("INFO: Set speed: %d Hz\n", (int)speed);

    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_SPI_CONFIG_t cfg;

    if (status == ALT_E_SUCCESS)
    {
        status = alt_spi_disable(device);
    }

    if (status == ALT_E_SUCCESS)
    {
        cfg.frame_size    = ALT_SPI_DFS_8BIT;
        cfg.frame_format  = ALT_SPI_FRF_SPI;
        cfg.clk_phase     = ALT_SPI_SCPH_TOGGLE_START;
        cfg.clk_polarity  = ALT_SPI_SCPOL_INACTIVE_HIGH;
        cfg.loopback_mode = 0;
        cfg.transfer_mode = ALT_SPI_TMOD_TXRX;

        status = alt_spi_config_set(device, &cfg);
    }

    if (status == ALT_E_SUCCESS)
    {
        //status = alt_spi_rx_sample_delay_set(device, 16);
    }
    
    if (status == ALT_E_SUCCESS)
    {
        uint32_t div, out_speed;
        alt_spi_speed_to_divider(device, speed, &div);
        alt_spi_divider_to_speed(device, &out_speed, &div);
        
        ALT_PRINTF("TEST: Set speed (divider = %d): %dHz\n", (int)div, (int)out_speed);
        alt_spi_baud_rate_set(device, div);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_spi_enable(device);
    }

    return status;
}

//
// Function prepare random data for sending to eeprom
//
ALT_STATUS_CODE test_spi_eeprom_data_prepare(uint16_t *test_data_array_s, 
                                             uint16_t *test_data_array_r)
{
    for (uint32_t index = 0; index < SPI_EEPROM_BLOCK_DATA; index++)
    {
        test_data_array_s[index]= rand() & 0xFF;
        test_data_array_r[index]= 0;
    }

    return ALT_E_SUCCESS;
}

//
// This is ping pong test for eeprom
//
ALT_STATUS_CODE test_spi_eeprom_main(ALT_SPI_DEV_t *device, uint32_t speed)
{
    ALT_PRINTF("TEST: Common EEPROM Test (speed %dHz, another configurations default).\n", 
           (int)speed);

    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint32_t counter = SPI_TEST_EEPROM_MAIN_COUNTER;
    uint32_t success_counter = 0;
    uint16_t address = 0;
    
    if (status == ALT_E_SUCCESS)
    {
        status = test_spi_cfg_prepare(device, speed);
    }
   

    while (counter--)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        test_spi_eeprom_data_prepare(test_data_array_s, test_data_array_r);

        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_busy_waiter(device);
        }
        
        address = rand() & SPI_EEPROM_ADDRESS_MAX;
        address -= address % SPI_EEPROM_SINGLE_BLOCK;

        if (status == ALT_E_SUCCESS)
        {
            status = test_spi_eeprom_rw_data(device, test_data_array_s, test_data_array_r, address, SPI_EEPROM_BLOCK_DATA);
            if (status != ALT_E_SUCCESS)
            {
                char buffer[64];
                get_error_type(status, buffer, sizeof(buffer));
                ALT_PRINTF("ERROR: During write data [%s].\n", buffer);
            }
        }

        if (status == ALT_E_SUCCESS)
        {
            if (test_spi_eeprom_data_compare(test_data_array_s, test_data_array_r) == ALT_E_FALSE)
            {
                ALT_PRINTF("ERROR: Data are different.\n");
                status = ALT_E_ERROR;
            }
        }
            
        ALT_PRINTF(".");
        if (counter % 32 == 0)
        {
            ALT_PRINTF("\n");
        }
            
        if (status == ALT_E_SUCCESS)
        {
            success_counter++;
        }
    }
    ALT_PRINTF("\n");

    if (status == ALT_E_SUCCESS)
    {
        ALT_PRINTF("RESULT: %d of %d operations are completed successfully.\n",
               (int)success_counter, SPI_TEST_EEPROM_MAIN_COUNTER);
    }

    return status;
}


//
// This is read test for eeprom
//
ALT_STATUS_CODE test_spi_eeprom_read_speed(ALT_SPI_DEV_t *device, uint32_t speed)
{
    ALT_PRINTF("TEST: Read EEPROM Speed Test (speed %d Hz).\n",
           (int)speed);
            
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint32_t counter = SPI_TEST_EEPROM_READ_COUNTER;
    uint32_t success_counter = 0;
    uint32_t start_clock = 0, diff_clock = 0, diff_time = 0, cur_clock;
    uint16_t address = 0;
    
    if (status == ALT_E_SUCCESS)
    {
        status = test_spi_cfg_prepare(device, speed);
    }

    while (counter--)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        test_spi_eeprom_data_prepare(test_data_array_s, test_data_array_r);

        address = rand() & SPI_EEPROM_ADDRESS_MAX;
        address -= address % SPI_EEPROM_SINGLE_BLOCK;

        if (status == ALT_E_SUCCESS)
        {
            start_clock = alt_gpt_counter_get(ALT_GPT_OSC1_TMR1);
            status = test_spi_eeprom_read_data(device, test_data_array_r, address, SPI_EEPROM_BLOCK_DATA);
            cur_clock = alt_gpt_counter_get(ALT_GPT_OSC1_TMR1);
            if (cur_clock > start_clock)
            {
                diff_clock =  ALT_TMR_TMR1LDCOUNT_TMR1LDCOUNT_SET_MSK - cur_clock + start_clock;
            }
            else
            { 
                diff_clock = start_clock - cur_clock;
            }
            diff_time += diff_clock / CRYSTAL_CLOCKS_PER_SEC;
            diff_clock %=  CRYSTAL_CLOCKS_PER_SEC;
            // ALT_PRINTF("TIME: During read data.(diff_time %d, diff_clock %d)\n", (int)diff_time, (int)diff_clock);
                
            if (status != ALT_E_SUCCESS)
            {
                char buffer[64];
                get_error_type(status, buffer, sizeof(buffer));
                ALT_PRINTF("ERROR: During read data [%s].\n", buffer);
            }
        }
        ALT_PRINTF(".");
        if (counter % 32 == 0)
        {
            ALT_PRINTF("\n");
        }

        if (status == ALT_E_SUCCESS)
        {
            success_counter++;
        }
    }
    ALT_PRINTF("\n");

    if (status == ALT_E_SUCCESS)
    {
        float speed_kbps = (float)SPI_TEST_EEPROM_READ_COUNTER * ((float)SPI_EEPROM_BLOCK_DATA / 1024) 
                                                                        / (float)diff_time;
        ALT_PRINTF("RESULT: %d of %d operations are completed successfully. (speed = %f kb/s)\n", 
               (int)success_counter, SPI_TEST_EEPROM_READ_COUNTER,
               speed_kbps);
    }

    return status;
}



//
// This is write test for eeprom
//
ALT_STATUS_CODE test_spi_eeprom_write_speed(ALT_SPI_DEV_t *device, uint32_t speed)
{
    ALT_PRINTF("TEST: Write EEPROM Speed Test(speed %dHz).\n", 
           (int)speed);
            
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint32_t counter = SPI_TEST_EEPROM_READ_COUNTER;
    uint32_t success_counter = 0;
    uint32_t start_clock = 0, diff_clock = 0, diff_time = 0, cur_clock;
    uint16_t address = 0;
    
    if (status == ALT_E_SUCCESS)
    {
        status = test_spi_cfg_prepare(device, speed);
    }

    while (counter--)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        test_spi_eeprom_data_prepare(test_data_array_s, test_data_array_r);

        address = rand() & SPI_EEPROM_ADDRESS_MAX;
        address -= address % SPI_EEPROM_SINGLE_BLOCK;

        if (status == ALT_E_SUCCESS)
        {
            start_clock = alt_gpt_counter_get(ALT_GPT_OSC1_TMR1);
            status = test_spi_eeprom_write_data(device, test_data_array_s, address,SPI_EEPROM_BLOCK_DATA);
            cur_clock = alt_gpt_counter_get(ALT_GPT_OSC1_TMR1);
            if (cur_clock > start_clock)
            {
                diff_clock =  ALT_TMR_TMR1LDCOUNT_TMR1LDCOUNT_SET_MSK - cur_clock + start_clock;
            }
            else
            {
                diff_clock = start_clock - cur_clock;
            }
            diff_time += diff_clock / CRYSTAL_CLOCKS_PER_SEC;
            diff_clock %=  CRYSTAL_CLOCKS_PER_SEC;
            //ALT_PRINTF("TIME: During write data.(diff_time %d, diff_clock %d)\n", (int)diff_time, (int)diff_clock);
                
            if (status != ALT_E_SUCCESS)
            {
                char buffer[64];
                get_error_type(status, buffer, sizeof(buffer));
                ALT_PRINTF("ERROR: During write data [%s].\n", buffer);
            }
        }
        ALT_PRINTF(".");
        if (counter % 32 == 0)
        {
            ALT_PRINTF("\n");
        }
            
        if (status == ALT_E_SUCCESS)
        {
            success_counter++;
        }
    }
    
    ALT_PRINTF("\n");
    if (status == ALT_E_SUCCESS)
    {
        float speed_kbps = (float)SPI_TEST_EEPROM_READ_COUNTER * ((float)SPI_EEPROM_BLOCK_DATA / 1024) 
                                                                        / (float)diff_time;
        ALT_PRINTF("RESULT: %d of %d operations are completed successfully. (speed = %f kb/s)\n", 
               (int)success_counter, SPI_TEST_EEPROM_READ_COUNTER,
               speed_kbps);
    }

    return status;
}

int main(int argc, char** argv)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    
    ALT_SPI_DEV_t device;

    /////

    ALT_PRINTF("\n");
    ALT_PRINTF("TEST: SPI Test Suite.\n");

    if (status == ALT_E_SUCCESS)
    {
        ALT_PRINTF("\n");
        ALT_PRINTF("TEST: SPI0 init.\n");
        status = alt_spi_init(ALT_SPI_SPIM0, &device);
        if (status != ALT_E_SUCCESS)
        {
            char buffer[64];
            get_error_type(status, buffer, sizeof(buffer));
            
            ALT_PRINTF("ERROR\n");
            ALT_PRINTF("TEST: Cannot init Return value (%s)\n", buffer);
        }
    }

    alt_gpt_mode_set   (ALT_GPT_OSC1_TMR1, ALT_GPT_RESTART_MODE_PERIODIC);
    alt_gpt_counter_set(ALT_GPT_OSC1_TMR1, ALT_TMR_TMR1LDCOUNT_TMR1LDCOUNT_SET_MSK);
    alt_gpt_int_disable(ALT_GPT_OSC1_TMR1);
    alt_gpt_tmr_start  (ALT_GPT_OSC1_TMR1);

    srand(12345);

    if (status == ALT_E_SUCCESS)
    {
        ALT_PRINTF("\n");

        // Ping pong test compare if write and read data are identical
        status = test_spi_eeprom_main(&device, 2000000);
    }

/*
    for (uint32_t speed = SPI_TEST_EEPROM_MIN_SPEED; speed <= SPI_TEST_EEPROM_MAX_SPEED; speed += 100000)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        if (status == ALT_E_SUCCESS)
        {
            // Read speed test for different of the bus speed
            status = test_spi_eeprom_read_speed(&device, speed);
        }
    }



    for (uint32_t speed = SPI_TEST_EEPROM_MIN_SPEED; speed <= SPI_TEST_EEPROM_MAX_SPEED; speed += 90000)
    {
        if (status != ALT_E_SUCCESS)
        {
            break;
        }

        if (status == ALT_E_SUCCESS)
        {
            // Write speed test for different of the bus speed
            status = test_spi_eeprom_write_speed(&device, speed);
        }
    }
*/

    /////

    ALT_PRINTF("TEST: SPI0 uninit.\n");
    if (alt_spi_uninit(&device) != ALT_E_SUCCESS)
    {
        ALT_PRINTF("ERROR: alt_spi_uninit() failed.\n");
        status = ALT_E_ERROR;
    }

    ALT_PRINTF("\n");

    if (status == ALT_E_SUCCESS)
    {
        ALT_PRINTF("RESULT: All tests successful.\n");
        return 0;
    }
    else
    {
        ALT_PRINTF("RESULT: Some failures detected.\n");
        return 1;
    }
}

/* enable semihosting with gcc by defining an __auto_semihosting symbol */
#if !defined(__ARMCC_VERSION) && !defined(PRINTF_UART)
int __auto_semihosting;
#endif


/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include "alt_interrupt.h"
#include "alt_globaltmr.h"
#include "alt_timers.h"
#include "alt_clock_manager.h"
#include "alt_watchdog.h"
#include "socal/hps.h"

#define TIMER_LOAD_VALUE 32000

ALT_STATUS_CODE system_init(void);
ALT_STATUS_CODE system_uninit(void);
static void clk_status_demo(void);
ALT_STATUS_CODE clk_configuration_demo(void);
ALT_STATUS_CODE clk_modification_demo(void);
ALT_STATUS_CODE clk_source_demo(void);

/******************************************************************************/
/*!
 * Initialize system
 *
 * \return      result of the function
 */
ALT_STATUS_CODE system_init(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("\n");
    printf("INFO: System Initialization.\n");

    // Initialize global timer
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Setting up Global Timer.\n");
        if(!alt_globaltmr_int_is_enabled())
        {
            status = alt_globaltmr_init();
        }
    }

    // Initialize general purpose timers
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Initializing General Purpose Timers.\n");
        status = alt_gpt_all_tmr_init();
    }

    return status;
}

/******************************************************************************/
/*!
 * Get the status of the PLLs
 *
 * \return      result of the function
 */
static void clk_status_demo(void)
{
    uint32_t clk_status = 0;
    bool     clk_bool = false;
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("\n");
    printf("INFO: Clock Status Demo.\n");

    // Getting the interrupt status register
    clk_status = alt_clk_lock_status_get();

    // Check each PLL for lock
    if(clk_status & ALT_CLKMGR_INTER_MAINPLLLOCKED_SET_MSK)
    {
        printf("INFO: Main PLL is Locked.\n");
    }

    if(clk_status & ALT_CLKMGR_INTER_PERPLLLOCKED_SET_MSK)
    {
        printf("INFO: Peripheral PLL is Locked.\n");
    }

    if(clk_status & ALT_CLKMGR_INTER_SDRPLLLOCKED_SET_MSK)
    {
        printf("INFO: SDRAM PLL is Locked.\n");
    }

    // Check if clocks are in safe mode
    clk_bool = alt_clk_is_in_safe_mode(ALT_CLK_DOMAIN_NORMAL);
    if(clk_bool == true)
    {
        printf("INFO: Clocks are in Safe Mode.\n");
    }
    else
    {
        printf("INFO: Clocks are not in Safe Mode.\n");
    }

    // Check if PLLs are is in bypass
    status = alt_clk_pll_is_bypassed(ALT_CLK_MAIN_PLL);
    switch (status)
    {
    case ALT_E_TRUE  :
        printf("INFO: Main PLL is Bypassed.\n");
        break;
    case ALT_E_FALSE :
        printf("INFO: Main PLL is not Bypassed.\n");
        break;
    default :
        printf("INFO: Bypass Bad Arg.\n");
        break;
    }

    status = alt_clk_pll_is_bypassed(ALT_CLK_PERIPHERAL_PLL);
    switch (status)
    {
    case ALT_E_TRUE  :
        printf("INFO: Peripheral PLL is Bypassed.\n");
        break;
    case ALT_E_FALSE :
        printf("INFO: Peripheral PLL is not Bypassed.\n");
        break;
    default :
        printf("INFO: Bypass Bad Arg.\n");
        break;
    }

    status = alt_clk_pll_is_bypassed(ALT_CLK_SDRAM_PLL);
    switch (status)
    {
    case ALT_E_TRUE  :
        printf("INFO: SDRAM PLL is Bypassed.\n");
        break;
    case ALT_E_FALSE :
        printf("INFO: SDRAM PLL is not Bypassed.\n");
        break;
    default :
        printf("INFO: Bypass Bad Arg.\n");
        break;
    }

    // Check if Peripheral C0 is enabled
    status = alt_clk_is_enabled(ALT_CLK_L4_MAIN);
    if(status == ALT_E_TRUE)
    {
        printf("INFO: L4 Main Clock is Enabled.\n");
    }
    else
    {
        printf("INFO: L4 Main Clock is not Enabled.\n");
    }
}

/******************************************************************************/
/*!
 * Clock Configuration
 *
 * \return      result of the function
 */
ALT_STATUS_CODE clk_configuration_demo(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    alt_freq_t clk_freq, clk_freq1, clk_freq2 = 0;
    ALT_CLK_PLL_CFG_t pll_cfg;
    ALT_CLK_t clk_type;

    // Internal divider values for the main PLL
    // Other PLLs internal dividers set to 1
    uint32_t main_pll_k_values[6] = {2, 4, 4, 1, 1, 1};

    printf("\n");
    printf("INFO: PLL Configuration Demo.\n");

    clk_freq1 = alt_clk_ext_clk_freq_get(ALT_CLK_IN_PIN_OSC1);
    printf("INFO: HPS_CLK1 clock frequency = %" PRIu32 " MHz.\n",(clk_freq1/1000000));

    clk_freq2 = alt_clk_ext_clk_freq_get(ALT_CLK_IN_PIN_OSC2);
    printf("INFO: HPS_CLK2 clock frequency = %" PRIu32 " MHz.\n",(clk_freq2/1000000));

    printf("\n");
    printf("INFO: Configuration parameters for the Main PLL.\n");
    printf("INFO: Main PLL Reference clock is HPS_CLK1.\n");

    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_pll_cfg_get(ALT_CLK_MAIN_PLL, &pll_cfg);
    }

    printf("INFO: Main PLL Reference Clock Divider   = %" PRIu32 ".\n", pll_cfg.div+1);
    printf("INFO: Main PLL VCO Multiplier            = %" PRIu32 ".\n", pll_cfg.mult+1);
    printf("INFO: Main VCO Frequency                 = %" PRIu32 " MHz.\n",(clk_freq1/(pll_cfg.div+1)*(pll_cfg.mult+1))/1000000);

    // Print out info for each PLL output
    for (int i = 0; i <6; i++)
    {
        printf("INFO: Main PLL C%i Internal Divider       = %" PRIu32 ".\n",i, main_pll_k_values[i]);
        printf("INFO: Main PLL C%i Output Divider         = %" PRIu32 ".\n",i, pll_cfg.cntrs[i]+1);
        printf("INFO: Main PLL C%i Output Frequency       = %" PRIu32 " MHz.\n", i, ((clk_freq1*(pll_cfg.mult+1))/(pll_cfg.div +1)/(pll_cfg.cntrs[i]+1))/main_pll_k_values[i]/1000000);
    }

    printf("\n");
    printf("INFO: Configuration parameters for the Peripheral PLL.\n");

    clk_type = alt_clk_source_get(ALT_CLK_PERIPHERAL_PLL);
    switch (clk_type)
    {
    case ALT_CLK_IN_PIN_OSC1 :
        clk_freq = clk_freq1;
        printf("INFO: Peripheral PLL Reference clock is HPS_CLK1.\n");
        break;
    case ALT_CLK_IN_PIN_OSC2 :
        clk_freq = clk_freq2;
        printf("INFO: Peripheral PLL Reference clock is HPS_CLK2.\n");
        break;
    default :
        clk_freq = alt_clk_ext_clk_freq_get(ALT_CLK_F2H_PERIPH_REF);
        printf("INFO: Peripheral PLL Reference clock is FPGA.\n");
        break;
    }

    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_pll_cfg_get(ALT_CLK_PERIPHERAL_PLL, &pll_cfg);
    }

    // Print out vco parameters
    printf("INFO: Peripheral PLL Reference Clock Divider   = %" PRIu32 ".\n", pll_cfg.div+1);
    printf("INFO: Peripheral PLL VCO Multiplier            = %" PRIu32 ".\n", pll_cfg.mult+1);
    printf("INFO: Peripheral VCO Frequency                 = %" PRIu32 " MHz.\n",(clk_freq/(pll_cfg.div+1)*(pll_cfg.mult+1))/1000000);

    // Print out values for each PLL output
    for (int i = 0; i <6; i++)
    {
        printf("INFO: Peripheral PLL C%i Output Divider         = %" PRIu32 ".\n",i, pll_cfg.cntrs[i]+1);
        printf("INFO: Peripheral PLL C%i Output Frequency       = %" PRIu32 " MHz.\n", i, ((clk_freq*(pll_cfg.mult+1))/(pll_cfg.div +1)/(pll_cfg.cntrs[i]+1)/1000000));
    }

    printf("\n");
    printf("INFO: Configuration parameters for the SDRAM PLL.\n");

    clk_type = alt_clk_source_get(ALT_CLK_SDRAM_PLL);
    switch (clk_type)
    {
    case ALT_CLK_IN_PIN_OSC1 :
        clk_freq = clk_freq1;
        printf("INFO: SDRAM PLL Reference clock is HPS_CLK1.\n");
        break;
    case ALT_CLK_IN_PIN_OSC2 :
        clk_freq = clk_freq2;
        printf("INFO: SDRAM PLL Reference clock is HPS_CLK2.\n");
        break;
    default :
        clk_freq = alt_clk_ext_clk_freq_get(ALT_CLK_F2H_SDRAM_REF);
        printf("INFO: SDRAM PLL Reference clock is FPGA.\n");
        break;
    }

    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_pll_cfg_get(ALT_CLK_SDRAM_PLL, &pll_cfg);
    }

    printf("INFO: SDRAM PLL Reference Clock Divider   = %" PRIu32 ".\n", pll_cfg.div+1);
    printf("INFO: SDRAM PLL VCO Multiplier            = %" PRIu32 ".\n", pll_cfg.mult+1);
    printf("INFO: SDRAM VCO Frequency                 = %" PRIu32 " MHz.\n",(clk_freq1/(pll_cfg.div+1)*(pll_cfg.mult+1))/1000000);

    // Print out divider values for each PLL output
    for (int i = 0; i <6; i++)
    {
        printf("INFO: SDRAM PLL C%i Output Divider         = %" PRIu32 ".\n",i, pll_cfg.cntrs[i]+1);
        printf("INFO: SDRAM PLL C%i Phase Shift            = -%" PRIu32 " degrees.\n",i, (pll_cfg.pshift[i] *45));
        printf("INFO: SDRAM PLL C%i Output Frequency       = %" PRIu32 " MHz.\n", i, ((clk_freq*(pll_cfg.mult+1))/(pll_cfg.div +1)/(pll_cfg.cntrs[i]+1)/1000000));
    }
    return status;
}

/******************************************************************************/
/*!
 * Let's change some peripheral PLL settings
 *
 * \return      result of the function
 */
ALT_STATUS_CODE clk_modification_demo(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_CLK_GROUP_RAW_CFG_t clk_group_raw_cfg;
    uint32_t c3_freq, c5_freq, old_per_vco_freq, new_per_vco_freq, vco_guard_band, clk_freq;
    ALT_CLK_PLL_CFG_t pll_cfg;
    ALT_CLK_t clk_type;

    printf("\n");
    printf("INFO: Clock Modification Demo.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_freq_get(ALT_CLK_PERIPHERAL_PLL_C3, &c3_freq);
        printf("INFO: Peripheral C3 Output Frequency = %" PRIu32 " MHz.\n", ((c3_freq/1000000)));
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_freq_get(ALT_CLK_PERIPHERAL_PLL_C5, &c5_freq);
        printf("INFO: Peripheral C5 Output Frequency = %" PRIu32 " MHz.\n", ((c5_freq/1000000)));
    }

    // Using the configuration structure of the PLL you can change certain parameters of the PLL
    // Changing the VCO parameters is not possible with this
    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_group_cfg_raw_get(ALT_PERIPH_PLL_CLK_GRP, &clk_group_raw_cfg);
    }

    // Changing the divider for C3 (sdmmc/nand) & C5 (user1 (FPGA) clock)
    if(status == ALT_E_SUCCESS)
    {
        clk_group_raw_cfg.clkgrp.perpllgrp.fld.pernandsdmmcclk.cnt = 24;
        clk_group_raw_cfg.clkgrp.perpllgrp.fld.s2fuser1clk.cnt = 19;
        status = alt_clk_group_cfg_raw_set(&clk_group_raw_cfg);
    }

    printf("INFO: Changed C3 and C5 output dividers.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_freq_get(ALT_CLK_PERIPHERAL_PLL_C3, &c3_freq);
        printf("INFO: Peripheral C3 Output Frequency = %" PRIu32 " MHz.\n", ((c3_freq/1000000)));
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_freq_get(ALT_CLK_PERIPHERAL_PLL_C5, &c5_freq);
        printf("INFO: Peripheral C5 Output Frequency = %" PRIu32 " MHz.\n", ((c5_freq/1000000)));
    }

    // Changing the Peripheral PLL VCO down to 800 MHz
    // get vco frequency compare with existing; if within the guard band no need to bypass
    // Update the VCO multiplier & wait for a lock interrupt
    // Then un-bypass the PLL if by-passed

    printf("\n");
    printf("INFO: Changing the Peripheral PLL VCO Frequency.\n");
    new_per_vco_freq = 800000000;

    if (status == ALT_E_SUCCESS)
    {
        status = alt_clk_pll_vco_freq_get(ALT_CLK_PERIPHERAL_PLL, &old_per_vco_freq);
    }

    if (status == ALT_E_SUCCESS)
    {
        vco_guard_band = alt_clk_pll_guard_band_get(ALT_CLK_PERIPHERAL_PLL);
        printf("INFO: VCO Guard Band = %" PRIu32 ".\n", vco_guard_band);
    }

    if(abs(new_per_vco_freq - old_per_vco_freq) > vco_guard_band)
    {
        printf("INFO: New VCO Frequency Outside of Guard Band, Need to Bypass PLL\n");
    }
    else
    {
        printf("INFO: New VCO Frequency Inside of Guard Band, No Need to Bypass PLL\n");
     }

    // VCO cfg set actually looks at the new frequency and decides if the vco needs to bypassed
    // Also waits for lock
    status = alt_clk_pll_vco_cfg_set(ALT_CLK_PERIPHERAL_PLL,64,2);

    // Now print out new Peripheral PLL Configuration
    printf("\n");
    printf("INFO: New Peripheral PLL Configuration Parameters.\n");

    clk_type = alt_clk_source_get(ALT_CLK_PERIPHERAL_PLL);
    switch (clk_type)
    {
    case ALT_CLK_IN_PIN_OSC1 :
        clk_freq = alt_clk_ext_clk_freq_get(ALT_CLK_IN_PIN_OSC1);;
         break;
    case ALT_CLK_IN_PIN_OSC2 :
        clk_freq = alt_clk_ext_clk_freq_get(ALT_CLK_IN_PIN_OSC2);;
         break;
    default :
        clk_freq = alt_clk_ext_clk_freq_get(ALT_CLK_F2H_PERIPH_REF);
         break;
    }

    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_pll_cfg_get(ALT_CLK_PERIPHERAL_PLL, &pll_cfg);
    }

    // Print out vco parameters
    printf("INFO: Peripheral PLL Reference Clock Divider   = %" PRIu32 ".\n", pll_cfg.div+1);
    printf("INFO: Peripheral PLL VCO Multiplier            = %" PRIu32 ".\n", pll_cfg.mult+1);
    printf("INFO: Peripheral VCO Frequency                 = %" PRIu32 " MHz.\n",(clk_freq/(pll_cfg.div+1)*(pll_cfg.mult+1))/1000000);

    // Print out values for each PLL output
    for (int i = 0; i <6; i++)
    {
        printf("INFO: Peripheral PLL C%i Output Divider         = %" PRIu32 ".\n",i, pll_cfg.cntrs[i]+1);
        printf("INFO: Peripheral PLL C%i Output Frequency       = %" PRIu32 " MHz.\n", i, ((clk_freq*(pll_cfg.mult+1))/(pll_cfg.div +1)/(pll_cfg.cntrs[i]+1)/1000000));
    }

    return status;
}

/******************************************************************************/
/*!
 * Changing clock sources
 *
 * \return      result of the function
 */
ALT_STATUS_CODE clk_source_demo(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t timer_value = 0;

    printf("\n");
    printf("INFO: Changing a Source Clock Demo.\n");

    // What's the period of the general purpose timers
    // Change the source and then report the period
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_mode_set(ALT_GPT_SP_TMR1, ALT_GPT_RESTART_MODE_PERIODIC);
    }

    // Set SP Timer 1 counter reset value
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_counter_set(ALT_GPT_SP_TMR1, 1024);
    }

    // Start SP Timer 1
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_tmr_start(ALT_GPT_SP_TMR1);
    }

    if(status == ALT_E_SUCCESS)
    {
        timer_value = alt_gpt_freq_get(ALT_GPT_SP_TMR1);
        printf("INFO: SP Timer 1 running at %" PRIu32 " Hz with Peripheral PLL C4 as source.\n",timer_value);
    }

    // Stop SP Timer 1
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_tmr_stop(ALT_GPT_SP_TMR1);
    }

    printf("INFO: Changing the source of SP Timer 1.\n");

    if(status == ALT_E_SUCCESS)
    {
        status = alt_clk_source_set(ALT_CLK_L4_SP, ALT_CLK_MAIN_PLL_C1);
    }

    // Start SP Timer 1
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_tmr_start(ALT_GPT_SP_TMR1);
    }

    if(status == ALT_E_SUCCESS)
    {
        timer_value = alt_gpt_freq_get(ALT_GPT_SP_TMR1);
        printf("INFO: SP Timer 1 running at %" PRIu32 " Hz with Main PLL C1 as source.\n",timer_value);
    }

    // Stop SP Timer 1
    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpt_tmr_stop(ALT_GPT_SP_TMR1);
    }

    return status;
}

/******************************************************************************/
/*!
 * Un-init system
 *
 * \return      result of the function
 */
ALT_STATUS_CODE system_uninit(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("\n");
    printf("INFO: Shutting Down System.\n");

    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_uninit();
    }

    if(status == ALT_E_SUCCESS)
    {
        status =  alt_int_global_uninit();
    }

    return status;
}

/******************************************************************************/
/*!
 * Main entry point
 *
 */
int main(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // System init
    if(status == ALT_E_SUCCESS)
    {
        status = system_init();
    }

    // Get PLL status
    if(status == ALT_E_SUCCESS)
    {
        clk_status_demo();
    }

    // Get PLL configuration
    if(status == ALT_E_SUCCESS)
    {
        clk_configuration_demo();
    }

    // Change clock parameters
    if(status == ALT_E_SUCCESS)
    {
        clk_modification_demo();
    }

    // Get PLL configuration
    if(status == ALT_E_SUCCESS)
    {
        clk_source_demo();
    }

    // System uninit
    if(status == ALT_E_SUCCESS)
    {
        status = system_uninit();
    }

    // Report status
    if (status == ALT_E_SUCCESS)
    {
        printf("\nRESULT: All tests successful.\n");
        return 0;
    }
    else
    {
        printf("\nRESULT: Some failures detected.\n");
        return 1;
    }
}


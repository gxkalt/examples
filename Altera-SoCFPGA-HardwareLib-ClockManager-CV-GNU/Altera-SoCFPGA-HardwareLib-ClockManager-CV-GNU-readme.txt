Introduction

This HardwareLibs project is meant as an example for using the Clock Manager APIs.

The project demonstrates the following Clock Manager API features:
- Obtaining Clock Manager Status
- Obtaining Clock Manager (PLL) Configuration
- Modifying Clock Manager (PLL) Configuration
- Changing a clock source

The project also uses the following additional HWLIBs APIs:
- Interrupt Management

Note: This example was developed with and tested against SoC EDS 14.0b199.

====

Target Boards:
- Altera Cyclone V SoC Development Board rev D

====

Limitations:
None  

=====

Source Files

The following are descriptions of the source and header files contained in this
project:

clkmgr_demo.c - contains all clock manager routines

  
=====

Building Example

Before running the example, the target executable first needs to be built.

1. In DS-5, build the application:
  1a. Switch to the C/C++ Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> C/C++.
  1b. In the "Project Explorer" panel, right-mouse-click 
      "Altera-SoCFPGA-HardwareLib-ClockManager-CV-GNU" and select "Build Project".

The Console panel (bottom of the UI) should detail the progress of the build
and report any warnings or errors.

=====

System Setup

1. Connect the USB to serial bridge to the host computer.
2. Connect the USB-BlasterII to the host computer.
3. Install the USB to serial bridge driver on the host computer if that driver
   is not already present. Consult documentation for the DevKit for
   instructions on installing the USB to serial bridge driver.
4. Install the USB-BlasterII driver on the host computer if that driver is not
   already present. Consult documentation for QuartusII for instructions on
   installing the USB-BlasterII driver.
5. In DS-5, configure the launch configuration.
  5a. Select the menu: Run >> Debug Configurations...
  5b. In the options on the left, expand "DS-5 Debugger" and select
      "Altera-SoCFPGA-HardwareLib-ClockManager-CV-GNU".
  5c. In the "Connections" section near the bottom, click Browse.
  5d. Select the appropriate USB-BlasterII to use. Multiple items will be
      presented if there is more than one USB-BlasterII connection attached to
      the host computer.
  5e. Click "Apply" then "OK" to apply the USB-BlasterII selection.
  5f. Click "Close" to close the Debug Configuration. Otherwise click "Debug"
      run the example in the debugger.

=====

Running the Example

After building the example and setting up the host computer system, the example
can be run by following these steps.

1. In DS-5, launch the debug configuration.
  1a. Switch to the Debug Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> DS-5 Debug.
  1b. In the "Debug Control" panel, right-mouse-click
      "Altera-SoCFPGA-HardwareLib-ClockManager-CV-GNU" and select
      "Connect to Target".

Connecting to the target takes a moment to load the preloader, run the
preloader, load the executable, and run executable. After the debug connection
is established, the debugger will pause the execution at the main() function.
Users can then set additional break points, step into, step out of, or step one
line using the DS-5 debugger. Consult documentation for DS-5 for more
information on debugging operations.

=====

Sample output

The following is a sample output:

INFO: System Initialization.
INFO: Setting up Global Timer.
INFO: Initializing General Purpose Timers.

INFO: Clock Status Demo.
INFO: Main PLL is Locked.
INFO: Peripheral PLL is Locked.
INFO: SDRAM PLL is Locked.
INFO: Clocks are not in Safe Mode.
INFO: Main PLL is not Bypassed.
INFO: Peripheral PLL is not Bypassed.
INFO: SDRAM PLL is not Bypassed.
INFO: L4 Main Clock is Enabled.

INFO: PLL Configuration Demo.
INFO: HPS_CLK1 clock frequency = 25 MHz.
INFO: HPS_CLK2 clock frequency = 25 MHz.

INFO: Configuration parameters for the Main PLL.
INFO: Main PLL Reference clock is HPS_CLK1.
INFO: Main PLL Reference Clock Divider   = 1.
INFO: Main PLL VCO Multiplier            = 64.
INFO: Main VCO Frequency                 = 1600 MHz.
INFO: Main PLL C0 Internal Divider       = 2.
INFO: Main PLL C0 Output Divider         = 1.
INFO: Main PLL C0 Output Frequency       = 800 MHz.
INFO: Main PLL C1 Internal Divider       = 4.
INFO: Main PLL C1 Output Divider         = 1.
INFO: Main PLL C1 Output Frequency       = 400 MHz.
INFO: Main PLL C2 Internal Divider       = 4.
INFO: Main PLL C2 Output Divider         = 1.
INFO: Main PLL C2 Output Frequency       = 400 MHz.
INFO: Main PLL C3 Internal Divider       = 1.
INFO: Main PLL C3 Output Divider         = 4.
INFO: Main PLL C3 Output Frequency       = 400 MHz.
INFO: Main PLL C4 Internal Divider       = 1.
INFO: Main PLL C4 Output Divider         = 4.
INFO: Main PLL C4 Output Frequency       = 400 MHz.
INFO: Main PLL C5 Internal Divider       = 1.
INFO: Main PLL C5 Output Divider         = 16.
INFO: Main PLL C5 Output Frequency       = 100 MHz.

INFO: Configuration parameters for the Peripheral PLL.
INFO: Peripheral PLL Reference clock is HPS_CLK1.
INFO: Peripheral PLL Reference Clock Divider   = 2.
INFO: Peripheral PLL VCO Multiplier            = 80.
INFO: Peripheral VCO Frequency                 = 1000 MHz.
INFO: Peripheral PLL C0 Output Divider         = 4.
INFO: Peripheral PLL C0 Output Frequency       = 250 MHz.
INFO: Peripheral PLL C1 Output Divider         = 4.
INFO: Peripheral PLL C1 Output Frequency       = 250 MHz.
INFO: Peripheral PLL C2 Output Divider         = 2.
INFO: Peripheral PLL C2 Output Frequency       = 500 MHz.
INFO: Peripheral PLL C3 Output Divider         = 20.
INFO: Peripheral PLL C3 Output Frequency       = 50 MHz.
INFO: Peripheral PLL C4 Output Divider         = 5.
INFO: Peripheral PLL C4 Output Frequency       = 200 MHz.
INFO: Peripheral PLL C5 Output Divider         = 10.
INFO: Peripheral PLL C5 Output Frequency       = 100 MHz.

INFO: Configuration parameters for the SDRAM PLL.
INFO: SDRAM PLL Reference clock is HPS_CLK1.
INFO: SDRAM PLL Reference Clock Divider   = 1.
INFO: SDRAM PLL VCO Multiplier            = 32.
INFO: SDRAM VCO Frequency                 = 800 MHz.
INFO: SDRAM PLL C0 Output Divider         = 2.
INFO: SDRAM PLL C0 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C0 Output Frequency       = 400 MHz.
INFO: SDRAM PLL C1 Output Divider         = 1.
INFO: SDRAM PLL C1 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C1 Output Frequency       = 800 MHz.
INFO: SDRAM PLL C2 Output Divider         = 2.
INFO: SDRAM PLL C2 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C2 Output Frequency       = 400 MHz.
INFO: SDRAM PLL C3 Output Divider         = 1.
INFO: SDRAM PLL C3 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C3 Output Frequency       = 800 MHz.
INFO: SDRAM PLL C4 Output Divider         = 1.
INFO: SDRAM PLL C4 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C4 Output Frequency       = 800 MHz.
INFO: SDRAM PLL C5 Output Divider         = 6.
INFO: SDRAM PLL C5 Phase Shift            = -0 degrees.
INFO: SDRAM PLL C5 Output Frequency       = 133 MHz.

INFO: Clock Modification Demo.
INFO: Peripheral C3 Output Frequency = 50 MHz.
INFO: Peripheral C5 Output Frequency = 100 MHz.
INFO: Changed C3 and C5 output dividers.
INFO: Peripheral C3 Output Frequency = 40 MHz.
INFO: Peripheral C5 Output Frequency = 50 MHz.

INFO: Changing the Peripheral PLL VCO Frequency.
INFO: VCO Guard Band = 20.
INFO: New VCO Frequency Outside of Guard Band, Need to Bypass PLL

INFO: New Peripheral PLL Configuration Parameters.
INFO: Peripheral PLL Reference Clock Divider   = 2.
INFO: Peripheral PLL VCO Multiplier            = 64.
INFO: Peripheral VCO Frequency                 = 800 MHz.
INFO: Peripheral PLL C0 Output Divider         = 4.
INFO: Peripheral PLL C0 Output Frequency       = 200 MHz.
INFO: Peripheral PLL C1 Output Divider         = 4.
INFO: Peripheral PLL C1 Output Frequency       = 200 MHz.
INFO: Peripheral PLL C2 Output Divider         = 2.
INFO: Peripheral PLL C2 Output Frequency       = 400 MHz.
INFO: Peripheral PLL C3 Output Divider         = 25.
INFO: Peripheral PLL C3 Output Frequency       = 32 MHz.
INFO: Peripheral PLL C4 Output Divider         = 5.
INFO: Peripheral PLL C4 Output Frequency       = 160 MHz.
INFO: Peripheral PLL C5 Output Divider         = 20.
INFO: Peripheral PLL C5 Output Frequency       = 40 MHz.

INFO: Changing a Source Clock Demo.
INFO: SP Timer 1 running at 78048 Hz with Peripheral PLL C4 as source.
INFO: Changing the source of SP Timer 1.
INFO: SP Timer 1 running at 195121 Hz with Main PLL C1 as source.

INFO: Shutting Down System.

RESULT: All tests successful.
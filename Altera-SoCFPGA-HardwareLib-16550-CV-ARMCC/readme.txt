Introduction

This HardwareLibs project is meant as an example for using the 16550 UART API.
This project uses a combination of the UART and interrupt API to construct a
rudimentary CLI application over the serial port. A simple memory dump
application is also implemented.

=====

Source Files

The following are descriptions of the source and header files contained in this
project:

alt_16550_buffer.{c,h}

  This file acts as a usermode circular buffer for the 16550 UART. It uses UART
  interrupts to eagerly copy data out of the UART RX FIFO. It also buffers data
  to be sent to the UART and will eagerly copy it out as the UART TX FIFO
  empties. It has added functionality to echo back characters read back on to
  the screen and to read groups of data ending in a new line in one batch.

alt_16550_prompt.{c,h}

  This file acts as a callback dispatch mechanism. Clients of this structure
  register keywords with an associated callback. Once a command is recognized,
  it dispatches that particular callback. A default callback is used to
  implement a prompt and a callback if no commands are recognized.

echo_prompt.{c,h}

  This is the echo prompt. It simply echos back anything the user types.

hwlib.c

  This file is the main glue for all the components. It initializes the
  interrupt API, UART API, buffers, and prompts. It then starts the default
  prompt, the launcher prompt.

launcher_prompt.{c,h}

  This is the launcher prompt which is used as the top level prompt. It can
  instantiate other prompts.

memory_prompt.{c,h}

  This is the memory prompt. It implements a simple memory dump application.

=====

Building Example

Before running the example, the target executable first needs to be built.

1. In DS-5, build the application:
  1a. Switch to the C/C++ Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> C/C++.
  1b. In the "Project Explorer" panel, right-mouse-click 
      "Altera-SoCFPGA-HardwareLib-16550-CV-ARMCC" and select "Build Project".

The Console panel (bottom of the UI) should detail the progress of the build
and report any warnings or errors.

=====

System Setup

1. Connect the USB to serial bridge to the host computer.
2. Connect the USB-BlasterII to the host computer.
3. Install the USB to serial bridge driver on the host computer if that driver
   is not already present. Consult documentation for the DevKit for
   instructions on installing the USB to serial bridge driver.
4. Install the USB-BlasterII driver on the host computer if that driver is not
   already present. Consult documentation for QuartusII for instructions on
   installing the USB-BlasterII driver.
5. In DS-5, configure the launch configuration.
  5a. Select the menu: Run >> Debug Configurations...
  5b. In the options on the left, expand "DS-5 Debugger" and select
      "Altera-SoCFPGA-HardwareLib-16550-CV-ARMCC-Debug".
  5c. In the "Connections" section near the bottom, click Browse.
  5d. Select the appropriate USB-BlasterII to use. Multiple items will be
      presented if there is more than one USB-BlasterII connection attached to
      the host computer.
  5e. Click "Apply" then "OK" to apply the USB-BlasterII selection.
  5f. Click "Close" to close the Debug Configuration. Otherwise click "Debug"
      run the example in the debugger.

=====

Running the Example

After building the example and setting up the host computer system, the example
can be run by following these steps.

1. Open a terminal session on the host computer to the serial port associated
   with the USB to serial bridge using a 8-N-1 configuration (8 databits per
   frame, no parity, and 1 stopbit) at 57600 baud. While not strictly
   necessary, the terminal will allow for interaction with the CLI.
2. In DS-5, launch the debug configuration.
  2a. Switch to the Debug Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> DS-5 Debug.
  2b. In the "Debug Control" panel, right-mouse-click
      "Altera-SoCFPGA-HardwareLib-16550-CV-ARMCC-Debug" and select
      "Connect to Target".

Connecting to the target takes a moment to load the preloader, run the
preloader, load the executable, and run executable. After the debug connection
is established, the debugger will pause the execution at the main() function.
Users can then set additional break points, step into, step out of, or step one
line using the DS-5 debugger. Consult documentation for DS-5 for more
information on debugging operations.

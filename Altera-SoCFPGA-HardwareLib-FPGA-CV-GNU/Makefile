#
# Copyright Altera 2013,2014
# All Rights Reserved.
#

SEMIHOSTING ?= 1

SOCEDS_ROOT ?= $(SOCEDS_DEST_ROOT)
HWLIBS_ROOT = $(SOCEDS_ROOT)/ip/altera/hps/altera_hps/hwlib

HWLIBS_SRC  := alt_address_space.c alt_bridge_manager.c alt_cache.c alt_clock_manager.c alt_dma.c alt_dma_program.c alt_fpga_manager.c
EXAMPLE_SRC := hwlib.c
C_SRC       := $(EXAMPLE_SRC) $(HWLIBS_SRC)

MULTILIBFLAGS := -mcpu=cortex-a9 -mfloat-abi=softfp -mfpu=neon
CFLAGS  := -g -O0 -Wall -Werror -std=c99 $(MULTILIBFLAGS) -I$(HWLIBS_ROOT)/include -DALT_FPGA_ENABLE_DMA_SUPPORT=1

CROSS_COMPILE := arm-altera-eabi-
CC := $(CROSS_COMPILE)gcc
LD := $(CROSS_COMPILE)g++
NM := $(CROSS_COMPILE)nm
OD := $(CROSS_COMPILE)objdump
OC := $(CROSS_COMPILE)objcopy

RM := rm -rf
CP := cp -f

ifeq ($(SEMIHOSTING),0)
CFLAGS := $(CFLAGS) -DPRINTF_UART
LINKER_SCRIPT := cycloneV-dk-ram.ld
EXAMPLE_SRC := $(EXAMPLE_SRC) alt_printf.c
HWLIBS_SRC := $(HWLIBS_SRC) alt_16550_uart.c
else
LINKER_SCRIPT := cycloneV-dk-ram-hosted.ld
CFLAGS := $(CFLAGS) -DPRINTF_HOST
endif

LDFLAGS := -T$(LINKER_SCRIPT) $(MULTILIBFLAGS)

C_SRC := $(EXAMPLE_SRC) $(HWLIBS_SRC)

ELF ?= $(basename $(firstword $(C_SRC))).axf
SPL := u-boot-spl.axf
OBJ := $(patsubst %.c,%.o,$(C_SRC))

.PHONY: all
all: $(ELF) $(SPL)

.PHONY: clean
clean:
	$(RM) $(ELF) $(SPL) $(OBJ) *.objdump *.map *.rbf $(HWLIBS_SRC) soc_system* cpf_option.txt

%.c: $(HWLIBS_ROOT)/src/hwmgr/%.c
	$(CP) $< $@

soc_system.sof: $(CYC5SOC_FW_PATH)/soc_system.sof
	$(CP) $< $@

# No Data Compression
soc_system_nodc.rbf: soc_system.sof
	quartus_cpf -c $< $@

# With Data Compression
soc_system_dc.rbf: soc_system.sof
	$(RM) cpf_option.txt
	echo bitstream_compression=on > cpf_option.txt
	quartus_cpf -c -o cpf_option.txt $< $@
	$(RM) cpf_option.txt

soc_system_nodc.o: soc_system_nodc.rbf
	$(OC) --input-target binary --output-target elf32-little --alt-machine-code 40 $< $@

soc_system_dc.o: soc_system_dc.rbf
	$(OC) --input-target binary --output-target elf32-little --alt-machine-code 40 $< $@

$(SPL): $(SOCEDS_ROOT)/examples/hardware/cv_soc_devkit_ghrd/software/preloader/uboot-socfpga/spl/u-boot-spl
	$(CP) $< $@
	$(OD) -d $@ > $@.objdump

$(OBJ): %.o: %.c Makefile
	$(CC) $(CFLAGS) -c $< -o $@

$(ELF): $(OBJ) soc_system_dc.o
	$(LD) $(LDFLAGS) $(OBJ) soc_system_dc.o -o $@
	$(OD) -d $@ > $@.objdump
	$(NM) $@ > $@.map

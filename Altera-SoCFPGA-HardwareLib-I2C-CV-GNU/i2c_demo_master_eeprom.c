/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "i2c_demo.h"

#define I2C_EEPROM_ADDRESS                  (0xA2 >> 1)    // I2C address of EEPROM module
#define I2C_EEPROM_SPEED                    100000         // I2C bus speed for accessing EEPROM module
#define I2C_EEPROM_BLOCK_SIZE               4096           // EEPROM module size in bytes
#define I2C_EEPROM_PAGE_SIZE                32             // EEPROM page size in bytes
#define I2C_EEPROP_WRITE_TIME_US            5000           // Maximum EEPROM write duration

// Init I2C module
static ALT_STATUS_CODE i2c_init(ALT_I2C_DEV_t * device);

// Read from EEPROM
static ALT_STATUS_CODE eeprom_read(ALT_I2C_DEV_t * device, void * buffer, uint16_t address, uint16_t size);

// Write to EEPROM
static ALT_STATUS_CODE eeprom_write(ALT_I2C_DEV_t * device, uint16_t address, void * buffer, uint16_t size);

// Write single byte to EEPROM
static ALT_STATUS_CODE eeprom_write_byte(ALT_I2C_DEV_t * device, uint16_t address, uint8_t data);

// Write full page to EEPROM
static ALT_STATUS_CODE eeprom_write_page(ALT_I2C_DEV_t * device, uint16_t address, uint8_t * data);

// Demonstrate EEPROM
static ALT_STATUS_CODE eeprom_demo(ALT_I2C_DEV_t * device);


/******************************************************************************/
/*!
 * Init I2C Module.
 *
 * \param       device I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE i2c_init(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_MASTER_CONFIG_t cfg;
    uint32_t speed;

    // Init I2C module
    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Init I2C module.\n");
        status = alt_i2c_init(ALT_I2C_I2C0, device);
    }

    // Enable I2C module
    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Enable I2C module.\n");
        status = alt_i2c_enable(device);
    }

    // Configure I2C module
    printf("INFO: Configuring I2C parameters.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_get(device, &cfg);
    }
    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_set(device, &cfg, I2C_EEPROM_SPEED);
    }
    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_get(device, &cfg, &speed);
        printf("INFO: Using I2C speed = %d Hz.\n", (int)speed);
    }
    if(status == ALT_E_SUCCESS)
    {
        cfg.addr_mode = ALT_I2C_ADDR_MODE_7_BIT;
        cfg.restart_enable = ALT_E_TRUE;
        cfg.fs_spklen = 2;
        status = alt_i2c_master_config_set(device, &cfg);
    }

    return status;
}

/******************************************************************************/
/*!
 * Read from EEPROM memory
 *
 * \param       device I2C device.
 * \param         buffer where to put read data
 * \param         address where to read data from
 * \param         size size of the data to be read
 * \return      result of the function
 */
static ALT_STATUS_CODE eeprom_read(ALT_I2C_DEV_t * device, void * buffer, uint16_t address, uint16_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Write just the address to the EEPROM, to set the pointer correctly
    if(status == ALT_E_SUCCESS)
    {
        uint8_t addr_buffer[2];

        printf("INFO: Setting EEPROM pointer address to 0x%04x.\n", (int)address);
        addr_buffer[0] = (address >> 8) & 0xFF;
        addr_buffer[1] = (address >> 0) & 0xFF;
        status = alt_i2c_master_transmit(device,
                                         addr_buffer,
                                         sizeof(addr_buffer),
                                         ALT_E_FALSE,
                                         ALT_E_TRUE);
    }

    // Read all the data from the EEPROM
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Sequentially reading %d bytes from EEPROM.\n", (int)size);
        status = alt_i2c_master_receive(device,
                                        buffer,
                                        size,
                                        ALT_E_FALSE,
                                        ALT_E_TRUE);
    }

    return status;
}

/******************************************************************************/
/*!
 * Write single byte to EEPROM memory
 *
 * \param       device I2C device.
 * \param         address where to write the byte
 * \param         data byte to be written
 * \return      result of the function
 */
static ALT_STATUS_CODE eeprom_write_byte(ALT_I2C_DEV_t * device, uint16_t address, uint8_t data)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Writing single byte at address = 0x%04x.\n", (int)address);

    // Issue the single byte write
    if(status == ALT_E_SUCCESS)
    {
        uint8_t send_buffer[3];
        uint16_t send_size = 0;

        // Fill the address in the send buffer
        send_buffer[send_size++] = (address >> 8) & 0xFF;
        send_buffer[send_size++] = (address >> 0) & 0xFF;

        // Fill in the data in the send buffer
        send_buffer[send_size++] = data;

        // Send the data to the device
        status = alt_i2c_master_transmit(device,
                                         send_buffer,
                                         send_size,
                                         ALT_E_FALSE,
                                         ALT_E_TRUE);

    }

    // Wait for completion of operation
    if(status == ALT_E_SUCCESS)
    {
        delay_us(I2C_EEPROP_WRITE_TIME_US);
    }

    return status;
}

/******************************************************************************/
/*!
 * Write full page to EEPROM memory
 *
 * \param       device I2C device.
 * \param         address where to write the page. Needs to be page-aligned.
 * \param         data 32 bytes to be written
 * \return      result of the function
 */
static ALT_STATUS_CODE eeprom_write_page(ALT_I2C_DEV_t * device, uint16_t address, uint8_t * data)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Writing 32 byte page at address = 0x%04x.\n", (int)address);

    // Issue the page write
    if(status == ALT_E_SUCCESS)
    {
        uint8_t send_buffer[2 + I2C_EEPROM_PAGE_SIZE];
        uint16_t send_size = 0;

        // Fill the address in the send buffer
        send_buffer[send_size++] = (address >> 8) & 0xFF;
        send_buffer[send_size++] = (address >> 0) & 0xFF;

        // Fill in the data
        for(int i=0; i<I2C_EEPROM_PAGE_SIZE; i++)
        {
            send_buffer[send_size++] = data[i];
        }

        // Send the data to the device
        status = alt_i2c_master_transmit(device,
                                         send_buffer,
                                         send_size,
                                         ALT_E_FALSE,
                                         ALT_E_TRUE);
        // Make sure the FIFO is empty
        if(status == ALT_E_SUCCESS)
        {
            while (alt_i2c_tx_fifo_is_empty(device) == ALT_E_FALSE);
        }
    }

    // Wait for completion of operation
    if(status == ALT_E_SUCCESS)
    {
        delay_us(I2C_EEPROP_WRITE_TIME_US);
    }

    return ALT_E_SUCCESS;
}

/******************************************************************************/
/*!
 * Generic EEPROM write function.
 *
 * \param       device I2C device.
 * \param         address where to write the data
 * \param         data bytes to be written
 * \param         size how many bytes to be written
 * \return      result of the function
 */
static ALT_STATUS_CODE eeprom_write(ALT_I2C_DEV_t * device, uint16_t address, void * data, uint16_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint16_t crt_address = address;
    uint8_t * crt_data = (uint8_t*)data;
    uint16_t crt_size = size;

    // Write single bytes until we get to page boundary
    while((status == ALT_E_SUCCESS) &&
          ((crt_address % I2C_EEPROM_PAGE_SIZE) != 0) &&
          (crt_size > 0))
    {
        status = eeprom_write_byte(device, crt_address, *crt_data);
        crt_size--;
        crt_data++;
        crt_address++;
    }

    // Write whole pages
    while((status == ALT_E_SUCCESS) &&
          ((crt_address % I2C_EEPROM_PAGE_SIZE) == 0) &&
          (crt_size >= I2C_EEPROM_PAGE_SIZE))
    {
        status = eeprom_write_page(device, crt_address, crt_data);
        crt_size -= I2C_EEPROM_PAGE_SIZE;
        crt_data += I2C_EEPROM_PAGE_SIZE;
        crt_address += I2C_EEPROM_PAGE_SIZE;
    }

    // Write leftover single bytes at the end
    while((status == ALT_E_SUCCESS) && (crt_size > 0))
    {
        status = eeprom_write_byte(device, crt_address, *crt_data);
        crt_size--;
        crt_data++;
        crt_address++;
    }

    return status;
}

/******************************************************************************/
/*!
 * EEPROM demo function.
 *
 * \param       device I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE eeprom_demo(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint8_t read_buffer[I2C_EEPROM_BLOCK_SIZE];
    uint8_t write_buffer[I2C_EEPROM_BLOCK_SIZE];
    uint16_t test_size;
    uint16_t test_address;

    // Generate random address and size
    test_address = rand() % I2C_EEPROM_BLOCK_SIZE;
    test_size = rand() % (I2C_EEPROM_BLOCK_SIZE - test_address) + 1;

    printf("INFO: Using random address = 0x%04x, size = %d bytes.\n", (int)test_address, (int)test_size);

    // Fill the write buffer with random data
    for(int i=0; i<test_size; i++)
    {
        write_buffer[i] = (uint8_t)rand();
    }

    // Set target EEPROM I2C address
    if(status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_target_set(device, I2C_EEPROM_ADDRESS);
    }


    // Write to EEPROM
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Writing to EEPROM address 0x%04x, size = %d bytes.\n", (int)test_address, (int)test_size);
        status = eeprom_write(device, test_address, write_buffer, test_size);
    }

    // Read from EEPROM
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Reading from EEPROM address 0x%04x, size = %d bytes.\n", (int)test_address, (int)test_size);
        memset(read_buffer, 0, test_size);
        status = eeprom_read(device, read_buffer, test_address, test_size);
    }

    // Compare the results
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Comparing written data with read back data.\n");
        if(memcmp(read_buffer, write_buffer, test_size) != 0)
        {
            status = ALT_E_ERROR;
        }
    }

    return status;
}

/******************************************************************************/
/*!
 * EEPROM demo wrapper.
 *
 * \return      result of the function
 */
ALT_STATUS_CODE i2c_demo_master_eeprom(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_DEV_t device;


    printf("INFO: I2C Demo Master EEPROM started.\n");

    // Init I2C module
    if (status == ALT_E_SUCCESS)
    {
        status = i2c_init(&device);
    }

    // Demonstrate LCD operation
    if(status == ALT_E_SUCCESS)
    {
        status = eeprom_demo(&device);
    }

    // I2C Cleanup
    if(status == ALT_E_SUCCESS)
    {
        alt_i2c_disable(&device);
        alt_i2c_uninit(&device);
    }

    // Display status
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: I2C Demo Master EEPROM succeeded.\n\n");
    }
    else
    {
        printf("INFO: I2C Demo Master EEPROM failed.\n\n");
    }

    return status;
}

/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef I2C_DEMO_H
#define I2C_DEMO_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include "alt_pt.h"
#include "alt_cache.h"
#include "alt_interrupt.h"
#include "alt_i2c.h"
#include "alt_qspi_private.h"
#include "alt_globaltmr.h"
#include "alt_timers.h"
#include "socal/hps.h"
#include "socal/socal.h"

// Determine the minimum of two values
#define MIN(a, b) ((a) > (b) ? (b) : (a))

// Delay function
void delay_us(uint32_t us);

// Demonstrates writing to NHD-0216K3Z-NSW-BBW-V3 I2C LCD Module on Development board
ALT_STATUS_CODE i2c_demo_master_lcd(void);

// Demonstrates using 24LC32A I2C EEPROM Chip on Development Board
ALT_STATUS_CODE i2c_demo_master_eeprom(void);

// Demonstrate both master and slave operation
ALT_STATUS_CODE i2c_demo_master_slave(void);

#endif

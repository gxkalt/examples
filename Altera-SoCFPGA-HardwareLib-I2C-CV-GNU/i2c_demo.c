/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "i2c_demo.h"

// System initialization
ALT_STATUS_CODE system_init(void);

// System shutdown
ALT_STATUS_CODE system_uninit(void);

// Delay function
void delay_us(uint32_t us);

/******************************************************************************/
/*!
 * System Initialization
 * \return      result of the function
 */
ALT_STATUS_CODE system_init(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: System Initialization.\n");

    // Initialize global timer
    if(status == ALT_E_SUCCESS)
    {
        if(!alt_globaltmr_int_is_enabled())
        {
            status = alt_globaltmr_init();
        }
        // printf duration is not completely deterministic, and will induce some element of randomness
        printf("INFO: Setting up Global Timer.\n");
    }

    // Initialize random number generator
    if(status == ALT_E_SUCCESS)
    {
        int random_seed = alt_globaltmr_counter_get_low32();
        printf("INFO: Using random seed = 0x%04x.\n", random_seed);
        srand(random_seed);
    }

    printf("\n");

    return status;
}

/******************************************************************************/
/*!
 * System Cleanup
 *
 * \return      result of the function
 */
ALT_STATUS_CODE system_uninit(void)
{
    printf("INFO: System shutdown.\n");
    printf("\n");
    return ALT_E_SUCCESS;
}

/******************************************************************************/
/*!
 * Delay function
 *
 * \param      us delay interval
 */
void delay_us(uint32_t us)
{
    uint64_t start_time = alt_globaltmr_get64();
    uint32_t timer_prescaler = alt_globaltmr_prescaler_get() + 1;
    uint64_t end_time;
    alt_freq_t timer_clock;

    alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &timer_clock);
    end_time = start_time + us * ((timer_clock / timer_prescaler) / 1000000);

    while(alt_globaltmr_get64() < end_time)
    {
    }
}


/******************************************************************************/
/*!
 * Program entrypoint
 *
 * \return result of the program
 */
int main(void)
{

    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // System init
    if(status == ALT_E_SUCCESS)
    {
        status = system_init();
    }

    // Demo LCD
    if(status == ALT_E_SUCCESS)
    {
        status = i2c_demo_master_lcd();
    }

    // Demo EEPROM
    if(status == ALT_E_SUCCESS)
    {
        status = i2c_demo_master_eeprom();
    }

    // Demo master and slave
    if(status == ALT_E_SUCCESS)
    {
        status = i2c_demo_master_slave();
    }

    // System uninit
    if(status == ALT_E_SUCCESS)
    {
        status = system_uninit();
    }

    // Report status
    if (status == ALT_E_SUCCESS)
    {
        printf("RESULT: All tests successful.\n");
        return 0;
    }
    else
    {
        printf("RESULT: Some failures detected.\n");
        return 1;
    }

    return 0;
}

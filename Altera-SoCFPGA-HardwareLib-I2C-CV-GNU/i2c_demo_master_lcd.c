/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "i2c_demo.h"

#define LCD_I2C_ADDRESS      (0x50 >> 1)    // I2C address of LCD module
#define LCD_I2C_SPEED        40000          // I2C bus speed for accessing LCD module
#define LCD_ESCAPE_CHAR      0xfe           // Escape character used to prefix commands
#define LCD_POS_1ST_LINE     0x00           // LCD cursor position for 1st line
#define LCD_POS_2ND_LINE     0x40           // LCD cursor position for 2nd line
#define LCD_PRINT_DELAY_US   500            // Delay in us after printing text on LCD

// Commands supported by the LCD display
typedef enum
{
    LCD_COMMAND_DISPLAY_ON = 0,
    LCD_COMMAND_DISPLAY_OFF,
    LCD_COMMAND_SET_CURSOR,
    LCD_COMMAND_CURSOR_HOME,
    LCD_COMMAND_UNDERLINE_CURSOR_ON,
    LCD_COMMAND_UNDERLINE_CURSOR_OFF,
    LCD_COMMAND_MOVE_CURSOR_LEFT_ONE_PLACE,
    LCD_COMMAND_MOVE_CURSOR_RIGHT_ONE_PLACE,
    LCD_COMMAND_BLINKING_CURSOR_ON,
    LCD_COMMAND_BLINKING_CURSOR_OFF,
    LCD_COMMAND_BACKSPACE,
    LCD_COMMAND_CLEAR_SCREEN,
    LCD_COMMAND_SET_CONTRAST,
    LCD_COMMAND_SET_BACKLIGHT_BRIGHTNESS,
    LCD_COMMAND_LOAD_CUSTOM_CHARACTER,
    LCD_COMMAND_MOVE_DISPLAY_ONE_PLACE_TO_THE_LEFT,
    LCD_COMMAND_MOVE_DISPLAY_ONE_PLACE_TO_THE_RIGHT,
    LCD_COMMAND_CHANGE_RS_232_BAUD_RATE,
    LCD_COMMAND_CHANGE_I2C_ADDRESS,
    LCD_COMMAND_DISPLAY_FIRMWARE_VERSION_NUMBER,
    LCD_COMMAND_DISPLAY_RS_232_BAUD_RATE,
    LCD_COMMAND_DISPLAY_I2C_ADDRESS,
} LCD_COMMAND_T;

// Command description
typedef struct
{
    uint8_t command_code;
    uint8_t no_parameters;
    uint16_t execution_time_us;
} LCD_COMMAND_DESC_T;


// Descriptions for all supported commands
static LCD_COMMAND_DESC_T lcd_command_descriptions[] =
{
    {0x41, 0, 100},  // LCD_COMMAND_DISPLAY_ON,
    {0x42, 0, 100},  // LCD_COMMAND_DISPLAY_OFF,
    {0x45, 1, 100},  // LCD_COMMAND_SET_CURSOR,
    {0x46, 0, 1500}, // LCD_COMMAND_CURSOR_HOME,
    {0x47, 0, 1500}, // LCD_COMMAND_UNDERLINE_CURSOR_ON,
    {0x48, 0, 1500}, // LCD_COMMAND_UNDERLINE_CURSOR_OFF,
    {0x49, 0, 100},  // LCD_COMMAND_MOVE_CURSOR_LEFT_ONE_PLACE,
    {0x4A, 0, 100},  // LCD_COMMAND_MOVE_CURSOR_RIGHT_ONE_PLACE,
    {0x4B, 0, 100},  // LCD_COMMAND_BLINKING_CURSOR_ON,
    {0x4C, 0, 100},  // LCD_COMMAND_BLINKING_CURSOR_OFF,
    {0x4E, 0, 100},  // LCD_COMMAND_BACKSPACE,
    {0x51, 0, 1500}, // LCD_COMMAND_CLEAR_SCREEN,
    {0x52, 1, 500},  // LCD_COMMAND_SET_CONTRAST,
    {0x53, 1, 100},  // LCD_COMMAND_SET_BACKLIGHT_BRIGHTNESS,
    {0x54, 9, 200},  // LCD_COMMAND_LOAD_CUSTOM_CHARACTER,
    {0x55, 0, 100},  // LCD_COMMAND_MOVE_DISPLAY_ONE_PLACE_TO_THE_LEFT,
    {0x56, 0, 100},  // LCD_COMMAND_MOVE_DISPLAY_ONE_PLACE_TO_THE_RIGHT,
    {0x61, 1, 3000}, // LCD_COMMAND_CHANGE_RS_232_BAUD_RATE,
    {0x62, 1, 3000}, // LCD_COMMAND_CHANGE_I2C_ADDRESS,
    {0x70, 0, 4000}, // LCD_COMMAND_DISPLAY_FIRMWARE_VERSION_NUMBER,
    {0x71, 0, 10000},// LCD_COMMAND_DISPLAY_RS_232_BAUD_RATE,
    {0x72, 0, 4000}, // LCD_COMMAND_DISPLAY_I2C_ADDRESS,
};

// Init I2C
static ALT_STATUS_CODE i2c_init(ALT_I2C_DEV_t * device);

// Send Command to LCD
static ALT_STATUS_CODE lcd_send_command(ALT_I2C_DEV_t * device, LCD_COMMAND_T command, uint8_t * params);

// Print text to LCD
static ALT_STATUS_CODE lcd_print_text(ALT_I2C_DEV_t * device, char * text);

// Demonstrate LCD
static ALT_STATUS_CODE lcd_demo(ALT_I2C_DEV_t * device);

/******************************************************************************/
/*!
 * Send command to LCD display
 *
 * \param       device  I2C device.
 * \param       command opcode of command to be sent
 * \param       params command parameters or NULL for no parameters
 * \return      result of the function
 */
static ALT_STATUS_CODE lcd_send_command(ALT_I2C_DEV_t * device, LCD_COMMAND_T command, uint8_t * params)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    LCD_COMMAND_DESC_T command_description = lcd_command_descriptions[(int)command];
    uint8_t data[10];
    uint8_t data_size = 0;

    data[data_size++] = LCD_ESCAPE_CHAR;
    data[data_size++] = command_description.command_code;
    for(int i=0; i<command_description.no_parameters; i++)
    {
        data[data_size++] = params[i];
    }

    status = alt_i2c_master_transmit(device, data, data_size , ALT_E_FALSE, ALT_E_TRUE);
    delay_us(command_description.execution_time_us);

    return status;
}


/******************************************************************************/
/*!
 * Display text on LCD.
 *
 * \param       device I2C device.
 * \param       text  text to be displayed.
 * \return      result of the function
 */
static ALT_STATUS_CODE lcd_print_text(ALT_I2C_DEV_t * device, char * text)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    for(int i=0; i<strlen(text); i++)
    {
        status = alt_i2c_master_transmit(device, &text[i], 1, ALT_E_FALSE, ALT_E_TRUE);
        if(status != ALT_E_SUCCESS)
        {
            break;
        }
        delay_us(LCD_PRINT_DELAY_US);
    }

    return status;
}

/******************************************************************************/
/*!
 * Initialize the I2C module.
 *
 * \param       I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE i2c_init(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_MASTER_CONFIG_t cfg;
    uint32_t speed;

    // Init I2C module
    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Init I2C module.\n");
        status = alt_i2c_init(ALT_I2C_I2C0, device);
    }

    // Enable I2C module
    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Enable I2C module.\n");
        status = alt_i2c_enable(device);
    }

    // Configure I2C module
    printf("INFO: Configuring I2C parameters.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_get(device, &cfg);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_get(device, &cfg, &speed);
        printf("INFO: Current I2C speed = %d Hz.\n", (int)speed);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_set(device, &cfg, LCD_I2C_SPEED);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_get(device, &cfg, &speed);
        printf("INFO: New I2C speed = %d Hz.\n", (int)speed);
    }

    if(status == ALT_E_SUCCESS)
    {
        cfg.addr_mode = ALT_I2C_ADDR_MODE_7_BIT;
        cfg.restart_enable = ALT_E_TRUE;
        status = alt_i2c_master_config_set(device, &cfg);
    }

    if(status == ALT_E_SUCCESS)
    {
        alt_i2c_sda_hold_time_set(device, 8);
    }

    return status;
}

/******************************************************************************/
/*!
 * Demonstrate using I2C to drive the LCD display.
 *
 */
static ALT_STATUS_CODE lcd_demo(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint8_t param;

    // Set target display I2C address
    if(status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_target_set(device, LCD_I2C_ADDRESS);
    }

    // Turn display on
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Turning display on.\n");
        status = lcd_send_command(device, LCD_COMMAND_DISPLAY_ON, NULL);
    }

    // Clear screen
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Clearing screen.\n");
        status = lcd_send_command(device, LCD_COMMAND_CLEAR_SCREEN, NULL);
    }

    // Turn cursor on
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Turning cursor on.\n");
        status = lcd_send_command(device, LCD_COMMAND_BLINKING_CURSOR_ON, NULL);
    }

    // Display some text
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Printing text.\n");
        status = lcd_print_text(device, "Hello ... ");
    }

    // Display some random number
    if(status == ALT_E_SUCCESS)
    {
        char str[5];
        printf("INFO: Printing random number.\n");
        sprintf(str, "%x", (int) rand() & 0xFFFF);
        status = lcd_print_text(device, str);
    }

    // Go to 2nd line
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Moving cursor to the 2nd line.\n");
        param = LCD_POS_2ND_LINE;
        status = lcd_send_command(device, LCD_COMMAND_SET_CURSOR, &param);
    }

    // Display some text
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Printing text.\n");
        status = lcd_print_text(device, "good bye!");
    }

    return status;
}

/******************************************************************************/
/*!
 * LCD demo wrapper.
 *
 * \return      result of the function
 */
ALT_STATUS_CODE i2c_demo_master_lcd(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_DEV_t device;


    printf("INFO: I2C Demo Master LCD started.\n");

    // Init I2C module
    if (status == ALT_E_SUCCESS)
    {
        status = i2c_init(&device);
    }

    // Demonstrate LCD operation
    if(status == ALT_E_SUCCESS)
    {
        status = lcd_demo(&device);
    }

    // I2C Cleanup
    if(status == ALT_E_SUCCESS)
    {
        alt_i2c_disable(&device);
        alt_i2c_uninit(&device);
    }

    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: I2C Demo Master LCD succeeded.\n\n");
    }
    else
    {
        printf("INFO: I2C Demo Master LCD failed.\n\n");
    }

    return status;
}

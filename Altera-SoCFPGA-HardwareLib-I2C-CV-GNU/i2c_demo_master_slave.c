/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "i2c_demo.h"

#define I2C_MASTER_DEVICE     ALT_I2C_I2C0                // Device to be used as master
#define I2C_SLAVE_DEVICE      ALT_I2C_I2C1                // Device to be used as slave
#define I2C_SLAVE_ADDRESS     0x43                        // I2C address of the slave device
#define I2C_INTERRUPT_SLAVE   ALT_INT_INTERRUPT_I2C1_IRQ  // Slave IRQ
#define I2C_MASTER_SPEED      40000                       // I2C bus speed for master
#define I2C_BUFFER_SIZE       64                          // Size of data to be exchanged, up to 64bytes


// Context data structure used by the I2C slave interrupt
typedef struct I2C_SLAVE_CONTEXT_s
{
    ALT_I2C_DEV_t * device;             // slave device
    volatile ALT_STATUS_CODE status;    // current status
    uint8_t buffer[I2C_BUFFER_SIZE];    // data buffer
    uint32_t count;                     // data buffer fill level
}
I2C_SLAVE_CONTEXT_t;

// Initialize interrupt subsystem and setup I2C interrupt
static ALT_STATUS_CODE socfpga_int_setup(ALT_INT_INTERRUPT_t int_id, ALT_INT_TRIGGER_t trigger);

// Cleanup interrupt subsystem and the specific I2C interrupt
static ALT_STATUS_CODE socfpga_int_cleanup(ALT_INT_INTERRUPT_t int_id);

// Initialize the master I2C module.
static ALT_STATUS_CODE socfpga_i2c_setup_master(ALT_I2C_DEV_t * device);

// Initialize the slave I2C module.
static ALT_STATUS_CODE socfpga_i2c_setup_slave(ALT_I2C_DEV_t * device);

// Cleanup I2C device
static ALT_STATUS_CODE socfpga_i2c_cleanup(ALT_I2C_DEV_t * device);

// ISR callback for the slave I2C device
static void i2c_slave_isr_callback(uint32_t icciar, void * context);

/******************************************************************************/
/*!
 * Initialize interrupt subsystem and setup I2C interrupt.
 *
 * \param       int_id interrupt id
 * \param       trigger type of trigger to be used for the interrupt
 * \return      result of the function
 */
static ALT_STATUS_CODE socfpga_int_setup(ALT_INT_INTERRUPT_t int_id, ALT_INT_TRIGGER_t trigger)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Setting up I2C interrupt.\n");

    // Initialize global interrupts
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_init();
    }

    // Initialize CPU interrupts
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_init();
    }

    // Set interrupt distributor target
    if (status == ALT_E_SUCCESS)
    {
        int target = 0x3;
        status = alt_int_dist_target_set(int_id, target);
    }

    // Set interrupt trigger type
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_dist_trigger_set(int_id, trigger);
    }

    // Enable interrupt at the distributor level
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_dist_enable(int_id);
    }

    // Enable CPU interrupts
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_enable();
    }

    // Enable global interrupts
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_enable();
    }

    return status;
}

/******************************************************************************/
/*!
 * Cleanup interrupt subsystem and the specific I2C interrupt.
 *
 * \param       int_id interrupt id
 * \return      result of the function
 */
static ALT_STATUS_CODE socfpga_int_cleanup(ALT_INT_INTERRUPT_t int_id)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Cleaning up I2C interrupt.\n");

    // Disable global interrupts
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_global_disable();
    }

    // Disable CPU interrupts
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_disable();
    }

    // Disable I2C interrupt at the distributor level
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_dist_disable(int_id);
    }

    // Unregister I2C ISR
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_isr_unregister(int_id);
    }

    // Uninitialize CPU interrupts
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_uninit();
    }

    // Uninitialize Global interrupts
    if(status == ALT_E_SUCCESS)
    {
        status = alt_int_global_uninit();
    }

    return status;
}

/******************************************************************************/
/*!
 * Initialize the master I2C module.
 *
 * \param       I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE socfpga_i2c_setup_master(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_MASTER_CONFIG_t cfg;

    printf("INFO: Init master I2C module.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_init(I2C_MASTER_DEVICE, device);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_enable(device);
    }

    if (status == ALT_E_SUCCESS)
    {
        alt_i2c_op_mode_set(device, ALT_I2C_MODE_MASTER);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_get(device, &cfg);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_config_speed_set(device, &cfg, I2C_MASTER_SPEED);
    }

    if (status == ALT_E_SUCCESS)
    {
        cfg.addr_mode      = ALT_I2C_ADDR_MODE_7_BIT;
        cfg.restart_enable = ALT_E_TRUE;

        status = alt_i2c_master_config_set(device, &cfg);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_sda_hold_time_set(device, 3);
    }

    return status;
}

/******************************************************************************/
/*!
 * Initialize the slave I2C module.
 *
 * \param       device I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE socfpga_i2c_setup_slave(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Init slave I2C module.\n");

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_init(I2C_SLAVE_DEVICE, device);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_enable(device);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_op_mode_set(device, ALT_I2C_MODE_SLAVE);
    }

    if (status == ALT_E_SUCCESS)
    {
        ALT_I2C_SLAVE_CONFIG_t cfg =
        {
            .addr_mode   = ALT_I2C_ADDR_MODE_7_BIT,
            .addr        = I2C_SLAVE_ADDRESS,
            .nack_enable = ALT_E_FALSE
        };
        status = alt_i2c_slave_config_set(device, &cfg);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_sda_hold_time_set(device, 1);
    }

    return status;
}


/******************************************************************************/
/*!
 * Cleanup I2C module.
 *
 * \param       device I2C device.
 * \return      result of the function
 */
static ALT_STATUS_CODE socfpga_i2c_cleanup(ALT_I2C_DEV_t * device)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    if(status == ALT_E_SUCCESS)
    {
        status = alt_i2c_disable(device);
    }

    if(status == ALT_E_SUCCESS)
    {
        status = alt_i2c_uninit(device);
    }

    return status;
}

/******************************************************************************/
/*!
 * Slave I2C Module ISR Callback
 *
 * \param       icciar
 * \param       context ISR context.
 * \return      none
 */
static void i2c_slave_isr_callback(uint32_t icciar, void * context)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    I2C_SLAVE_CONTEXT_t * ctxt = (I2C_SLAVE_CONTEXT_t *)context;

    uint32_t i2c_int = 0;

    // Determine the interrupt type signaled.
    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_int_status_get(ctxt->device, &i2c_int);
    }

    // Handle the interrupts
    if (status == ALT_E_SUCCESS)
    {
        if (i2c_int & ALT_I2C_STATUS_RX_UNDER)
        {
            // Reading the RX FIFO when it is empty.
            // This is an internal error.
            printf("ERROR [ISR]: Software Error: RX_UNDER [line:%d].\n", __LINE__);
            status = ALT_E_ERROR;
        }

        if (i2c_int & ALT_I2C_STATUS_RX_OVER)
        {
            // Overflow of RX FIFO due to incoming data.
            // This is an error as the Slave is not reading fast enough.
            printf("ERROR [ISR]: Software Error: RX_OVER [line:%d].\n", __LINE__);
            status = ALT_E_ERROR;
        }

        if (i2c_int & ALT_I2C_STATUS_TX_OVER)
        {
            // Overflow of TX FIFO due to data written by CPU.
            // This is an internal error.
            printf("ERROR [ISR]: Software Error: TX_OVER [line:%d].\n", __LINE__);
            status = ALT_E_ERROR;
        }


        if (i2c_int & ALT_I2C_STATUS_TX_ABORT)
        {
            // Transmit aborted.
            // This is an internal error.
            printf("ERROR [ISR]: Bus Error: TX_ABORT [line:%d].\n", __LINE__);
            status = ALT_E_ERROR;
        }


        if (i2c_int & ALT_I2C_STATUS_RX_FULL)
        {
            // RX FIFO level tripped due to incoming data.
            // Service request: Read data from RX FIFO to memory buffer.
            status = alt_i2c_slave_receive(ctxt->device, ctxt->buffer + ctxt->count);
            if (!ctxt->count)
            {
                // Print out only once during the transfer. Otherwise it'll get noisy.
                printf("INFO [ISR]: Reading data from bus ...\n");
            }
            ++ctxt->count;
        }

        if (i2c_int & ALT_I2C_STATUS_RD_REQ)
        {
            // I2C Master requesting read to current slave controller.
            // Service Request: Incoming read request; write something to TX FIFO.
            status = alt_i2c_slave_bulk_transmit(ctxt->device, ctxt->buffer, ctxt->count);
            printf("INFO [ISR]: Writing %" PRIi32 " data item(s) to bus..\n", ctxt->count);
            ctxt->count = 0;
        }

        // Note: The following interrupts were not enabled for this demo:
        //     ALT_I2C_STATUS_RX_DONE
        //     ALT_I2C_STATUS_ACTIVITY
        //     ALT_I2C_STATUS_STOP_DET
        //     ALT_I2C_STATUS_START_DET
        //     ALT_I2C_STATUS_INT_CALL

        // Clear the interrupt in I2C controller.
        ALT_STATUS_CODE int_status = alt_i2c_int_clear(ctxt->device, ALT_I2C_STATUS_INT_ALL);
        if (int_status != ALT_E_SUCCESS)
        {
            status = int_status;
        }
    }

    // Check status
    if (status != ALT_E_SUCCESS)
    {
        printf("ERROR [ISR]: Disabling interrupt.\n");

        // Report ISR failure to context.
        ctxt->status = status;

        // Disable the interrupt at the GIC.
        alt_int_dist_disable((ALT_INT_INTERRUPT_t)ALT_INT_ICCIAR_ACKINTID_GET(icciar));
    }
}

/******************************************************************************/
/*!
 * Master slave demo.
 *
 * \return      result of the function
 */
ALT_STATUS_CODE i2c_demo_master_slave(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_I2C_DEV_t device_master;
    ALT_I2C_DEV_t device_slave;

    char buffer_send[I2C_BUFFER_SIZE];
    char buffer_recv[I2C_BUFFER_SIZE];

    I2C_SLAVE_CONTEXT_t slave_context =
       {
           .device = &device_slave,
           .status = ALT_E_SUCCESS,
           .count  = 0,
       };

    printf("INFO: I2C demo master slave started.\n");

    // Setup Interrupt
    if(status == ALT_E_SUCCESS)
    {
        status = socfpga_int_setup(I2C_INTERRUPT_SLAVE, ALT_INT_TRIGGER_AUTODETECT);
    }

    // Setup master
    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_i2c_setup_master(&device_master);
    }

    // Setup slave
    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_i2c_setup_slave(&device_slave);
    }

    // Set target master target address
    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_master_target_set(&device_master, I2C_SLAVE_ADDRESS);
    }

    // Register slave ISR
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_isr_register(I2C_INTERRUPT_SLAVE, i2c_slave_isr_callback, &slave_context);
    }

    // Enable slave interrupts
    if (status == ALT_E_SUCCESS)
    {
        status = alt_i2c_int_enable(&device_slave,
                                    ALT_I2C_STATUS_RX_UNDER |
                                    ALT_I2C_STATUS_RX_OVER  |
                                    ALT_I2C_STATUS_RX_FULL  |
                                    ALT_I2C_STATUS_TX_OVER  |
                                    ALT_I2C_STATUS_RD_REQ   |
                                    ALT_I2C_STATUS_TX_ABORT
            );
    }

    // Perform the transfers
    for (int i=0; i<2; i++)
    {
        // first run with half size, 2nd run with full size
        int size = i ? (I2C_BUFFER_SIZE/2) : I2C_BUFFER_SIZE;

        printf("TEST: Buffer size = %d.\n", size);

        // Transmit
        if (status == ALT_E_SUCCESS)
        {
            printf("INFO: Master transmitting.\n");
            for (int i = 0; i < size; ++i)
            {
                buffer_send[i] = (char)rand();
            }
            status = alt_i2c_master_transmit(&device_master, buffer_send, size, true, true);
        }

        // Receive
        if(status == ALT_E_SUCCESS)
        {
            printf("INFO: Master receiving.\n");

            // Zero out receive buffer
            memset(buffer_recv, 0, sizeof(buffer_send));

            status = alt_i2c_master_receive(&device_master, buffer_recv, size, true, true);
        }

        // Compare data
        if (status == ALT_E_SUCCESS)
        {
            printf("INFO: Comparing received data with transmitted data.\n");
            if (memcmp(buffer_send, buffer_recv, size) != 0)
            {
                status = ALT_E_ERROR;
            }
        }

        // Check for ISR error
        if (status == ALT_E_SUCCESS)
        {
            if (slave_context.status != ALT_E_SUCCESS)
            {
                status = slave_context.status;
                printf("ERROR: ISR Reported error.\n");
            }
        }

        // Decode error
        if (status != ALT_E_SUCCESS)
        {
            ALT_I2C_TX_ABORT_CAUSE_t cause;

            alt_i2c_tx_abort_cause_get(&device_master, &cause);
            printf("DEBUG: TX Abort Cause [Master]: 0x%x.\n", cause);

            alt_i2c_tx_abort_cause_get(&device_slave, &cause);
            printf("DEBUG: TX Abort Cause [Slave]:  0x%x.\n", cause);
        }
    }

    // Cleanup I2C Modules
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Cleaning up master.\n");
        status = socfpga_i2c_cleanup(&device_master);
    }

    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Cleaning up slave.\n");
        status = socfpga_i2c_cleanup(&device_slave);
    }

    // Cleanup Interrupt
    if(status == ALT_E_SUCCESS)
    {
        status = socfpga_int_cleanup(I2C_INTERRUPT_SLAVE);
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: I2C demo master slave succeeded.\n\n");
    }
    else
    {
        printf("INFO: I2C demo master slave failed.\n\n");
    }

    return status;
}

#include <stdio.h>

double my_func(double arg1, double arg2, double arg3, double arg4,
    double arg5, double arg6, float arg7, double arg8,
    double arg9, float arg10)
{
    return arg1 + arg2;
}

int main(void)
{
    double dbl1, dbl2;

    my_func(2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 2.10);

    dbl1 = 2.3;
    dbl2 = 4.5;

    dbl1 += dbl2;

    return 0;
}

USE_ARMCC ?= 1

SOCEDS_ROOT ?= $(SOCEDS_DEST_ROOT)
HWLIBS_ROOT = $(SOCEDS_ROOT)/ip/altera/hps/altera_hps/hwlib

HWLIBS_SRC  := 
EXAMPLE_SRC := hello.c io.c
C_SRC       := $(EXAMPLE_SRC) $(HWLIBS_SRC)

CROSS_COMPILE := arm-altera-eabi-

ifeq ($(USE_ARMCC),0)

LINKER_SCRIPT := cycloneV-dk-ram-modified.ld

MULTILIBFLAGS := -mcpu=cortex-a9 -mfloat-abi=softfp -mfpu=neon
#MULTILIBFLAGS := -mcpu=cortex-a9 -mfloat-abi=hard
CFLAGS  := -g -O0 -Wall -Werror -std=c99 $(MULTILIBFLAGS) -I$(HWLIBS_ROOT)/include
LDFLAGS := -T$(LINKER_SCRIPT) $(MULTILIBFLAGS)

CC := $(CROSS_COMPILE)gcc
LD := $(CROSS_COMPILE)g++

else

LINKER_SCRIPT := scatter.scat

# Suppress ARMCC warning 9931: "Yours License for Compiler (feature compiler5) will expire in X days"
CFLAGS   := -g -O0 --fpu=softvfp --strict --diag_error=warning --diag_suppress=9931 --cpu=Cortex-A9 --no_unaligned_access --c99 -I$(HWLIBS_ROOT)/include
ASMFLAGS := -g --diag_error=warning --diag_suppress=9931 --cpu=Cortex-A9 --no_unaligned_access
LDFLAGS  := --strict --diag_error=warning --diag_suppress=9931 --entry=alt_interrupt_vector --cpu=Cortex-A9 --no_unaligned_access --scatter=$(LINKER_SCRIPT)

CC := armcc
AS := armasm
LD := armlink
AR := armar

endif

NM := $(CROSS_COMPILE)nm
OD := $(CROSS_COMPILE)objdump
OC := $(CROSS_COMPILE)objcopy
MKIMAGE := mkimage
RM := rm -rf
CP := cp -f

ELF ?= $(basename $(firstword $(C_SRC))).axf
OBJ := $(patsubst %.c,%.o,$(C_SRC))
BIN = $(basename $(firstword $(C_SRC))).bin
IMG = $(basename $(firstword $(C_SRC)))-mkimage.bin

.PHONY: all
all: $(BIN) $(ELF) $(IMG) $(SPL)

.PHONY: clean
clean:
	$(RM) $(ELF) $(HWLIBS_SRC) $(OBJ) $(BIN) $(IMG) *.lst

%.c: $(HWLIBS_ROOT)/src/hwmgr/%.c
	$(CP) $< $@

$(OBJ): %.o: %.c Makefile
ifeq ($(USE_ARMCC),0)
	$(CC) -c -g -Wa,-a,-ad $(CFLAGS) $< > $@.lst
else
	$(CC) $(CFLAGS) --asm --interleave $<
endif
	$(CC) $(CFLAGS) -c $< -o $@

$(ELF): $(OBJ)
	$(LD) $(LDFLAGS) $(OBJ) -o $@
	
$(BIN): $(ELF)
	$(OC) -O binary $(ELF) $(BIN)

$(IMG): $(BIN)
	$(MKIMAGE) -A arm -T standalone -C none -a 0x100040 -e 0 -n "baremetal image" -d $(BIN) $(IMG)

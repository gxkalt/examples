/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

/******************************************************************************/
/*!
 * Write a QSPI Flash using the Indirect Mode.
 *
 * The whole write must be confined to a single Flash page (256 bytes).
 *
 * \param       address  Where to write the data in Flash. Must be 4-byte aligned.
 * \param       buffer Data to write to Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
static ALT_STATUS_CODE qspi_write_indirect(uint32_t address, void * buffer, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t * data = (uint32_t*)buffer;
    uint32_t write_capacity = ALT_QSPI_SRAM_FIFO_ENTRY_COUNT - alt_qspi_sram_partition_get();

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_write_start(address, size);
    }

    // Write data
    while ((size != 0) && (status == ALT_E_SUCCESS))
    {
        uint32_t space = write_capacity - alt_qspi_indirect_write_fill_level();
        space = MIN(space, size/4);

        // Write the FIFO
        for (uint32_t i = 0; i < space; ++i)
        {
            alt_write_word(ALT_QSPIDATA_ADDR, *data++);
        }

        // Decrement size
        size -= space * sizeof(uint32_t);
    }

    // Complete the read
    if(status != ALT_E_ERROR)
    {
        alt_qspi_indirect_write_finish();
    }

    // Check we have read all the data
    if(size != 0)
    {
        status = ALT_E_ERROR;
    }

    return status;
}

/******************************************************************************/
/*!
 * Read from QSPI Flash using the DMA.
 *
 * The whole read must be confined to a single Flash bank (16 MBbytes).
  *
 * \param       buffer Location to read the data to. Must be 4-byte aligned.
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_read_indirect(void * buffer, uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t * data = (uint32_t*)buffer;

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_read_start(address, size);
    }

    // Read data
    while (!alt_qspi_indirect_read_is_complete() && (status == ALT_E_SUCCESS))
    {
        // Read the fill level
        uint32_t level = alt_qspi_indirect_read_fill_level();

        // Read the FIFO
        for (uint32_t i = 0; i < level; ++i)
        {
            *data++ = alt_read_word(ALT_QSPIDATA_ADDR);
        }

        // Decrement size
        size -= level * sizeof(uint32_t);
    }

    // Complete the read
    if(status != ALT_E_ERROR)
    {
        alt_qspi_indirect_read_finish();
    }

    // Check we have read all the data
    if(size != 0)
    {
        status = ALT_E_ERROR;
    }

    return status;
}

/******************************************************************************/
/*!
 * Demonstrates reading and writing from/to QSPI Flash Memory using Indirect Mode
 *
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_indirect(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Demo indirect mode using address = 0x%08x, size = %d bytes.\n", (int)address, (int)size);

    // Erase flash
    status = qspi_demo_erase(address, size);

    // Write 'write_buffer' to flash - by pages (256 B)
    if(status ==  ALT_E_SUCCESS)
    {
        char * crt_data = (char*)Write_Buffer;
        uint32_t crt_size = size;
        uint32_t crt_address = address;

        // Split into 256B pages and write it
        while((crt_size != 0) & (status == ALT_E_SUCCESS))
        {
            // Limit writing size to filling the current 16MB
            uint32_t write_size = ALT_QSPI_PAGE_SIZE - (crt_address % ALT_QSPI_PAGE_SIZE);
            write_size = MIN(write_size, crt_size);

            // Write the data
            printf("INFO: Writing to address 0x%08x, size = %d bytes using indirect mode.\n", (int)crt_address, (int)write_size);
            status = qspi_write_indirect(crt_address, crt_data, write_size);

            // Update addresses and size
            crt_size -= write_size;
            crt_address += write_size;
            crt_data += write_size;
        }
    }

    //  Read 'read_buffer' from flash - by banks (16 MiB)
    if(status ==  ALT_E_SUCCESS)
    {
        char * crt_data = (char*)Read_Buffer;
        uint32_t crt_size = size;
        uint32_t crt_address = address;

        memset(Read_Buffer, 0, size);

        // Split into 16MiB banks and read it
        while((crt_size != 0) & (status == ALT_E_SUCCESS))
        {
            // Limit reading size to filling the current 16MB
            uint32_t read_size = ALT_QSPI_BANK_SIZE - (crt_address % ALT_QSPI_BANK_SIZE);
            read_size = MIN(read_size, crt_size);

            // Write the data
            printf("INFO: Reading from address 0x%08x, size = %d bytes using indirect mode.\n", (int)crt_address, (int)read_size);
            status = qspi_read_indirect(crt_data, crt_address, read_size);

            // Update addresses and size
            crt_size -= read_size;
            crt_address += read_size;
            crt_data += read_size;
        }
    }

    // Compare buffers
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Comparing written data with read back data.\n");
        if(memcmp(Read_Buffer, Write_Buffer, size) != 0)
        {
            status = ALT_E_ERROR;
        }
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Demo indirect mode succeeded.\n");
    }
    else
    {
        printf("ERROR: DEMO indirect mode failed.\n");
    }

    printf("\n");

    return status;
}



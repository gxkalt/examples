Introduction

This HardwareLibs project is meant as an example for using the QSPI Flash APIs.

The project demonstrates the following QSPI Flash API features:
- Reading and writing using Generic Block I/O functions
- Reading and writing using Indirect Mode
- Reading and writing using DMA Mode
- Erasing using sector (64KB) and subsector (4KB) erasing commands

The project also demonstrates the following additional API features:
- Setting up MMU and caches
- Performing cache cleanup and purging
- Setting up DMA, and using Memory2Peripheral and Peripheral2Memory operations

Note: This example was developed with and tested against SoC EDS 14.0.1b235.

====

Target Boards:
- Altera Cyclone V SoC Development Board rev D

Target QSPI Devices:
- Micron N2Q512A  - 512Mb device 
- Micron N25Q00AA - 1Gb device

====

Limitations:

1. The project was developed for and tested on Micron N2Q512A and N25Q00AA QSPI Flash Devices.
The above mentioned devices require a 'Read Flag Status Register' command to determine that 
the last initiated erase or program operation was completed. Since the QSPI controller
can automatically issue only 'Read Status Register' commands, the following limitations occur:
- Direct mode cannot be used
- Indirect writes are limited within a single page (256 Bytes) 

2. Only 3-byte addressing mode is used. Therefore:
- Indirect reads are limited within a single bank (16 MBytes)

The sample code works around the above limitations for indirect (and DMA) modes by:
- Splitting write operations into a series of page-confined operations
- Splitting the read operations into a series of bank-confined operations

Note that the generic block I/O functions also work around the above limitations internally.  

=====

Source Files

The following are descriptions of the source and header files contained in this
project:

alt_pt.{c,h}

  Contain the functions to setup the MMU translation tables.

qspi_demo.{c,h}

  Contain the main, system initialization and system cleanup functions.

qspi_demo_blockio.c

  Contains the functions demonstrating QSPI Flash generic block I/O APIs.

qspi_demo_dma.c

  Contains the functions demonstrating QSPI Flash DMA APIs.

qspi_demo_erase.c

  Contains the functions demonstrating QSPI Flash erase APIs.

qspi_demo_indirect.c

  Contains the functions demonstrating QSPI Flash indirect mode APIs.
  
=====

Building Example

Before running the example, the target executable first needs to be built.

1. In DS-5, build the application:
  1a. Switch to the C/C++ Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> C/C++.
  1b. In the "Project Explorer" panel, right-mouse-click 
      "Altera-SoCFPGA-HardwareLib-QSPI-CV-GNU" and select "Build Project".

The Console panel (bottom of the UI) should detail the progress of the build
and report any warnings or errors.

=====

System Setup

1. Connect the USB to serial bridge to the host computer.
2. Connect the USB-BlasterII to the host computer.
3. Install the USB to serial bridge driver on the host computer if that driver
   is not already present. Consult documentation for the DevKit for
   instructions on installing the USB to serial bridge driver.
4. Install the USB-BlasterII driver on the host computer if that driver is not
   already present. Consult documentation for QuartusII for instructions on
   installing the USB-BlasterII driver.
5. In DS-5, configure the launch configuration.
  5a. Select the menu: Run >> Debug Configurations...
  5b. In the options on the left, expand "DS-5 Debugger" and select
      "Altera-SoCFPGA-HardwareLib-QSPI-CV-GNU".
  5c. In the "Connections" section near the bottom, click Browse.
  5d. Select the appropriate USB-BlasterII to use. Multiple items will be
      presented if there is more than one USB-BlasterII connection attached to
      the host computer.
  5e. Click "Apply" then "OK" to apply the USB-BlasterII selection.
  5f. Click "Close" to close the Debug Configuration. Otherwise click "Debug"
      run the example in the debugger.

=====

Running the Example

After building the example and setting up the host computer system, the example
can be run by following these steps.

1. In DS-5, launch the debug configuration.
  1a. Switch to the Debug Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> DS-5 Debug.
  1b. In the "Debug Control" panel, right-mouse-click
      "Altera-SoCFPGA-HardwareLib-QSPI-CV-GNU" and select
      "Connect to Target".

Connecting to the target takes a moment to load the preloader, run the
preloader, load the executable, and run executable. After the debug connection
is established, the debugger will pause the execution at the main() function.
Users can then set additional break points, step into, step out of, or step one
line using the DS-5 debugger. Consult documentation for DS-5 for more
information on debugging operations.

=====

Sample output

The address and size of the test buffers are randomly generated for each test, to help 
with wear leveling of the QSPI Flash memory device. 

The following is a sample output:

INFO: System Initialization.
INFO: Setting up Global Timer.
INFO: Setting up interrupt system.
INFO: Populating page table and enabling MMU.
INFO: Enabling caches.
INFO: Configuring DMA.
INFO: Allocating DMA channel.
INFO: Initializing QSPI.
INFO: Configuring QSPI DMA single size = 4, burst_size = 32 bytes.
INFO: Enabling QSPI.
INFO: Checking QSPI is idle.
INFO: Querying device characteristics.
INFO: Checking supported manufacturer.
INFO: Device is 512 Mib or 64 MiB.
INFO: Using random seed = 0x112b62e2.

INFO: Demo block I/O using address = 0x018791dc, size = 1400 bytes.
INFO: Erasing 1400 bytes at address 0x018791dc.
INFO: Saving 476 bytes from address 0x1879000 which also need to be erased.
INFO: Saving 2220 bytes from address 0x1879754 which also need to be erased.
INFO: Erasing 4KB subsector at address 0x01879000.
INFO: Restoring 476 bytes to address 0x01879000.
INFO: Restoring 2220 bytes to address 0x01879754.
INFO: Erasing completed successfully.
INFO: Writing to address 0x018791dc, size = 1400 bytes using block I/O.
INFO: Reading from address 0x018791dc, size = 1400 bytes using block I/O.
INFO: Comparing written data with read back data.
INFO: Demo block I/O succeeded.

INFO: Demo indirect mode using address = 0x00c10790, size = 356 bytes.
INFO: Erasing 356 bytes at address 0x00c10790.
INFO: Saving 1936 bytes from address 0xc10000 which also need to be erased.
INFO: Saving 1804 bytes from address 0xc108f4 which also need to be erased.
INFO: Erasing 4KB subsector at address 0x00c10000.
INFO: Restoring 1936 bytes to address 0x00c10000.
INFO: Restoring 1804 bytes to address 0x00c108f4.
INFO: Erasing completed successfully.
INFO: Writing to address 0x00c10790, size = 112 bytes using indirect mode.
INFO: Writing to address 0x00c10800, size = 244 bytes using indirect mode.
INFO: Reading from address 0x00c10790, size = 356 bytes using indirect mode.
INFO: Comparing written data with read back data.
INFO: Demo indirect mode succeeded.

INFO: Demo DMA mode using address = 0x0144711c, size = 2060 bytes.
INFO: Erasing 2060 bytes at address 0x0144711c.
INFO: Saving 284 bytes from address 0x1447000 which also need to be erased.
INFO: Saving 1752 bytes from address 0x1447928 which also need to be erased.
INFO: Erasing 4KB subsector at address 0x01447000.
INFO: Restoring 284 bytes to address 0x01447000.
INFO: Restoring 1752 bytes to address 0x01447928.
INFO: Erasing completed successfully.
INFO: Enabling QSPI DMA operation.
INFO: Writing to address 0x0144711c, size = 228 bytes using DMA mode.
INFO: Writing to address 0x01447200, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447300, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447400, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447500, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447600, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447700, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447800, size = 256 bytes using DMA mode.
INFO: Writing to address 0x01447900, size = 40 bytes using DMA mode.
INFO: Reading from address 0x0144711c, size = 2060 bytes using DMA mode.
INFO: Comparing written data with read back data.
INFO: Demo DMA mode succeeded.

INFO: System shutdown.

RESULT: All tests successful.

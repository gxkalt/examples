/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

/******************************************************************************/
/*!
 * Demonstrates erasing QSPI Flash memory
 *
 * \param       address  Where to erase data. Must be 4-byte aligned.
 * \param       size Size of data to be erased. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_erase(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Buffers for storing the additional data that we may need to erase
    uint32_t buffer1[ALT_QSPI_SUBSECTOR_SIZE / sizeof(uint32_t)];
    uint32_t buffer2[ALT_QSPI_SUBSECTOR_SIZE / sizeof(uint32_t)];

    // Location and size of the data that is additionally erased
    uint32_t buffer1_address;
    uint32_t buffer2_address;
    uint32_t buffer1_size = 0;
    uint32_t buffer2_size = 0;

    printf("INFO: Erasing %d bytes at address 0x%08x.\n", (int)size, (int)address);

    // Align the start address to subsector boundary
    if(status == ALT_E_SUCCESS)
    {
        uint32_t remainder = address % ALT_QSPI_SUBSECTOR_SIZE;
        if(remainder != 0)
        {
            address -= remainder;
            size += remainder;
            buffer1_address = address;
            buffer1_size = remainder;

            // Save additionally erased data at the beginning of the area
            printf("INFO: Saving %d bytes from address 0x%04x which also need to be erased.\n", (int)buffer1_size, (int) buffer1_address);
            status = alt_qspi_read(buffer1, buffer1_address, buffer1_size);
        }
    }

    // align the end address to subsector boundary
    if(status == ALT_E_SUCCESS)
    {
        uint32_t remainder = size % ALT_QSPI_SUBSECTOR_SIZE;
        if(remainder != 0)
        {
            remainder = ALT_QSPI_SUBSECTOR_SIZE - remainder;
            buffer2_address = address + size;
            buffer2_size = remainder;
            size += remainder;

            // Save additionally erased data at the end of the area
            printf("INFO: Saving %d bytes from address 0x%04x which also need to be erased.\n", (int)buffer2_size, (int) buffer2_address);
            status = alt_qspi_read(buffer2, buffer2_address, buffer2_size);
        }
    }

    // Perform erasing
    while((size != 0) && (status == ALT_E_SUCCESS))
    {
        // Erase a full sector if possible, otherwise a subsector
        if((0 == (address % ALT_QSPI_SECTOR_SIZE)) && (size >= ALT_QSPI_SECTOR_SIZE))
        {
            printf("INFO: Erasing 64KB sector at address 0x%08x.\n", (int)address);
            status =  alt_qspi_erase_sector(address);
            address += ALT_QSPI_SECTOR_SIZE;
            size -= ALT_QSPI_SECTOR_SIZE;
        }
        else
        {
            printf("INFO: Erasing 4KB subsector at address 0x%08x.\n", (int)address);
            status = alt_qspi_erase_subsector(address);
            address += ALT_QSPI_SUBSECTOR_SIZE;
            size -= ALT_QSPI_SUBSECTOR_SIZE;
        }
    }

    // Restore additionally erased data at the beginning of the area
    if((status == ALT_E_SUCCESS) && (buffer1_size != 0))
    {
        printf("INFO: Restoring %d bytes to address 0x%08x.\n", (int)buffer1_size, (int) buffer1_address);
        status = alt_qspi_write(buffer1_address, buffer1, buffer1_size);
    }

    // Restore additionally erased data at the end of the area
    if((status == ALT_E_SUCCESS) && (buffer2_size != 0))
    {
        printf("INFO: Restoring %d bytes to address 0x%08x.\n", (int)buffer2_size, (int) buffer2_address);
        status = alt_qspi_write(buffer2_address, buffer2, buffer2_size);
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Erasing completed successfully.\n");
    }
    else
    {
        printf("INFO: Erasing failed.\n");
    }

    return status;
}

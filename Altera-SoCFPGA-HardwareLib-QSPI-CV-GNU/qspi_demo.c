/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

// Size of the QSPI Flash Memory - filled in by system_init()
uint32_t Qspi_Device_Size;

// Buffers used for testing - aligned to cache line size so they can be cleaned/purged/invalidated
uint32_t Write_Buffer[MAX_TEST_BYTES / sizeof(uint32_t)] __attribute__ ((aligned (ALT_CACHE_LINE_SIZE))) ;
uint32_t Read_Buffer[MAX_TEST_BYTES / sizeof(uint32_t)] __attribute__ ((aligned (ALT_CACHE_LINE_SIZE))) ;

// DMA channel used by the demo
ALT_DMA_CHANNEL_t Dma_Channel;

// System initialization
ALT_STATUS_CODE system_init(void);

// System shutdown
ALT_STATUS_CODE system_uninit(void);

// Generate random test data and fill in the write_buffer
void generate_test_data(uint32_t * address, uint32_t * size);

/******************************************************************************/
/*!
 * System Initialization
 * \return      result of the function
 */
ALT_STATUS_CODE system_init(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t rdid;

    printf("INFO: System Initialization.\n");

    // Initialize global timer
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Setting up Global Timer.\n");
        if(!alt_globaltmr_int_is_enabled())
        {
            status = alt_globaltmr_init();
        }
    }

    // Setting up interrupt system
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Setting up interrupt system.\n");
        status = alt_int_cpu_init();
    }

    // Populating page table and enabling MMU
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Populating page table and enabling MMU.\n");
        status = alt_pt_init();

    }

    // Enabling caches
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Enabling caches.\n");
        status = alt_cache_system_enable();
    }

    // Initialize DMA
    if(status == ALT_E_SUCCESS)
    {
        ALT_DMA_CFG_t dma_config;

        printf("INFO: Configuring DMA.\n");

        dma_config.manager_sec = ALT_DMA_SECURITY_DEFAULT;
        for (int i = 0; i < 8; ++i)
        {
            dma_config.irq_sec[i] = ALT_DMA_SECURITY_DEFAULT;
        }
        for (int i = 0; i < 32; ++i)
        {
            dma_config.periph_sec[i] = ALT_DMA_SECURITY_DEFAULT;
        }
        for (int i = 0; i < 4; ++i)
        {
            dma_config.periph_mux[i] = ALT_DMA_PERIPH_MUX_DEFAULT;
        }

        status = alt_dma_init(&dma_config);
    }

    // Allocate DMA Channel
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Allocating DMA channel.\n");
        status = alt_dma_channel_alloc_any(&Dma_Channel);
    }

    // Initializing QSPI
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Initializing QSPI.\n");
        status = alt_qspi_init();
    }

    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Configuring QSPI DMA single size = %d, burst_size = %d bytes.\n", QSPI_DMA_SINGLE_SIZE, QSPI_DMA_BURST_SIZE);
        status = alt_qspi_dma_config_set(QSPI_DMA_SINGLE_SIZE, QSPI_DMA_BURST_SIZE);
    }

    // Enabling QSPI
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Enabling QSPI.\n");
        status = alt_qspi_enable();
    }

    // Checking QSPI is idle
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Checking QSPI is idle.\n");
        if (!alt_qspi_is_idle())
        {
            status = ALT_E_ERROR;
        }
    }

    // Querying device characteristics
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Querying device characteristics.\n");
        status = alt_qspi_device_rdid(&rdid);
    }

    // Check supported manufacturers
    if(status == ALT_E_SUCCESS)
    {
        int jedec_id =  ALT_QSPI_STIG_RDID_JEDECID_GET(rdid);

        printf("INFO: Checking supported manufacturer.\n");
        if(jedec_id != ALT_QSPI_STIG_RDID_JEDECID_MICRON)
        {
            status = ALT_E_ERROR;
        }
    }

    // Check supported sizes
    if(status == ALT_E_SUCCESS)
    {
        int size_code = ALT_QSPI_STIG_RDID_CAPACITYID_GET(rdid);

        switch (size_code)
        {
        case 0x20:
            printf("INFO: Device is 512 Mib or 64 MiB.\n");
            Qspi_Device_Size = 0x04000000; // 512 Mib => 64 MiB
            break;
        case 0x21:
            printf("INFO: Device is 1 Gib or 128 MiB.\n");
            Qspi_Device_Size = 0x08000000; // 1 Gib => 128 MiB
            break;
        default:
            printf("ERROR: Unknown device size code 0x%02x.\n", size_code);
            status = ALT_E_ERROR;
            break;
        }
    }

    // Initialize random number generator
    if(status == ALT_E_SUCCESS)
    {
        int random_seed = alt_globaltmr_counter_get_low32();
        printf("INFO: Using random seed = 0x%04x.\n", random_seed);
        srand(random_seed);
    }

    printf("\n");

    return status;
}

/******************************************************************************/
/*!
 * System Cleanup
 *
 * \return      result of the function
 */
ALT_STATUS_CODE system_uninit(void)
{
    printf("INFO: System shutdown.\n");

    alt_dma_channel_free(Dma_Channel);
    alt_dma_uninit();
    alt_qspi_disable();
    alt_qspi_uninit();
    alt_cache_system_disable();
    alt_pt_uninit();
    alt_int_cpu_uninit();

    printf("\n");

    return ALT_E_SUCCESS;
}

/******************************************************************************/
/*!
 * Generate random test data and fill the write buffer
 *
 * \param address [out] QSPI Flash Address
 * \param size    [out] Size of data
 * \return        result of the function
 */
void generate_test_data(uint32_t * address, uint32_t * size)
{
    // Generate random flash address
    *address = rand() % (Qspi_Device_Size - MAX_TEST_BYTES);
    *address &= ~0x3;

    // Generate random size
    *size = rand() % MAX_TEST_BYTES;
    *size &= ~0x3;

    // Initialize write buffer accordingly
    for(int i = 0; i < ARRAY_COUNT(Write_Buffer); i++)
    {
        Write_Buffer[i] = rand();
    }
}

/******************************************************************************/
/*!
 * Program entrypoint
 *
 * \return result of the program
 */
int main(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t address;
    uint32_t size;

    // System init
    if(status == ALT_E_SUCCESS)
    {
        status = system_init();
    }

    // Block I/O
    if(status == ALT_E_SUCCESS)
    {
        generate_test_data(&address, &size);
        status = qspi_demo_block_io(address, size);
    }

    // Indirect Mode
    if(status == ALT_E_SUCCESS)
    {
        generate_test_data(&address, &size);
        status = qspi_demo_indirect(address, size);
    }

    // DMA Mode
    if(status == ALT_E_SUCCESS)
    {
        generate_test_data(&address, &size);
        status = qspi_demo_dma(address, size);
    }

    // System uninit
    if(status == ALT_E_SUCCESS)
    {
        status = system_uninit();
    }

    // Report status
    if (status == ALT_E_SUCCESS)
    {
        printf("RESULT: All tests successful.\n");
        return 0;
    }
    else
    {
        printf("RESULT: Some failures detected.\n");
        return 1;
    }
}

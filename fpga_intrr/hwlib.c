/*****************************************************************************
 *
 * Copyright 2013 Altera Corporation. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************/

#include <stdio.h>
#include <inttypes.h>
#include "alt_address_space.h"
#include "alt_bridge_manager.h"
#include "alt_clock_manager.h"
#include "alt_dma.h"
#include "alt_globaltmr.h"
#include "alt_timers.h"
#include "alt_fpga_manager.h"
#include "alt_generalpurpose_io.h"
#include "alt_interrupt.h"
#include "socal/socal.h"

#define ARRAY_COUNT(array)  (sizeof(array) / sizeof(array[0]))

#define HPS_PB_0_ASSERT         0x01C00000  // GPIO[8] (HPS_PB_0)
#define HPS_PB_1_ASSERT         0x01A00000  // GPIO[9] (HPS_PB_1)
#define HPS_PB_2_ASSERT         0x01600000  // GPIO[10](HPS_PB_2)
#define HPS_PB_3_ASSERT         0x00E00000  // GPIO[11](HPS_PB_3)

#define HPS_PB_INT_ALL_BIT_MASK     0x01E00000 // Interrupt bits for GPIO2

#define FPGA_PB_0               0x00000001
#define FPGA_PB_1               0x00000002

/* enable semihosting with gcc by defining an __auto_semihosting symbol */
int __auto_semihosting;

const uint32_t ALT_LWFPGA_BASE         = 0xFF200000;
const uint32_t ALT_LWFPGA_SYSID_OFFSET = 0x00010000;
const uint32_t ALT_LWFPGA_LED_OFFSET   = 0x00010040;
const uint32_t ALT_LWFPGA_INTR_CAP_OFF = 0x00030000;
const uint32_t ALT_LWFPGA_PIO_OFF = 0x000100c0;
const uint32_t ALT_LWFPGA_PIO_DIR_OFF = 0x00000004;
const uint32_t ALT_LWFPGA_PIO_INTR_MSK_OFF = 0x00000008;
const uint32_t ALT_LWFPGA_PIO_EDGECAP_OFF = 0x0000000c;

const uint32_t ALT_PERIPH_BASE         = 0xFFFEC000;
const uint32_t ALT_GIC_DST_OFF         = 0x00001000;
const uint32_t ALT_GIC_DST_ICDISERN_OFF = 0x00000100;
const uint32_t ALT_GIC_DST_ICDICTR_OFF = 0x00000004;
const uint32_t ALT_GIC_DST_ICDISRN_OFF = 0x00000080;
const uint32_t ALT_GIC_DST_ICDIPRN_OFF = 0x00000400;
const uint32_t ALT_GIC_CPU_IF_OFF      = 0x00000100;

uint32_t push_button_stat = 0;
uint64_t fpga_irq3_cnt = 0;

ALT_GPIO_CONFIG_RECORD_t pb_gpio_init[] =
{
    { ALT_HLGPI_8, ALT_GPIO_PIN_INPUT, ALT_GPIO_PIN_LEVEL_TRIG_INT, ALT_GPIO_PIN_ACTIVE_LOW, 0, 0 }, //HPS_PB_0
    { ALT_HLGPI_9, ALT_GPIO_PIN_INPUT, ALT_GPIO_PIN_LEVEL_TRIG_INT, ALT_GPIO_PIN_ACTIVE_LOW, 0, 0 }, //HPS_PB_1
    { ALT_HLGPI_10, ALT_GPIO_PIN_INPUT, ALT_GPIO_PIN_LEVEL_TRIG_INT, ALT_GPIO_PIN_ACTIVE_LOW, 0, 0 },//HPS_PB_2
    { ALT_HLGPI_11, ALT_GPIO_PIN_INPUT, ALT_GPIO_PIN_LEVEL_TRIG_INT, ALT_GPIO_PIN_ACTIVE_LOW, 0, 0 } //HPS_PB_3
};

void fpga3_isr_callback(uint32_t icciar, void * context)
{
    //static uint32_t gray = 0;

    fpga_irq3_cnt++;

    //if (!(fpga_irq3_cnt & 0xff))
    //    printf("%lld\n", fpga_irq3_cnt);
        //alt_write_word(ALT_LWFPGA_BASE + ALT_LWFPGA_LED_OFFSET, gray ^= 0x1);

    return;
}

void fpga_isr_callback(uint32_t icciar, void * context)
{
    uint32_t priority, val;

    val = alt_read_word(ALT_LWFPGA_BASE + ALT_LWFPGA_PIO_OFF + ALT_LWFPGA_PIO_EDGECAP_OFF);

    alt_write_word(ALT_LWFPGA_BASE + ALT_LWFPGA_PIO_OFF + ALT_LWFPGA_PIO_EDGECAP_OFF, 0xf);

    //printf("INFO: LWFPGA Slave => PIO Intrr status = 0x%" PRIx32 "\n", val);
    printf("Got irq from FPGA PB %d\n", (val == FPGA_PB_0)? 0 : 1);

    alt_int_dist_priority_get(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1, &priority);
    printf("intrr priority = 0x%" PRIx32 "\n", priority);

    printf("cnt = %lld\n\n", fpga_irq3_cnt);

    return;
}

void gpio_isr_callback(uint32_t icciar, void * context)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t push_button_read = 0;
    uint32_t priority;

    if (status == ALT_E_SUCCESS)
    {
        push_button_read = alt_gpio_port_int_status_get(ALT_GPIO_PORTC);
        push_button_stat = push_button_read ^ HPS_PB_INT_ALL_BIT_MASK;
    }

    alt_int_dist_priority_get(ALT_INT_INTERRUPT_GPIO2, &priority);
    printf("intrr priority = 0x%" PRIx32 "\n\n", priority);

    if ((push_button_stat == HPS_PB_0_ASSERT) && (status == ALT_E_SUCCESS))
    {
        printf("Pushed HPS PB 0\n");
    }

    if ((push_button_stat == HPS_PB_1_ASSERT) && (status == ALT_E_SUCCESS))
    {
        printf("Pushed HPS PB 1\n");
    }

    if ((push_button_stat == HPS_PB_2_ASSERT) && (status == ALT_E_SUCCESS))
    {
        printf("Pushed HPS PB 2\n");
    }

    if ((push_button_stat == HPS_PB_3_ASSERT) && (status == ALT_E_SUCCESS))
    {
        printf("Pushed HPS PB 3\n");
    }

    return;
}

ALT_STATUS_CODE get_clocks(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t freq;

    printf("-------------------Clocks------------------\n");

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C0, &freq);
    printf("INFO: Main pll C0 (mpu_base_clk) freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C1, &freq);
    printf("INFO: Main pll C1 (main_base_clk) freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C2, &freq);
    printf("INFO: Main pll C2 (dbg_base_clk) freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C3, &freq);
    printf("INFO: Main pll C3 (main_qspi_base_clk) freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C4, &freq);
    printf("INFO: Main pll C4 freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MAIN_PLL_C5, &freq);
    printf("INFO: Main pll C5 freq = %" PRIu32 ".\n", freq);

    printf("-------------------C0---------------------\n");

    status = alt_clk_freq_get(ALT_CLK_MPU, &freq);
    printf("INFO: mpu_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &freq);
    printf("INFO: mpu_periph_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_MPU_L2_RAM, &freq);
    printf("INFO: mpu_l2_ram_clk freq = %" PRIu32 ".\n", freq);


    printf("-------------------C1---------------------\n");

    status = alt_clk_freq_get(ALT_CLK_L4_MAIN, &freq);
    printf("INFO: l4_main_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_L3_MAIN, &freq);
    printf("INFO: l3_main_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_L3_MP, &freq);
    printf("INFO: l3_mp_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_L3_SP, &freq);
    printf("INFO: l3_sp_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_L4_MP, &freq);
    printf("INFO: l4_mp_clk freq = %" PRIu32 ".\n", freq);

    status = alt_clk_freq_get(ALT_CLK_L4_SP, &freq);
    printf("INFO: l4_sp_clk freq = %" PRIu32 ".\n", freq);

    printf("------------------------------------------\n");

    return status;
}

ALT_STATUS_CODE set_global_timer(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t freq;

    status = alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &freq);
    if (status != ALT_E_SUCCESS)
        return status;

    status = alt_globaltmr_init();
    if (status != ALT_E_SUCCESS)
        return status;

    status = alt_gpt_counter_set(ALT_GPT_CPU_GLOBAL_TMR, (freq/1000000 - 1));
    if (status != ALT_E_SUCCESS)
        return status;

    status = alt_globaltmr_start();
    if (status != ALT_E_SUCCESS)
        return status;

    printf("INFO: Global Timer running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_CPU_GLOBAL_TMR));

    status = alt_globaltmr_stop();

    return status;
}

ALT_STATUS_CODE set_private_timer(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t freq;

    status = alt_clk_freq_get(ALT_CLK_MPU_PERIPH, &freq);

    if (status != ALT_E_SUCCESS)
        return status;

    printf("----Private timer\n");
    //uint32_t counter = (freq / 1000) * period_in_ms;
    status = alt_gpt_counter_set(ALT_GPT_CPU_PRIVATE_TMR, (freq/1000 - 1));
    //printf("INFO: Period    = %" PRIu32 " millisecond(s).\n", period_in_ms);
    //printf("INFO: Counter   = %" PRIu32 ".\n", counter);

    alt_gpt_mode_set(ALT_GPT_CPU_PRIVATE_TMR, ALT_GPT_RESTART_MODE_PERIODIC);

    alt_gpt_int_enable(ALT_GPT_CPU_PRIVATE_TMR);

    alt_gpt_tmr_start(ALT_GPT_CPU_PRIVATE_TMR); 

    status = alt_gpt_tmr_is_running(ALT_GPT_CPU_PRIVATE_TMR);
    if(status == ALT_E_TRUE)
        printf("INFO: Private Timer is running.\n");

    printf("INFO: Private Timer running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_CPU_PRIVATE_TMR));
    printf("----end of private timer\n");

    return status;
}

ALT_STATUS_CODE set_general_purpose_timers(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    /* osc1_tmr0 */

    printf("----OSC1 timer 0\n");
    alt_gpt_all_tmr_init();

    alt_gpt_mode_set(ALT_GPT_OSC1_TMR0, ALT_GPT_RESTART_MODE_PERIODIC);
    alt_gpt_counter_set(ALT_GPT_OSC1_TMR0, 25);
    alt_gpt_tmr_start(ALT_GPT_OSC1_TMR0);
    status = alt_gpt_tmr_is_running(ALT_GPT_OSC1_TMR0);
    if(status == ALT_E_TRUE)
        printf("INFO: OSC1 Timer0 is running.\n");

    printf("INFO: OSC1 Timer0 running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_OSC1_TMR0));

    printf("INFO: OSC1 Timer0 Period in microseconds is %" PRIu32 ".\n",
        alt_gpt_time_microsecs_get(ALT_GPT_OSC1_TMR0));

    /* osc1_tmr1 */

    printf("----OSC1 timer 1\n");
    alt_gpt_mode_set(ALT_GPT_OSC1_TMR1, ALT_GPT_RESTART_MODE_PERIODIC);
    alt_gpt_tmr_start(ALT_GPT_OSC1_TMR1);
    status = alt_gpt_tmr_is_running(ALT_GPT_OSC1_TMR1);
    if(status == ALT_E_TRUE)
        printf("INFO: OSC1 Timer1 is running.\n");

    printf("INFO: OSC1 Timer1 running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_OSC1_TMR1));

    /* 
     * sp_tmr0 : gets clock 100MHz, set period of 1us
     */

    printf("----SP timer 0\n");
    alt_gpt_mode_set(ALT_GPT_SP_TMR0, ALT_GPT_RESTART_MODE_PERIODIC);
    alt_gpt_counter_set(ALT_GPT_SP_TMR0, 100);
    alt_gpt_tmr_start(ALT_GPT_SP_TMR0);
    status = alt_gpt_tmr_is_running(ALT_GPT_SP_TMR0);
    if(status == ALT_E_TRUE)
        printf("INFO: SP Timer0 is running.\n");

    printf("INFO: SP Timer0 running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_SP_TMR0));

    printf("INFO: SP Timer0 Period in microseconds is %" PRIu32 ".\n",
        alt_gpt_time_microsecs_get(ALT_GPT_SP_TMR0));

    /* 
     * sp_tmr1 : gets clock 100MHz
     */

    printf("----SP timer 1\n");
    alt_gpt_mode_set(ALT_GPT_SP_TMR1, ALT_GPT_RESTART_MODE_PERIODIC);
    alt_gpt_tmr_start(ALT_GPT_SP_TMR1);
    status = alt_gpt_tmr_is_running(ALT_GPT_SP_TMR1);
    if(status == ALT_E_TRUE)
        printf("INFO: SP Timer1 is running.\n");

    printf("INFO: SP Timer1 running at %" PRIu32 "Hz.\n",
        alt_gpt_freq_get(ALT_GPT_SP_TMR1));

    printf("INFO: SP Timer1 Period in microseconds is %" PRIu32 ".\n",
        alt_gpt_time_microsecs_get(ALT_GPT_SP_TMR1));

    printf("--------------\n");
    return status;
}

ALT_STATUS_CODE socfpga_int_start(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Initialize the global and CPU interrupt items
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_init();
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_int_global_init() failed, %" PRIi32 ".\n", status);
        }
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_init();
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_int_cpu_init() failed, %" PRIi32 ".\n", status);
        }
    }

    // Enable the CPU and global interrupt
    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_cpu_enable();
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_int_cpu_enable() failed, %" PRIi32 ".\n", status);
        }
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_int_global_enable();
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_int_global_enable() failed, %" PRIi32 ".\n", status);
        }
    }   
        
    return status;
}       

void socfpga_int_stop(void)
{
    //
    // Disable the global, CPU, and distributor interrupt
    //

    if (alt_int_global_disable() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_int_global_disable() return non-SUCCESS.");
    }

    if (alt_int_cpu_disable() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_int_cpu_disable() return non-SUCCESS.");
    }

    //
    // Uninitialize the CPU and global data structures.
    //

    if (alt_int_cpu_uninit() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_int_cpu_uninit() return non-SUCCESS.\n");
    }

    if (alt_int_global_uninit() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_int_global_uninit() return non-SUCCESS.\n");
    }
}

ALT_STATUS_CODE socfpga_dma_setup(ALT_DMA_CHANNEL_t * allocated)
{
    printf("INFO: Setup DMA System ...\n");

    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    if (status == ALT_E_SUCCESS)
    {
        // Configure everything as defaults.

        ALT_DMA_CFG_t dma_config;
        dma_config.manager_sec = ALT_DMA_SECURITY_DEFAULT;
        for (int i = 0; i < 8; ++i)
        {
            dma_config.irq_sec[i] = ALT_DMA_SECURITY_DEFAULT;
        }
        for (int i = 0; i < 32; ++i)
        {
            dma_config.periph_sec[i] = ALT_DMA_SECURITY_DEFAULT;
        }
        for (int i = 0; i < 4; ++i)
        {
            dma_config.periph_mux[i] = ALT_DMA_PERIPH_MUX_DEFAULT;
        }

        status = alt_dma_init(&dma_config);
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_dma_init() failed.\n");
        }
    }

    // Allocate the DMA channel

    if (status == ALT_E_SUCCESS)
    {
        status = alt_dma_channel_alloc_any(allocated);
        if (status != ALT_E_SUCCESS)
        {
            printf("ERROR: alt_dma_channel_alloc_any() failed.\n");
        }
        else
        {
            printf("INFO: Channel %d allocated.\n", *allocated);
        }
    }

    // Verify channel state

    if (status == ALT_E_SUCCESS)
    {
        ALT_DMA_CHANNEL_STATE_t state;
        status = alt_dma_channel_state_get(*allocated, &state);
        if (status == ALT_E_SUCCESS)
        {
            if (state != ALT_DMA_CHANNEL_STATE_STOPPED)
            {
                printf("ERROR: Bad initial channel state.\n");
                status = ALT_E_ERROR;
            }
        }
    }

    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Setup of DMA successful.\n\n");
    }
    else
    {
        printf("ERROR: Setup of DMA return non-SUCCESS %" PRIi32 ".\n\n", status);
    }

    return status;
}

void socfpga_dma_cleanup(ALT_DMA_CHANNEL_t channel)
{
    printf("INFO: Cleaning up DMA System ...\n");

    if (alt_dma_channel_free(channel) != ALT_E_SUCCESS)
    {
        printf("WARN: alt_dma_channel_free() returned non-SUCCESS.\n");
    }

    if (alt_dma_uninit() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_dma_uninit() returned non-SUCCESS.\n");
    }
}

ALT_STATUS_CODE socfpga_fpga_setup(ALT_DMA_CHANNEL_t dma_channel)
{
    printf("INFO: Setup FPGA System ...\n");

    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    if (status == ALT_E_SUCCESS)
    {
        status = alt_fpga_init();
    }

    // Verify power is on
    if (status == ALT_E_SUCCESS)
    {
        if (alt_fpga_state_get() == ALT_FPGA_STATE_POWER_OFF)
        {
            printf("ERROR: FPGA Monitor reports FPGA is powered off.\n");
            status = ALT_E_ERROR;
        }
    }

    // Take control of the FPGA CB
    if (status == ALT_E_SUCCESS)
    {
        status = alt_fpga_control_enable();
    }

    // Verify the MSELs are appropriate for the type of image we're using.
    if (status == ALT_E_SUCCESS)
    {
        ALT_FPGA_CFG_MODE_t mode = alt_fpga_cfg_mode_get();
        switch (mode)
        {
        case ALT_FPGA_CFG_MODE_PP32_FAST_AESOPT_DC:
        case ALT_FPGA_CFG_MODE_PP32_SLOW_AESOPT_DC:
        case ALT_FPGA_CFG_MODE_PP16_FAST_AESOPT_DC:
        case ALT_FPGA_CFG_MODE_PP16_SLOW_AESOPT_DC:
            printf("INFO: MSEL [%d] configured correctly for FPGA image.\n", mode);
            break;
        default:
            printf("ERROR: Incompatible MSEL [%d] set. Cannot continue with FPGA programming.\n", mode);
            status = ALT_E_ERROR;
            break;
        }
    }

    // Program the FPGA
    if (status == ALT_E_SUCCESS)
    {
        // This is the symbol name for the SOF file contents linked in.
        extern char _binary_soc_system_dc_rbf_start;
        extern char _binary_soc_system_dc_rbf_end;

        // Use the above symbols to extract the FPGA image information.
        const char *   fpga_image      = &_binary_soc_system_dc_rbf_start;
        const uint32_t fpga_image_size = &_binary_soc_system_dc_rbf_end - &_binary_soc_system_dc_rbf_start;

        // Trace the FPGA image information.
        printf("INFO: FPGA Image binary at %p.\n", fpga_image);
        printf("INFO: FPGA Image size is %" PRIu32 " bytes.\n", fpga_image_size);

        // Try the full configuration a few times.
        const uint32_t full_config_retry = 5;
        for (uint32_t i = 0; i < full_config_retry; ++i)
        {
            status = alt_fpga_configure_dma(fpga_image, fpga_image_size, dma_channel);
            if (status == ALT_E_SUCCESS)
            {
                printf("INFO: alt_fpga_configure() successful on the %" PRIu32 " of %" PRIu32 " retry(s).\n",
                       i + 1,
                       full_config_retry);
                break;
            }
        }
    }

    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Setup of FPGA successful.\n\n");
    }
    else
    {
        printf("ERROR: Setup of FPGA return non-SUCCESS %" PRIi32 ".\n\n", status);
    }

    return status;
}

void socfpga_fpga_cleanup(void)
{
    printf("INFO: Cleanup of FPGA ...\n");

    if (alt_fpga_control_disable() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_fpga_control_disable() returned non-SUCCESS.\n");
    }

    if (alt_fpga_uninit() != ALT_E_SUCCESS)
    {
        printf("WARN: alt_fpga_uninit() returned non-SUCCESS.\n");
    }
}

ALT_STATUS_CODE socfpga_bridge_setup(ALT_BRIDGE_t bridge)
{
    printf("INFO: Setup Bridge [%d] ...\n", bridge);

    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    if (status == ALT_E_SUCCESS)
    {
        status = alt_bridge_init(bridge, NULL, NULL);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = alt_addr_space_remap(ALT_ADDR_SPACE_MPU_ZERO_AT_BOOTROM,
                                      ALT_ADDR_SPACE_NONMPU_ZERO_AT_OCRAM,
                                      ALT_ADDR_SPACE_H2F_ACCESSIBLE,
                                      ALT_ADDR_SPACE_LWH2F_ACCESSIBLE);
    }

    if (status == ALT_E_SUCCESS)
    {
        printf("INFO: Setup of Bridge [%d] successful.\n\n", bridge);
    }
    else
    {
        printf("ERROR: Setup of Bridge [%d] return non-SUCCESS %" PRIi32 ".\n\n", bridge, status);
    }

    return status;
}

void socfpga_bridge_cleanup(ALT_BRIDGE_t bridge)
{
    printf("INFO: Cleanup of Bridge [%d] ...\n", bridge);

    if (alt_bridge_uninit(bridge, NULL, NULL) != ALT_E_SUCCESS)
    {
        printf("WARN: alt_bridge_uninit() returned non-SUCCESS.\n");
    }
}

ALT_STATUS_CODE socfpga_bridge_io(void)
{
    printf("INFO: Demostrating IO across bridge ...\n");

    // Attempt to read the system ID peripheral
    uint32_t sysid = alt_read_word(ALT_LWFPGA_BASE + ALT_LWFPGA_SYSID_OFFSET);
    printf("INFO: LWFPGA Slave => System ID Peripheral value = 0x%" PRIx32 ".\n", sysid);

    // Attempt to toggle the 4 LEDs
    const uint32_t bits = 4;
    printf("INFO: Toggling LEDs ...\n");
    for (uint32_t i = 0; i < (1 << bits); ++i)
    {
        // Use Gray code ... for fun!
        // http://en.wikipedia.org/wiki/Gray_code
        uint32_t gray = (i >> 1) ^ i;

        alt_write_word(ALT_LWFPGA_BASE + ALT_LWFPGA_LED_OFFSET, gray);

        //printf("INFO: Gray code(i=0x%x) => 0x%x [", (unsigned int)i, (unsigned int)gray);

        //for (uint32_t j = 0; j < bits; ++j) {
        //    printf("%c", (gray & (1 << (bits - j - 1))) ? '1' : '0');
        //}

        //printf("].\n");
    }

    // Reset the LEDs to all on
    alt_write_word(ALT_LWFPGA_BASE + ALT_LWFPGA_LED_OFFSET, 0);

    printf("INFO: LEDs should have blinked.\n\n");

    return ALT_E_SUCCESS;
}

int main(int argc, char** argv)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    ALT_DMA_CHANNEL_t channel;

    status = socfpga_int_start();

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_dma_setup(&channel);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_fpga_setup(channel);
    }

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_bridge_setup(ALT_BRIDGE_LWH2F);
    }

    /* Initialize HPS PB and enable intrr */
    if(status == ALT_E_SUCCESS) {
        status = alt_gpio_init();
    }

    if (status == ALT_E_SUCCESS) {
        status = alt_int_dist_target_set(ALT_INT_INTERRUPT_GPIO2, 0x1);
    }

    if (status == ALT_E_SUCCESS) {
        status = alt_int_dist_trigger_set(ALT_INT_INTERRUPT_GPIO2, ALT_INT_TRIGGER_LEVEL);
    }

    if(status == ALT_E_SUCCESS)
    {
        status = alt_gpio_group_config(pb_gpio_init, ARRAY_COUNT(pb_gpio_init));
    }

    alt_int_isr_register(ALT_INT_INTERRUPT_GPIO2, gpio_isr_callback, NULL);

    status = alt_int_dist_enable(ALT_INT_INTERRUPT_GPIO2);

    if (status == ALT_E_SUCCESS) {
        status = alt_gpio_port_int_enable(ALT_GPIO_PORTC, HPS_PB_INT_ALL_BIT_MASK);
    }

    /* Enable intrr from FPGA to HPS */
    if (status == ALT_E_SUCCESS)
        status = alt_int_dist_target_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1, 0x1);

    if (status == ALT_E_SUCCESS)
        status = alt_int_dist_trigger_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1, ALT_INT_TRIGGER_LEVEL);

    alt_int_dist_priority_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1, 8);

    status = alt_int_dist_enable(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1);

    alt_int_isr_register(ALT_INT_INTERRUPT_F2S_FPGA_IRQ1, fpga_isr_callback, NULL);

    /* unmask FPGA's PIO interrupts */
    alt_write_word(ALT_LWFPGA_BASE + ALT_LWFPGA_PIO_OFF + ALT_LWFPGA_PIO_INTR_MSK_OFF, 0xf);
    uint32_t val = alt_read_word(ALT_LWFPGA_BASE + ALT_LWFPGA_PIO_OFF + ALT_LWFPGA_PIO_INTR_MSK_OFF);
    printf("INFO: LWFPGA Slave => PIO Intrr mask = 0x%" PRIx32 "\n", val);

    /* Enable intrr IRQ3 from FPGA to HPS */
    uint32_t d = alt_read_word(ALT_LWFPGA_BASE + 0x40000);
    printf("irq3 %ld\n", d);
    alt_write_word(ALT_LWFPGA_BASE + 0x40000, 0x28); 
    d = alt_read_word(ALT_LWFPGA_BASE + 0x40000);
    printf("irq3 %ld\n", d);

    if (status == ALT_E_SUCCESS)
        status = alt_int_dist_target_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ3, 0x1);

    if (status == ALT_E_SUCCESS)
        status = alt_int_dist_trigger_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ3, ALT_INT_TRIGGER_EDGE);

    alt_int_dist_priority_set(ALT_INT_INTERRUPT_F2S_FPGA_IRQ3, 8);

    status = alt_int_dist_enable(ALT_INT_INTERRUPT_F2S_FPGA_IRQ3);

    alt_int_isr_register(ALT_INT_INTERRUPT_F2S_FPGA_IRQ3, fpga3_isr_callback, NULL);

    /* Enable intrr IRQ4 from FPGA to HPS */
    d = alt_read_word(ALT_LWFPGA_BASE + 0x40020);

    printf("irq4 %ld\n", d);

    /* get all clocks */
    get_clocks();

    /* set general purpose timers */
    set_general_purpose_timers();

    /* set global timer */
    set_global_timer();

    /* set private timer */
    set_private_timer();

    /////

    if (status == ALT_E_SUCCESS)
    {
        status = socfpga_bridge_io();
    }

    /////

    printf("INFO: Ready\n");

    if(status == ALT_E_SUCCESS) {
        while (push_button_stat != HPS_PB_3_ASSERT) {

        // While loop to wait for ...

        }
    }

    socfpga_bridge_cleanup(ALT_BRIDGE_LWH2F);
    socfpga_fpga_cleanup();
    socfpga_dma_cleanup(channel);
    socfpga_int_stop();
    printf("\n");

    if (status == ALT_E_SUCCESS)
    {
        printf("RESULT: Example completed successfully.\n");
        return 0;
    }
    else
    {
        printf("RESULT: Some failures detected.\n");
        return 1;
    }
}

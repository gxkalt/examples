/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

/******************************************************************************/
/*!
 * Write a QSPI Flash using the Indirect Mode.
 *
 * The whole write must be confined to a single Flash page (256 bytes).
 *
 * \param       address  Where to write the data in Flash. Must be 4-byte aligned.
 * \param       buffer Data to write to Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_write_indirect(uint32_t address, void * buffer, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t * data = (uint32_t*)buffer;
    uint32_t write_capacity = ALT_QSPI_SRAM_FIFO_ENTRY_COUNT - alt_qspi_sram_partition_get();

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_write_start(address, size);
    }

    // Write data
    while ((size != 0) && (status == ALT_E_SUCCESS))
    {
        uint32_t space = write_capacity - alt_qspi_indirect_write_fill_level();
        space = MIN(space, size/sizeof(uint32_t));

        // Write the FIFO
        for (uint32_t i = 0; i < space; ++i)
        {
            alt_write_word(ALT_QSPIDATA_ADDR, *data++);
        }

        // Decrement size
        size -= space * sizeof(uint32_t);
    }

    // Complete the read
    if(status != ALT_E_ERROR)
    {
        alt_qspi_indirect_write_finish();
    }

    // Check we have read all the data
    if(size != 0)
    {
        status = ALT_E_ERROR;
    }

    return status;
}

/******************************************************************************/
/*!
 * Read from QSPI Flash using the DMA.
 *
 * The whole read must be confined to a single Flash bank (16 MBbytes).
  *
 * \param       buffer Location to read the data to. Must be 4-byte aligned.
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_read_indirect(void * buffer, uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t * data = (uint32_t*)buffer;

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_read_start(address, size);
    }

    // Read data
    while (!alt_qspi_indirect_read_is_complete() && (status == ALT_E_SUCCESS))
    {
        // Read the fill level
        uint32_t level = alt_qspi_indirect_read_fill_level();

        // Read the FIFO
        for (uint32_t i = 0; i < level; ++i)
        {
            *data++ = alt_read_word(ALT_QSPIDATA_ADDR);
        }

        // Decrement size
        size -= level * sizeof(uint32_t);
    }

    // Complete the read
    if(status != ALT_E_ERROR)
    {
        alt_qspi_indirect_read_finish();
    }

    // Check we have read all the data
    if(size != 0)
    {
        status = ALT_E_ERROR;
    }

    return status;
}

/******************************************************************************/
/*!
 * Demonstrates reading and writing from/to QSPI Flash Memory using Indirect Mode
 *
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_indirect(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    printf("INFO: Demo indirect mode using address = 0x%08x, size = %d bytes.\n", (int)address, (int)size);

    // Erase flash
    status = qspi_demo_erase(address, size);

    // Write 'write_buffer' to flash - by pages
    if(status ==  ALT_E_SUCCESS)
    {

        if(alt_qspi_is_micron_multidie())
        {
            // For multiple dies we need to split the write into pages so that we can read the flag status register after each

            char * crt_data = (char*)Write_Buffer;
            uint32_t crt_size = size;
            uint32_t crt_address = address;
            uint32_t page_size = alt_qspi_get_page_size();

            // Split into pages and write it
            while((crt_size != 0) & (status == ALT_E_SUCCESS))
            {
                // Limit writing size to filling the current page
                uint32_t write_size = page_size - (crt_address % page_size);
                write_size = MIN(write_size, crt_size);

                // Write the data
                printf("INFO: Writing to address 0x%08x, size = %d bytes using indirect mode.\n", (int)crt_address, (int)write_size);
                status = qspi_write_indirect(crt_address, crt_data, write_size);

                // Update addresses and size
                crt_size -= write_size;
                crt_address += write_size;
                crt_data += write_size;
            }

            // Check that it ended correctly
            if(crt_size != 0)
            {
                status =  ALT_E_ERROR;
            }
        }
        else
        {
            printf("INFO: Writing to address 0x%08x, size = %d bytes using indirect mode.\n", (int)address, (int)size);
            // split it into pages for multi die
            status = qspi_write_indirect(address, Write_Buffer, size);
        }
    }

    //  Read 'read_buffer' from flash
    if(status ==  ALT_E_SUCCESS)
    {

        if(alt_qspi_is_micron_multidie())
        {
            // For multiple dies we need to split the read into die-contained pieces
            char * crt_data = (char*)Read_Buffer;
            uint32_t crt_size = size;
            uint32_t crt_address = address;
            uint32_t die_size = alt_qspi_get_die_size();

            // Split into dies and read it
            while((crt_size != 0) & (status == ALT_E_SUCCESS))
            {
                // Limit writing size to filling the current die
                uint32_t read_size = die_size - (crt_address % die_size);
                read_size = MIN(read_size, crt_size);

                printf("INFO: Reading from address 0x%08x, size = %d bytes using indirect mode.\n", (int)crt_address, (int)read_size);
                status = qspi_read_indirect(crt_data, crt_address, read_size);

                if(status == ALT_E_SUCCESS)
                {
                    // Update addresses and size
                    crt_size -= read_size;
                    crt_address += read_size;
                    crt_data += read_size;
                }
            }

            // Check that it ended correctly
            if(crt_size != 0)
            {
                status =  ALT_E_ERROR;
            }
        }
        else
        {
            printf("INFO: Reading from address 0x%08x, size = %d bytes using indirect mode.\n", (int)address, (int)size);
            status = qspi_read_indirect(Read_Buffer, address, size);
        }
    }

    // Compare buffers
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Comparing written data with read back data.\n");
        if(memcmp(Read_Buffer, Write_Buffer, size) != 0)
        {
            status = ALT_E_ERROR;
        }
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Demo indirect mode succeeded.\n");
    }
    else
    {
        printf("ERROR: DEMO indirect mode failed.\n");
    }

    printf("\n");

    return status;
}



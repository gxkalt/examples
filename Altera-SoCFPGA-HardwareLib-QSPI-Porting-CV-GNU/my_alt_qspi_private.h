/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

/*! \file
 *  Altera - QSPI Flash Controller Module
 */

#ifndef __MY_ALT_QSPI_PRIVATE_H__
#define __MY_ALT_QSPI_PRIVATE_H__

#include "socal/socal.h"

#define ALT_QSPI_MAX_ERASE_CMDS      4
#define ALT_QSPI_ERASE_CHIP_SIZE     0xffffffff
#define ALT_QSPI_INIT_BAUDRATE_MHZ    25

typedef ALT_STATUS_CODE (*ALT_QSPI_INIT_FUNC_T)(void);
typedef ALT_STATUS_CODE (*ALT_QSPI_WAIT_FUNC_T)(uint32_t timeout);

typedef struct ALT_QSPI_DEV_CONFIG_s
{
    ALT_QSPI_CLK_PHASE_t    clk_phase;
    ALT_QSPI_CLK_POLARITY_t clk_pol;
    uint32_t                max_freq_mhz;
    uint32_t                tshsl_ns;
    uint32_t                tsd2d_ns;
    uint32_t                tchsh_ns;
    uint32_t                tslch_ns;
    uint32_t                page_size;
    uint32_t                addr_size;
    ALT_QSPI_MODE_t         inst_type;
    uint32_t                read_op_code;
    ALT_QSPI_MODE_t         read_addr_xfer_type;
    ALT_QSPI_MODE_t         read_data_xfer_type;
    uint32_t                read_dummy_cycles;
    uint32_t                write_op_code;
    ALT_QSPI_MODE_t         write_addr_xfer_type;
    ALT_QSPI_MODE_t         write_data_xfer_type;
    uint32_t                write_dummy_cycles;
    uint8_t                 erase_count;
    uint32_t                erase_sizes[ALT_QSPI_MAX_ERASE_CMDS];
    uint8_t                 erase_cmds[ALT_QSPI_MAX_ERASE_CMDS];
    uint8_t                 erase_sector_idx;
    bool                    support_chip_erase;
    uint32_t                device_size;
    bool                    micron_multi_die;
    uint32_t                die_size;
    ALT_QSPI_INIT_FUNC_T    init_func;
    ALT_QSPI_WAIT_FUNC_T    wait_func;
    const char *            friendly_name;
} ALT_QSPI_DEV_CONFIG_t;

ALT_STATUS_CODE alt_qspi_confiugre_device(ALT_QSPI_DEV_CONFIG_t * config);

// Initial timing parameters - use very relaxed values
#define ALT_QSPI_TSHSL_NS_DEF       (100)
#define ALT_QSPI_TSD2D_NS_DEF       (30)
#define ALT_QSPI_TCHSH_NS_DEF       (30)
#define ALT_QSPI_TSLCH_NS_DEF       (30)

// Manufacturer ID Values
#define ALT_QSPI_STIG_RDID_JEDECID_MICRON      (0x20)
#define ALT_QSPI_STIG_RDID_JEDECID_SPANSION    (0x01)
#define ALT_QSPI_STIG_RDID_JEDECID_MACRONIX    (0xC2)

// JEDEC ID Macros
#define ALT_QSPI_STIG_RDID_JEDECID_GET(value)        ((value >>  0) & 0xff)
#define ALT_QSPI_STIG_RDID_TYPE_CAPACITY_GET(value)  ((value >>  8) & 0xffff)

// Common instructions
#define ALT_QSPI_STIG_OPCODE_RD_STAT_REG          (0x05)
#define ALT_QSPI_STIG_OPCODE_RDID                 (0x9F)
#define ALT_QSPI_STIG_OPCODE_WREN                 (0x06)
#define ALT_QSPI_STIG_OPCODE_WRDIS                (0x04)

/* Macros for accessing Status Register fields */
#define ALT_QSPI_SR_WIP_GET(value)                          ((value >> 0) & 0x1)

#define ALT_QSPI_TIMEOUT_INFINITE (0xffffffff)

ALT_STATUS_CODE alt_qspi_stig_cmd(uint32_t opcode, uint32_t dummy, uint32_t timeout);
ALT_STATUS_CODE alt_qspi_stig_rd_cmd(uint8_t opcode, uint32_t dummy,
                                     uint32_t num_bytes, uint32_t * output,
                                     uint32_t timeout);
ALT_STATUS_CODE alt_qspi_stig_wr_cmd(uint8_t opcode, uint32_t dummy,
                                     uint32_t num_bytes, const uint32_t * input,
                                     uint32_t timeout);
ALT_STATUS_CODE alt_qspi_stig_addr_cmd(uint8_t opcode, uint32_t dummy,
                                       uint32_t address,
                                       uint8_t addrbytes,
                                       uint32_t timeout);

ALT_STATUS_CODE alt_qspi_device_wren(void);
ALT_STATUS_CODE alt_qspi_device_rdid(uint32_t * rdid);

#endif // __ALT_PRIVATE_QSPI_H__

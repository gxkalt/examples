Introduction

This HardwareLibs project is meant as an example for porting the QSPI Flash APIs 
to support more QSPI Flash devices.

The project was created by first copying the HWLIBs QSPI-related sources to the project
then editing the files to add support for more devices.

The following devices are supported:
- MICRON M25P40
- MICRON M25P16
- MICRON M25PX16
- SPANSION S25FL116K
- SPANSION S25FL256S
- SPANSION S25FL512S
- MACRONIX MX25L25635
- MACRONIX MX66L51235
- MICRON N25Q128
- MICRON N25Q512A (*)
- MICRON N25Q00AA (*)

(*) Note that the current HWLIBs APIs support only the devices marked by asterisk.

The project demonstrates the following QSPU Flash API features:
- Reading and writing using Generic Block I/O functions
- Reading and writing using Indirect Mode
- Reading and writing using DMA Mode
- Erasing

The project also demonstrates the following additional API features:
- Setting up MMU and caches
- Performing cache cleanup and purging
- Setting up DMA, and using Memory2Peripheral and Peripheral2Memory operations

Note: This example was developed with and tested against SoC EDS 14.0b199.

====

Target Boards:
- Altera Cyclone V SoC Development Board rev D

In order to be able to test multiple QSPI devices with the Cyclone V Board,
the onboard QSPI device was replaced with a SOIC socket, allowing the QSPI
Flash device to be replaced easily.
====

QSPI Background Information

Different QSPI devices have different capabilities, so the code has to be customized to deal with
each type of device.

Some of the things that vary between different QSPI flash chips are:
- Instructions used for initialization:
    - reset chip, if available
    - configure chip to quad mode if available
    - configure number of address bytes if extended address instructions not available
- Instructions used for erasing: 
    - opcodes
    - supported erase sizes: sector, subsector, bulk etc
    - number of address bytes
- Instructions used for reading: 
    - opcodes
    - number of lines for address, data
    - number of dummy bits, if applicable    
- Instructions used for writing 
    - opcodes
    - page size
    - number of lines for address, data
    - number of dummy bits, if applicable
    
Another aspect is that some of the Micron multi-die devices involve further customizations:
- They require reading a special Flag Status Register to determine when a write operation has completed. 
Since the Cyclone V/Arria V SoC QSPI Flash Controller does not automatically poll this register, each 
write operation needs to be split into page-sized chunks, and after each write the register needs to 
be polled manually.
- They do not have a single command for erasing the whole chip, instead each die can be erased separately
- They cannot read past a die boundary, so reads need to be split into chunks fitting into a single die.

Please consult the following files:
- my_alt_qspi_private.h - for defining device parameters
- alt_qspi_device_specific.c - for instantiating parameters for various devices
- my_alt_qspi.c - for generic code that is used for all devices
    
====

Limitations:

1. The DMA mode write function only programs one page at a time. This should only be necessary
for the Micron multi-die devices, but there seems to be a limitation with the DMA driver code.

2. There is a known issue with large QSPI writes over DMA. Therefore the DMA write was temporarily 
disabled in the code and replaced with block I/O write. This is expected to be fixed in the next version
of this example.

=====

Source Files

The following are descriptions of the source and header files contained in this
project:

alt_pt.{c,h}

  Contain the functions to setup the MMU translation tables.

qspi_demo.{c,h}

  Contain the main, system initialization and system cleanup functions.

qspi_demo_blockio.c

  Contains the functions demonstrating QSPI Flash generic block I/O APIs.

qspi_demo_dma.c

  Contains the functions demonstrating QSPI Flash DMA APIs.

qspi_demo_erase.c

  Contains the functions demonstrating QSPI Flash erase APIs.

qspi_demo_indirect.c

  Contains the functions demonstrating QSPI Flash indirect mode APIs.
  
my_alt_qspi_private.h
my_alt_qspi.h
my_alt_qspi.c

  Files copied from HWLIBS, renamed and modified to add support for more QSPI devices
  
alt_qspi_device_specific.c
  
  Contains the device specific functions and parameters
  
=====

Building Example

Before running the example, the target executable first needs to be built.

1. In DS-5, build the application:
  1a. Switch to the C/C++ Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> C/C++.
  1b. In the "Project Explorer" panel, right-mouse-click 
      "Altera-SoCFPGA-HardwareLib-QSPI-Porting-CV-GNU" and select "Build Project".

The Console panel (bottom of the UI) should detail the progress of the build
and report any warnings or errors.

=====

System Setup

1. Connect the USB to serial bridge to the host computer.
2. Connect the USB-BlasterII to the host computer.
3. Install the USB to serial bridge driver on the host computer if that driver
   is not already present. Consult documentation for the DevKit for
   instructions on installing the USB to serial bridge driver.
4. Install the USB-BlasterII driver on the host computer if that driver is not
   already present. Consult documentation for QuartusII for instructions on
   installing the USB-BlasterII driver.
5. In DS-5, configure the launch configuration.
  5a. Select the menu: Run >> Debug Configurations...
  5b. In the options on the left, expand "DS-5 Debugger" and select
      "Altera-SoCFPGA-HardwareLib-QSPI-Porting-CV-GNU".
  5c. In the "Connections" section near the bottom, click Browse.
  5d. Select the appropriate USB-BlasterII to use. Multiple items will be
      presented if there is more than one USB-BlasterII connection attached to
      the host computer.
  5e. Click "Apply" then "OK" to apply the USB-BlasterII selection.
  5f. Click "Close" to close the Debug Configuration. Otherwise click "Debug"
      run the example in the debugger.

=====

Running the Example

After building the example and setting up the host computer system, the example
can be run by following these steps.

1. In DS-5, launch the debug configuration.
  1a. Switch to the Debug Perspective if not already in that perspective by
      selecting the menu: Window >> Open Perspective >> DS-5 Debug.
  1b. In the "Debug Control" panel, right-mouse-click
      "Altera-SoCFPGA-HardwareLib-QSPI-Porting-CV-GNU" and select
      "Connect to Target".

Connecting to the target takes a moment to load the preloader, run the
preloader, load the executable, and run executable. After the debug connection
is established, the debugger will pause the execution at the main() function.
Users can then set additional break points, step into, step out of, or step one
line using the DS-5 debugger. Consult documentation for DS-5 for more
information on debugging operations.

=====

Sample output

The address and size of the test buffers are randomly generated for each test, to help 
with wear leveling of the QSPI Flash memory device. 

The following is a sample output (for MICRON N25Q512A):

INFO: System Initialization.
INFO: Setting up Global Timer.
INFO: Setting up interrupt system.
INFO: Populating page table and enabling MMU.
INFO: Enabling caches.
INFO: Configuring DMA.
INFO: Allocating DMA channel.
INFO: Initializing QSPI.
INFO: Configuring QSPI DMA single size = 4, burst_size = 32 bytes.
INFO: Enabling QSPI.
INFO: Checking QSPI is idle.
INFO: Detected Device = MICRON_N25Q512A. Size = 64 MBytes.
INFO: Using random seed = 0xd8a58d4.

INFO: Demo block I/O using address = 0x011e87e0, size = 3624 bytes.
INFO: Saving 2016 bytes from address 0x11e8000 which also need to be erased.
INFO: Saving 2552 bytes from address 0x11e9608 which also need to be erased.
INFO: Erasing 8192 bytes at address 0x011e8000.
INFO: Restoring 2016 bytes to address 0x011e8000.
INFO: Restoring 2552 bytes to address 0x011e9608.
INFO: Erasing completed successfully.
INFO: Writing to address 0x011e87e0, size = 3624 bytes using block I/O.
INFO: Reading from address 0x011e87e0, size = 3624 bytes using block I/O.
INFO: Comparing written data with read back data.
INFO: Demo block I/O succeeded.

INFO: Demo indirect mode using address = 0x005f6868, size = 7284 bytes.
INFO: Saving 2152 bytes from address 0x5f6000 which also need to be erased.
INFO: Saving 2852 bytes from address 0x5f84dc which also need to be erased.
INFO: Erasing 12288 bytes at address 0x005f6000.
INFO: Restoring 2152 bytes to address 0x005f6000.
INFO: Restoring 2852 bytes to address 0x005f84dc.
INFO: Erasing completed successfully.
INFO: Writing to address 0x005f6868, size = 152 bytes using indirect mode.
INFO: Writing to address 0x005f6900, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6a00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6b00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6c00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6d00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6e00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f6f00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7000, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7100, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7200, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7300, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7400, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7500, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7600, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7700, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7800, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7900, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7a00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7b00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7c00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7d00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7e00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f7f00, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f8000, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f8100, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f8200, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f8300, size = 256 bytes using indirect mode.
INFO: Writing to address 0x005f8400, size = 220 bytes using indirect mode.
INFO: Reading from address 0x005f6868, size = 7284 bytes using indirect mode.
INFO: Comparing written data with read back data.
INFO: Demo indirect mode succeeded.

INFO: Demo DMA mode using address = 0x00f6b940, size = 4188 bytes.
INFO: Saving 2368 bytes from address 0xf6b000 which also need to be erased.
INFO: Saving 1636 bytes from address 0xf6c99c which also need to be erased.
INFO: Erasing 8192 bytes at address 0x00f6b000.
INFO: Restoring 2368 bytes to address 0x00f6b000.
INFO: Restoring 1636 bytes to address 0x00f6c99c.
INFO: Erasing completed successfully.
INFO: Enabling QSPI DMA operation.
INFO: Writing to address 0x00f6b940, size = 4188 bytes using BLOCKIO mode.
INFO: Reading from address 0x00f6b940, size = 4188 bytes using DMA mode.
INFO: Comparing written data with read back data.
INFO: Demo DMA mode succeeded.

INFO: System shutdown.

RESULT: All tests successful.

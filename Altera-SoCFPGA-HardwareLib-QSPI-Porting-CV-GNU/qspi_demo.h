/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#ifndef __QSPI_DEMO_H__
#define __QSPI_DEMO_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include "alt_pt.h"
#include "alt_cache.h"
#include "alt_interrupt.h"
#include "my_alt_qspi.h"
#include "my_alt_qspi_private.h"
#include "alt_globaltmr.h"
#include "alt_dma.h"
#include "socal/hps.h"

// Determine the minimum of two values
#define MIN(a, b) ((a) > (b) ? (b) : (a))

// Extend a size to a multiple of cache lines
#define EXTEND_TO_MULTIPLE_OF_CACHE_LINES(len) \
    ((((len) + ALT_CACHE_LINE_SIZE - 1) / ALT_CACHE_LINE_SIZE) * ALT_CACHE_LINE_SIZE)

// Determine size of an array
#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

// Parameters for QSPI DMA Operation
#define QSPI_DMA_SINGLE_SIZE            4
#define QSPI_DMA_BURST_SIZE             32

// Maximum size of test data
#define MAX_TEST_BYTES                  0x00002000 // 8 KiB

// Used to store the data to be written to Flash
extern uint32_t Write_Buffer[MAX_TEST_BYTES / sizeof(uint32_t)];

// Used to store the data that is read from Flash
extern uint32_t Read_Buffer[MAX_TEST_BYTES / sizeof(uint32_t)];

// DMA Channel Number
extern ALT_DMA_CHANNEL_t Dma_Channel;

// QSPI Demo Functions
ALT_STATUS_CODE qspi_demo_erase(uint32_t address, uint32_t size);
ALT_STATUS_CODE qspi_demo_block_io(uint32_t address, uint32_t size);
ALT_STATUS_CODE qspi_demo_indirect(uint32_t address, uint32_t size);
ALT_STATUS_CODE qspi_demo_dma(uint32_t address, uint32_t size);

#endif

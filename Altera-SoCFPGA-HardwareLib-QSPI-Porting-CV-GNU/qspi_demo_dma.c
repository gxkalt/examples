/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

/* QSPI DMA Write has a known problem for writing large DMA blocks. Disabling it temporarily */
#define DISABLE_DMA_WRITE 1

/******************************************************************************/
/*!
 * Write a QSPI Flash using the DMA.
 *
 * The whole write must be confined to a single Flash page (256 bytes).
 * The buffer to be written must be cleaned from caches first, if caches are enabled.
 *
 * \param       address  Where to write the data in Flash. Must be 4-byte aligned.
 * \param       buffer Data to write to Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
#if (DISABLE_DMA_WRITE != 1)
static ALT_STATUS_CODE qspi_write_dma(uint32_t address, void * buffer, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_write_start(address, size);
    }

    // Start the DMA transfer
    if(status == ALT_E_SUCCESS)
    {
        ALT_DMA_PROGRAM_t dma_program;

        status = alt_dma_memory_to_periph(Dma_Channel,
                                          &dma_program,
                                          ALT_DMA_PERIPH_QSPI_FLASH_TX,
                                          buffer,
                                          size,
                                          NULL,
                                          false,
                                          ALT_DMA_EVENT_0);
    }

    // Wait for DMA to complete
    if (status == ALT_E_SUCCESS)
    {
        ALT_DMA_CHANNEL_STATE_t channel_state = ALT_DMA_CHANNEL_STATE_EXECUTING;

        while((status == ALT_E_SUCCESS) && (channel_state != ALT_DMA_CHANNEL_STATE_STOPPED))
        {
            status = alt_dma_channel_state_get(Dma_Channel, &channel_state);
            if(channel_state == ALT_DMA_CHANNEL_STATE_FAULTING)
            {
                ALT_DMA_CHANNEL_FAULT_t fault;
                alt_dma_channel_fault_status_get(Dma_Channel, &fault);
                printf("ERROR: DMA CHannel Fault: %d\n", (int)fault);
                status = ALT_E_ERROR;
            }
        }
    }

    // Complete the read
    if(status == ALT_E_SUCCESS)
    {
        alt_qspi_indirect_write_finish();
    }
    else
    {
        alt_qspi_indirect_write_cancel();
    }

    return status;
}
#endif

/******************************************************************************/
/*!
 * Read from QSPI Flash using the DMA.
 *
 * The whole read must be confined to a single Flash bank (16 MBbytes).
 * The buffer to be read must be invalidated or purged from caches first, if caches are enabled.
 *
 * \param       buffer Location to read the data to. Must be 4-byte aligned.
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
static ALT_STATUS_CODE qspi_read_dma(void * buffer, uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Start the indirect read
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_indirect_read_start(address, size);
    }

    // Start the DMA transfer
    if(status == ALT_E_SUCCESS)
    {
        ALT_DMA_PROGRAM_t dma_program;

        status = alt_dma_periph_to_memory(Dma_Channel,
                                          &dma_program,
                                          buffer,
                                          ALT_DMA_PERIPH_QSPI_FLASH_RX,
                                          size,
                                          NULL,
                                          false,
                                          ALT_DMA_EVENT_0);
    }

    // Wait for DMA to complete
    if (status == ALT_E_SUCCESS)
    {
        ALT_DMA_CHANNEL_STATE_t channel_state = ALT_DMA_CHANNEL_STATE_EXECUTING;

        while((status == ALT_E_SUCCESS) && (channel_state != ALT_DMA_CHANNEL_STATE_STOPPED))
        {
            status = alt_dma_channel_state_get(Dma_Channel, &channel_state);

            if(channel_state == ALT_DMA_CHANNEL_STATE_FAULTING)
            {
                ALT_DMA_CHANNEL_FAULT_t fault;
                alt_dma_channel_fault_status_get(Dma_Channel, &fault);
                printf("ERROR: DMA CHannel Fault: %d\n", (int)fault);
                status = ALT_E_ERROR;
            }
        }
    }

    // Complete the read
    if(status == ALT_E_SUCCESS)
    {
        alt_qspi_indirect_read_finish();
    }
    else
    {
        alt_qspi_indirect_read_cancel();
    }
    return status;

}

/******************************************************************************/
/*!
 * Demonstrates reading and writing from/to QSPI Flash Memory using DMA
 *
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_dma(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Demo DMA mode using address = 0x%08x, size = %d bytes.\n", (int)address, (int)size);

    // Flush the write_buffer to main memory
    alt_cache_system_clean(Write_Buffer, EXTEND_TO_MULTIPLE_OF_CACHE_LINES(size));

    // Erase flash - before enabling DMA since it has read/write
    if(status == ALT_E_SUCCESS)
    {
        status = qspi_demo_erase(address, size);
    }

    // Enabling DMA
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Enabling QSPI DMA operation.\n");
        status = alt_qspi_dma_enable();
    }

    if(status ==  ALT_E_SUCCESS)
    {
#if !(DISABLE_DMA_WRITE)
        if(alt_qspi_is_micron_multidie())
        {
            // On multidie devices we need to split the write into pages
            // Write 'write_buffer' to flash - by pages
            char * crt_data = (char*)Write_Buffer;
            uint32_t crt_size = size;
            uint32_t crt_address = address;

            // Split into pages and write it
            while((crt_size != 0) & (status == ALT_E_SUCCESS))
            {
                uint32_t page_size = alt_qspi_get_page_size();

                // Limit writing size to filling the current page
                uint32_t write_size = page_size - (crt_address % page_size);
                write_size = MIN(write_size, crt_size);

                // Write the data
                printf("INFO: Writing to address 0x%08x, size = %d bytes using DMA mode.\n", (int)crt_address, (int)write_size);
                status = qspi_write_dma(crt_address, crt_data, write_size);

                // Update addresses and size
                crt_size -= write_size;
                crt_address += write_size;
                crt_data += write_size;
            }

            // Check that it ended correctly
            if(crt_size != 0)
            {
                status =  ALT_E_ERROR;
            }
        }
        else
        {
            // On non-multidie devices we can do all write in one step
            printf("INFO: Writing to address 0x%08x, size = %d bytes using DMA mode.\n", (int)address, (int)size);
            status = qspi_write_dma(address, Write_Buffer, size);
        }
#else
        // Write the data
        printf("INFO: Writing to address 0x%08x, size = %d bytes using BLOCKIO mode.\n", (int)address, (int)size);
        status = alt_qspi_write(address, Write_Buffer, size);
#endif
    }

    //  Read 'read_buffer' from flash
    if(status ==  ALT_E_SUCCESS)
    {
        // Purge the read_buffer from main memory
        alt_cache_system_purge(Read_Buffer, EXTEND_TO_MULTIPLE_OF_CACHE_LINES(size));

        // On multidie devices we need to split the read into chunks belonging to a single die at a time
        if(alt_qspi_is_micron_multidie())
        {
            char * crt_data = (char*)Read_Buffer;
            uint32_t crt_size = size;
            uint32_t crt_address = address;
            uint32_t die_size = alt_qspi_get_die_size();

            // Split into dies and read it
            while((crt_size != 0) & (status == ALT_E_SUCCESS))
            {
                // Limit writing size to filling the current die
                uint32_t read_size = die_size - (crt_address % die_size);
                read_size = MIN(read_size, crt_size);

                printf("INFO: Reading from address 0x%08x, size = %d bytes using DMA mode.\n", (int)crt_address, (int)read_size);
                status = qspi_read_dma(crt_data, crt_address, read_size);

                if(status == ALT_E_SUCCESS)
                {
                    // Update addresses and size
                    crt_size -= read_size;
                    crt_address += read_size;
                    crt_data += read_size;
                }
            }

            // Check that it ended correctly
            if(crt_size != 0)
            {
                status =  ALT_E_ERROR;
            }
        }
        else
        {
            printf("INFO: Reading from address 0x%08x, size = %d bytes using DMA mode.\n", (int)address, (int)size);
            status = qspi_read_dma(Read_Buffer, address, size);
        }
    }

    // Compare buffers
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Comparing written data with read back data.\n");
        if(memcmp(Read_Buffer, Write_Buffer, size) != 0)
        {
            status = ALT_E_ERROR;

            for(int i=0; i<size; i++)
            {
                uint8_t ref = ((uint8_t *)Write_Buffer)[i];
                uint8_t tst = ((uint8_t *)Read_Buffer)[i];
                printf("%5d: %s %02x %02x\n", i, (ref==tst) ? "    ": "diff", (int)ref, (int)tst);
            }
        }
    }

    // Disabling DMA
    if(status == ALT_E_SUCCESS)
    {
        alt_qspi_dma_disable();
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Demo DMA mode succeeded.\n");
    }
    else
    {
        printf("ERROR: Demo DMA mode failed.\n");
    }

    printf("\n");

    return status;
}

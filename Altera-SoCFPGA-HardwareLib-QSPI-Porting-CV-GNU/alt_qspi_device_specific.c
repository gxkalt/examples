/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include "hwlib.h"
#include "alt_clock_manager.h"
#include "my_alt_qspi.h"
#include "my_alt_qspi_private.h"
#include "socal/alt_qspi.h"
#include "socal/hps.h"
#include "socal/socal.h"

/////

// NOTE: To enable debugging output, delete the next line and uncomment the
//   line after.
#define dprintf(...)
//#define dprintf printf

/////

// List of devices that are enabled
// Disabling devices reduces program and data sizes
#define ALT_QSPI_SUPPORT_MICRON_M25P40            1 // aka ALTERA EPCS4
#define ALT_QSPI_SUPPORT_MICRON_M25P16            1 // aka ALTERA EPCS16
#define ALT_QSPI_SUPPORT_MICRON_M25PX16         1
#define ALT_QSPI_SUPPORT_SPANSION_S25FL116K     1
#define ALT_QSPI_SUPPORT_SPANSION_S25FL256S     1
#define ALT_QSPI_SUPPORT_SPANSION_S25FL512S     1
#define ALT_QSPI_SUPPORT_MACRONIX_MX25L25635    1
#define ALT_QSPI_SUPPORT_MACRONIX_MX66L51235    1
#define ALT_QSPI_SUPPORT_MICRON_N25Q128         1
#define ALT_QSPI_SUPPORT_MICRON_N25Q512A        1
#define ALT_QSPI_SUPPORT_MICRON_N25Q00AA        1

static ALT_STATUS_CODE alt_qspi_read_register(uint8_t opcode, uint8_t * reg);
static ALT_STATUS_CODE alt_qspi_sr_wait_write(uint32_t timeout);

#if (ALT_QSPI_SUPPORT_MICRON_M25P40==1)
static ALT_QSPI_DEV_CONFIG_t qspi_config_MICRON_M25P40 =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 2,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0x0B,
    .read_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_dummy_cycles = 8,
    .write_op_code = 0x02,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_dummy_cycles = 0,
    .erase_count = 2,
    .erase_sizes = {64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0xd8, 0xc7},
    .erase_sector_idx = 1,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
};
#endif

#if (ALT_QSPI_SUPPORT_MICRON_M25P16)
static ALT_QSPI_DEV_CONFIG_t qspi_config_MICRON_M25P16 =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 2,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0x0B,
    .read_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_dummy_cycles = 8,
    .write_op_code = 0x02,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_dummy_cycles = 0,
    .erase_count = 2,
    .erase_sizes = {64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0xd8, 0xc7},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
};
#endif

#if (ALT_QSPI_SUPPORT_MICRON_M25PX16==1)
static ALT_QSPI_DEV_CONFIG_t qspi_config_MICRON_M25PX16 =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 2,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0x3B,
    .read_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_data_xfer_type = ALT_QSPI_MODE_DUAL,
    .read_dummy_cycles = 8,
    .write_op_code = 0xA2,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_DUAL,
    .write_dummy_cycles = 0,
    .erase_count = 3,
    .erase_sizes = {4*1024, 64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0x20, 0xd8, 0xc7},
    .erase_sector_idx = 1,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
};
#endif

#if (ALT_QSPI_SUPPORT_SPANSION_S25FL116K==1)
static ALT_STATUS_CODE alt_qspi_S25FL116K_enable(void);
ALT_QSPI_DEV_CONFIG_t qspi_config_SPANSION_S25FL116K =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 2,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0x6B,
    .read_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 8,
    .write_op_code = 0x02,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_dummy_cycles = 0,
    .erase_count = 3,
    .erase_sizes = {4*1024, 64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0x20, 0xd8, 0x60},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
    .init_func = alt_qspi_S25FL116K_enable,
};
#endif

#if (ALT_QSPI_SUPPORT_SPANSION_S25FL256S==1)
static ALT_STATUS_CODE alt_qspi_S25FL256_S25FL512S_enable(void);
ALT_QSPI_DEV_CONFIG_t qspi_config_SPANSION_S25FL256S =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 3,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0xEC,
    .read_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 6,
    .write_op_code = 0x34,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_dummy_cycles = 0,
    .erase_count = 2,
    .erase_sizes = {64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0xdc, 0x60},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
    .init_func = alt_qspi_S25FL256_S25FL512S_enable,
};
#endif

#if (ALT_QSPI_SUPPORT_SPANSION_S25FL512S==1)
static ALT_STATUS_CODE alt_qspi_S25FL256_S25FL512S_enable(void);
ALT_QSPI_DEV_CONFIG_t qspi_config_SPANSION_S25FL512S =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 512,
    .addr_size = 3,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0xEC,
    .read_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 6,
    .write_op_code = 0x34,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_dummy_cycles = 0,
    .erase_count = 2,
    .erase_sizes = {256*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0xdc, 0x60},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
    .init_func = alt_qspi_S25FL256_S25FL512S_enable,
};
#endif

#if (ALT_QSPI_SUPPORT_MACRONIX_MX25L25635==1) || (ALT_QSPI_SUPPORT_MACRONIX_MX66L51235==1)
static ALT_STATUS_CODE alt_qspi_MX25L25635_MX66L51235_enable(void);
static ALT_QSPI_DEV_CONFIG_t qspi_config_MACRONIX_MX25L25635_MX66L51235 =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 3,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0xEC,
    .read_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 6,
    .write_op_code = 0x3E,
    .write_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_dummy_cycles = 0,
    .erase_count = 4,
    .erase_sizes = {4*1024, 32*1024, 64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0x21, 0x5c, 0xdc, 0x60},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
    .init_func = alt_qspi_MX25L25635_MX66L51235_enable,
};
#endif

#if (ALT_QSPI_SUPPORT_MICRON_N25Q128==1)
static ALT_STATUS_CODE alt_qspi_N25Q128_enable(void);
static ALT_QSPI_DEV_CONFIG_t qspi_config_MICRON_N25Q128 =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 2,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0xEB,
    .read_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 10,
    .write_op_code = 0x12,
    .write_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .write_dummy_cycles = 0,
    .erase_count = 2,
    .erase_sizes = {64*1024, ALT_QSPI_ERASE_CHIP_SIZE},
    .erase_cmds = {0xd8, 0xc7},
    .erase_sector_idx = 0,
    .support_chip_erase = true,
    .wait_func = alt_qspi_sr_wait_write,
    .init_func = alt_qspi_N25Q128_enable,
};
#endif

#if (ALT_QSPI_SUPPORT_MICRON_N25Q00AA==1) || (ALT_QSPI_SUPPORT_MICRON_N25Q512A==1)
static ALT_STATUS_CODE alt_qspi_N25Q512A_N25Q00AA_enable(void);
static ALT_STATUS_CODE alt_qspi_N25Q512A_N25Q00AA_wait_write(uint32_t timeout);
static ALT_QSPI_DEV_CONFIG_t qspi_config_MICRON_N25Q512A_N25Q00AA =
{
    .clk_phase = ALT_QSPI_CFG_SELCLKPHASE_RESET,
    .clk_pol = ALT_QSPI_CFG_SELCLKPOL_RESET,
    .max_freq_mhz = 50,
    .tshsl_ns = 100,
    .tsd2d_ns = 30,
    .tchsh_ns = 30,
    .tslch_ns = 30,
    .page_size = 256,
    .addr_size = 3,
    .inst_type = ALT_QSPI_MODE_SINGLE,
    .read_op_code = 0xEB,
    .read_addr_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_data_xfer_type = ALT_QSPI_MODE_QUAD,
    .read_dummy_cycles = 10,
    .write_op_code = 0x02,
    .write_addr_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_data_xfer_type = ALT_QSPI_MODE_SINGLE,
    .write_dummy_cycles = 0,
    .erase_count = 3,
    .erase_sizes = {4*1024, 64*1024, 32 * 1024 * 1024},
    .erase_cmds = {0x20, 0xd8, 0xc4},
    .erase_sector_idx = 1,
    .support_chip_erase = false,
    .wait_func = alt_qspi_N25Q512A_N25Q00AA_wait_write,
    .init_func = alt_qspi_N25Q512A_N25Q00AA_enable,
    .micron_multi_die = true,
    .die_size = 32*1024*1024,
};
#endif

ALT_STATUS_CODE alt_qspi_confiugre_device(ALT_QSPI_DEV_CONFIG_t * qspi_config)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t rdid;

    // Query device capabilities
    if (status == ALT_E_SUCCESS)
    {
        status = alt_qspi_device_rdid(&rdid);
    }

    if (status == ALT_E_SUCCESS)
    {
        int manuf_id = ALT_QSPI_STIG_RDID_JEDECID_GET(rdid);
        int type_capacity = ALT_QSPI_STIG_RDID_TYPE_CAPACITY_GET(rdid);

        switch (manuf_id)
        {
        case ALT_QSPI_STIG_RDID_JEDECID_MICRON:
            switch (type_capacity)
            {
#if (ALT_QSPI_SUPPORT_MICRON_N25Q00AA==1)
            case 0x21BA:
                memcpy(qspi_config, &qspi_config_MICRON_N25Q512A_N25Q00AA, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x08000000; // 1Gb
                qspi_config->friendly_name = "MICRON_N25Q00AA";
                break;
#endif
#if (ALT_QSPI_SUPPORT_MICRON_N25Q512A==1)
            case 0x20BA:
                memcpy(qspi_config, &qspi_config_MICRON_N25Q512A_N25Q00AA, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x04000000;// 512Mb
                qspi_config->friendly_name = "MICRON_N25Q512A";
                break;
#endif
#if (ALT_QSPI_SUPPORT_MICRON_N25Q128==1)
            case 0x18BA:
                memcpy(qspi_config, &qspi_config_MICRON_N25Q128, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x01000000; // 128Mb
                qspi_config->friendly_name = "MICRON_N25Q128";
                break;
#endif
#if (ALT_QSPI_SUPPORT_MICRON_M25P40==1)
            case 0x1320:
                memcpy(qspi_config, &qspi_config_MICRON_M25P40, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x00080000; // 4Mb
                qspi_config->friendly_name = "MICRON_M25P40";
                break;
#endif
#if (ALT_QSPI_SUPPORT_MICRON_M25PX16==1)
            case 0x1571:
                memcpy(qspi_config, &qspi_config_MICRON_M25PX16, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x00200000; // 16Mb
                qspi_config->friendly_name = "MICRON_M25PX16";
                break;
#endif
#if  (ALT_QSPI_SUPPORT_MICRON_M25P16==1)
            case 0x1520:
                memcpy(qspi_config, &qspi_config_MICRON_M25P16, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x00200000; // 16Mb
                qspi_config->friendly_name = "MICRON_M25P16";
                break;
#endif
            default:
                status = ALT_E_ERROR;
                break;
            }
            break;
        case ALT_QSPI_STIG_RDID_JEDECID_SPANSION:
            switch(type_capacity)
            {
#if (ALT_QSPI_SUPPORT_SPANSION_S25FL256S==1)
                case 0x1902:
                    memcpy(qspi_config, &qspi_config_SPANSION_S25FL256S, sizeof(ALT_QSPI_DEV_CONFIG_t));
                    qspi_config->device_size = 0x02000000; // 256Mb
                    qspi_config->friendly_name = "SPANSION_S25FL256S";
                break;
#endif
#if  ALT_QSPI_SUPPORT_SPANSION_S25FL512S
                case 0x2002:
                    memcpy(qspi_config, &qspi_config_SPANSION_S25FL512S, sizeof(ALT_QSPI_DEV_CONFIG_t));
                    qspi_config->device_size = 0x04000000; // 512Mb
                    qspi_config->friendly_name = "SPANSION_S25FL512S";
                break;
#endif
#if (ALT_QSPI_SUPPORT_SPANSION_S25FL116K==1)
                case 0x1540:
                    memcpy(qspi_config, &qspi_config_SPANSION_S25FL116K, sizeof(ALT_QSPI_DEV_CONFIG_t));
                    qspi_config->device_size = 0x00200000; // 16Mb
                    qspi_config->friendly_name = "SPANSION_S25FL116K";
                break;
#endif
            }
            break;
        case ALT_QSPI_STIG_RDID_JEDECID_MACRONIX:
            switch (type_capacity)
            {
#if (ALT_QSPI_SUPPORT_MACRONIX_MX25L25635==1)
            case 0x1920:
                memcpy(qspi_config, &qspi_config_MACRONIX_MX25L25635_MX66L51235, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x02000000; // 256Mb
                qspi_config->friendly_name = "MACRONIX_MX25L25635";
                break;
#endif
#if (ALT_QSPI_SUPPORT_MACRONIX_MX66L51235==1)
            case 0x1A20:
                memcpy(qspi_config, &qspi_config_MACRONIX_MX25L25635_MX66L51235, sizeof(ALT_QSPI_DEV_CONFIG_t));
                qspi_config->device_size = 0x04000000; // 512Mb
                qspi_config->friendly_name = "MACRONIX_MX66L51235";
                break;
#endif
            default:
                status = ALT_E_ERROR;
                break;
            }
            break;
        default:
            status = ALT_E_ERROR;
            break;
        }
    }

    // If enabled, add the QSPI device size to the list of erasable sizes
    if (status == ALT_E_SUCCESS)
    {
        dprintf("DEBUG[QSPI]: detected device = %s\n", qspi_config->friendly_name);

        if (qspi_config->support_chip_erase)
        {
            qspi_config->erase_sizes[qspi_config->erase_count -1] = qspi_config->device_size;
        }
    }

    return status;
}

static ALT_STATUS_CODE alt_qspi_read_register(uint8_t opcode, uint8_t * reg)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t data;

    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_stig_rd_cmd(opcode, 0, 1, &data, ALT_QSPI_TIMEOUT_INFINITE);
    }

    if(status == ALT_E_SUCCESS)
    {
        *reg = (uint8_t)data;
    }

    return status;
}

static ALT_STATUS_CODE alt_qspi_sr_wait_write(uint32_t timeout)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    uint8_t sr = 0;
    bool infinite = (timeout == ALT_QSPI_TIMEOUT_INFINITE);

    do {
        status = alt_qspi_read_register(ALT_QSPI_STIG_OPCODE_RD_STAT_REG, &sr);
        if (status != ALT_E_SUCCESS)
        {
            break;
        }
        if (!ALT_QSPI_SR_WIP_GET(sr))
        {
            break;
        }
    } while (timeout-- || infinite);

    if (timeout == (uint32_t) -1 && !infinite)
    {
        status = ALT_E_TMO;
    }

    return status;
}

#if (ALT_QSPI_SUPPORT_SPANSION_S25FL116K==1)
static ALT_STATUS_CODE alt_qspi_S25FL116K_enable(void)
{

    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint8_t sr[4];

    // Read SR1
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x05, &sr[0]);
    }

    // Read SR2
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x35, &sr[1]);
    }

    // Read SR3
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x33, &sr[2]);
    }

    if(status == ALT_E_SUCCESS)
    {
        // Enable Quad Mode
        sr[1] |= 0x2;

        // Use default Latencies
        sr[2] &= ~0xF;

        // Write Enable for Volatile Status Register
        status = alt_qspi_stig_cmd(0x50, 0, ALT_QSPI_TIMEOUT_INFINITE);
    }


    // Write Status Register
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_stig_wr_cmd(0x01, 0, 3, (uint32_t*)&sr, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Poll for completion
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_sr_wait_write(ALT_QSPI_TIMEOUT_INFINITE);
    }

    return status;
}
#endif


#if (ALT_QSPI_SUPPORT_SPANSION_S25FL256S==1) || (ALT_QSPI_SUPPORT_SPANSION_S25FL512S==1)
ALT_STATUS_CODE alt_qspi_S25FL256_S25FL512S_enable(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint8_t regs[2];

    // Read CR
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x35, &regs[1]);
    }

    // Return if Quad mode already set
    if((status == ALT_E_SUCCESS) && (regs[1] & 0x02))
    {
        return status;
    }

    // Read SR
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x05, &regs[0]);
    }

    // Write enable
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_device_wren();
    }

    // Write registers
    if(status == ALT_E_SUCCESS)
    {
        regs[1] |= 0x02;
        status = alt_qspi_stig_wr_cmd(0x01, 0, 2, (uint32_t*)regs, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Wait for completion
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_sr_wait_write(ALT_QSPI_TIMEOUT_INFINITE);
    }

    return status;
}
#endif

#if (ALT_QSPI_SUPPORT_MACRONIX_MX25L25635==1) || (ALT_QSPI_SUPPORT_MACRONIX_MX66L51235==1)
static ALT_STATUS_CODE alt_qspi_MX25L25635_MX66L51235_enable(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t data;
    uint8_t * sr = (uint8_t*)&data;

    // Read SR
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x05, sr);
    }

    // Return if quad mode already enabled
    if((status==ALT_E_SUCCESS) && (*sr & 0x40))
    {
        return status;
    }

    // Enable quad mode
    if(status == ALT_E_SUCCESS)
    {
        *sr |= 0x40;
        status = alt_qspi_stig_wr_cmd(0x01, 0, 1, (uint32_t*)sr, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Wait for completion
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_sr_wait_write(ALT_QSPI_TIMEOUT_INFINITE);
    }

    return status;
}
#endif

#if (ALT_QSPI_SUPPORT_MICRON_N25Q128==1)
static ALT_STATUS_CODE alt_qspi_N25Q128_enable(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t data;
    uint8_t * vecr = (uint8_t*)&data;

    // Read VECR
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_read_register(0x65, vecr);
    }

    // Write enable
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_device_wren();
    }

    // Return if Reset/Hold pad functionality is already disabled
    if((status == ALT_E_SUCCESS) && !(*vecr & 0x10))
    {
        return status;
    }

    // Disable Reset/Hold pad functionality
    if(status == ALT_E_SUCCESS)
    {
        *vecr &= ~0x10;
        status = alt_qspi_stig_wr_cmd(0x61, 0, 1, (uint32_t*)vecr, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Wait for completion
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_sr_wait_write(ALT_QSPI_TIMEOUT_INFINITE);
    }

    return status;
}
#endif

#if (ALT_QSPI_SUPPORT_MICRON_N25Q512A==1) || (ALT_QSPI_SUPPORT_MICRON_N25Q00AA==1)
static ALT_STATUS_CODE alt_qspi_N25Q512A_N25Q00AA_enable(void)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    // Reset enable
    if (status == ALT_E_SUCCESS)
    {
        status = alt_qspi_stig_cmd(0x66, 0, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Reset memory
    if (status == ALT_E_SUCCESS)
    {
        status = alt_qspi_stig_cmd(0x99, 0, ALT_QSPI_TIMEOUT_INFINITE);
    }

    // Write enable
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_device_wren();
    }

    // Switch to 4 byte mode
    if (status == ALT_E_SUCCESS)
    {
        status = alt_qspi_stig_cmd(0xb7, 0, 10000);
    }

    return status;
}

static ALT_STATUS_CODE alt_qspi_N25Q512A_N25Q00AA_wait_write(uint32_t timeout)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint8_t fsr = 0;
    bool infinite = (timeout == ALT_QSPI_TIMEOUT_INFINITE);

    // Wait for the status register as for the other devices
    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_sr_wait_write(timeout);
    }

    // Wait for the flag status register
    do
    {
        // Read flag status register
        status = alt_qspi_read_register(0x70, &fsr);
        if (status != ALT_E_SUCCESS)
        {
            break;
        }
        if (fsr & 0x80)
        {
            break;
        }
    }
    while (timeout-- || infinite);

    if (timeout == (uint32_t)-1 && !infinite)
    {
        status = ALT_E_TMO;
    }

    return status;

}
#endif

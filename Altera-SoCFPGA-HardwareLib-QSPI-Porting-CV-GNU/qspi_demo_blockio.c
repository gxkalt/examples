/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

/******************************************************************************/
/*!
 * Demonstrates reading and writing from/to QSPI Flash Memory using Block I/O
 *
 * \param       address  Where to read the data from Flash. Must be 4-byte aligned.
 * \param       size Size of data to write to flash. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_block_io(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;

    printf("INFO: Demo block I/O using address = 0x%08x, size = %d bytes.\n", (int)address, (int)size);

    // Erase flash
    status = qspi_demo_erase(address, size);

    // Write 'write_buffer' to flash
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Writing to address 0x%08x, size = %d bytes using block I/O.\n", (int)address, (int)size);
        status = alt_qspi_write(address, Write_Buffer, size);
    }

    //  Read 'read_buffer' from flash
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Reading from address 0x%08x, size = %d bytes using block I/O.\n", (int)address, (int)size);
        memset(Read_Buffer, 0, size);
        status = alt_qspi_read(Read_Buffer, address, size);
    }

    // Compare buffers
    if(status ==  ALT_E_SUCCESS)
    {
        printf("INFO: Comparing written data with read back data.\n");
        if(memcmp(Read_Buffer, Write_Buffer, size) != 0)
        {
            status = ALT_E_ERROR;
        }
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Demo block I/O succeeded.\n");
    }
    else
    {
        printf("ERROR: Demo block I/O failed.\n");
    }

    printf("\n");

    return status;
}

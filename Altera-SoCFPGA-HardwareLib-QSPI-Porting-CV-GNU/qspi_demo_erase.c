/******************************************************************************
*
* Copyright 2014 Altera Corporation. All Rights Reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. The name of the author may not be used to endorse or promote products
* derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE DISCLAIMED. IN NO
* EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
*
******************************************************************************/

#include "qspi_demo.h"

#define ALT_QSPI_MINIMUM_ERASE_SIZE (256 * 1024)

/******************************************************************************/
/*!
 * Demonstrates erasing QSPI Flash memory
 *
 * \param       address  Where to erase data. Must be 4-byte aligned.
 * \param       size Size of data to be erased. Must be 4-byte aligned.
 * \return      result of the function
 */
ALT_STATUS_CODE qspi_demo_erase(uint32_t address, uint32_t size)
{
    ALT_STATUS_CODE status = ALT_E_SUCCESS;
    uint32_t erase_address;
    uint32_t erase_size;

    // Buffers for storing the additional data that we may need to erase
    static uint32_t buffer1[ALT_QSPI_MINIMUM_ERASE_SIZE / sizeof(uint32_t)];
    static uint32_t buffer2[ALT_QSPI_MINIMUM_ERASE_SIZE / sizeof(uint32_t)];

    // Location and size of the data that is additionally erased
    uint32_t buffer1_address;
    uint32_t buffer2_address;
    uint32_t buffer1_size = 0;
    uint32_t buffer2_size = 0;

    if(status == ALT_E_SUCCESS)
    {
        status = alt_qspi_query_erase_span(address, size, &erase_address, &erase_size);
    }

    if(status == ALT_E_SUCCESS)
    {

        // Determine the buffer to be saved at the beginning
        if(erase_address < address)
        {
            buffer1_address = erase_address;
            buffer1_size = address - erase_address;
        }

        // Determine the buffer to be saved at the end
        uint32_t end_address = address + size;
        uint32_t erase_end_address = erase_address + erase_size;
        if(erase_end_address != end_address)
        {
            buffer2_address = end_address;
            buffer2_size = erase_end_address - end_address;
        }
    }

    // Save buffer at the beginning of the erase area
    if((status == ALT_E_SUCCESS) && (buffer1_size != 0))
    {
        printf("INFO: Saving %d bytes from address 0x%04x which also need to be erased.\n", (int)buffer1_size, (int) buffer1_address);
        status = alt_qspi_read(buffer1, buffer1_address, buffer1_size);
    }

    // Save buffer at the end of the erase area
    if((status == ALT_E_SUCCESS) && (buffer2_size != 0))
    {
        printf("INFO: Saving %d bytes from address 0x%04x which also need to be erased.\n", (int)buffer2_size, (int) buffer2_address);
        status = alt_qspi_read(buffer2, buffer2_address, buffer2_size);
    }

    // Perform erasing
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Erasing %d bytes at address 0x%08x.\n", (int)erase_size, (int)erase_address);
        alt_qspi_erase(erase_address, erase_size);
    }

    // Restore additionally erased data at the beginning of the area
    if((status == ALT_E_SUCCESS) && (buffer1_size != 0))
    {
        printf("INFO: Restoring %d bytes to address 0x%08x.\n", (int)buffer1_size, (int) buffer1_address);
        status = alt_qspi_write(buffer1_address, buffer1, buffer1_size);
    }

    // Restore additionally erased data at the end of the area
    if((status == ALT_E_SUCCESS) && (buffer2_size != 0))
    {
        printf("INFO: Restoring %d bytes to address 0x%08x.\n", (int)buffer2_size, (int) buffer2_address);
        status = alt_qspi_write(buffer2_address, buffer2, buffer2_size);
    }

    // Display result
    if(status == ALT_E_SUCCESS)
    {
        printf("INFO: Erasing completed successfully.\n");
    }
    else
    {
        printf("INFO: Erasing failed.\n");
    }

    return status;
}
